
DROP DATABASE IF EXISTS repraz;

CREATE DATABASE repraz;

USE repraz;

CREATE TABLE `trivia` (
`triviaid` int(11) NOT NULL auto_increment,
`question` VARCHAR(255) NOT NULL,
`answer` VARCHAR(255) NOT NULL,
`a` VARCHAR(255) NOT NULL,
`b` VARCHAR(255) NOT NULL,
`c` VARCHAR(255) NOT NULL,
`d` VARCHAR(255) NOT NULL,
`e` VARCHAR(255) NOT NULL,
`triviacategoryid` INT NOT NULL,
`level` INT NOT NULL,
`questionnumber` INT NOT NULL,
`difficulty` INT NOT NULL,
`explanation` TEXT NOT NULL,
`more` TINYTEXT NOT NULL, PRIMARY KEY  (`triviaid`)) ENGINE=MyISAM;

CREATE TABLE `triviacategory` (
`triviacategoryid` int(11) NOT NULL auto_increment,
`triviacategoryname` VARCHAR(255) NOT NULL,
`parentid` INT NOT NULL, PRIMARY KEY  (`triviacategoryid`)) ENGINE=MyISAM;

CREATE TABLE `trivialevel` (
`trivialevelid` int(11) NOT NULL auto_increment,
`level` INT NOT NULL,
`levelname` VARCHAR(255) NOT NULL,
`minwager` INT NOT NULL,
`maxwager` INT NOT NULL,
`allowdd` TINYINT NOT NULL,
`minknowledge` INT NOT NULL,
`maxknowledge` INT NOT NULL, PRIMARY KEY  (`trivialevelid`)) ENGINE=MyISAM;

CREATE TABLE `user` (
`userid` int(11) NOT NULL auto_increment,
`username` VARCHAR(255) NOT NULL,
`password` VARCHAR(255) NOT NULL,
`devicehash` VARCHAR(255) NOT NULL,
`facebook` VARCHAR(255) NOT NULL,
`firstname` VARCHAR(255) NOT NULL,
`lastname` VARCHAR(255) NOT NULL,
`email` VARCHAR(255) NOT NULL,
`level` INT NOT NULL,
`coins` INT NOT NULL,
`knowledge` INT NOT NULL, PRIMARY KEY  (`userid`)) ENGINE=MyISAM;

CREATE TABLE `usertrivia` (
`usertriviaid` int(11) NOT NULL auto_increment,
`userid` INT NOT NULL,
`triviaid` INT NOT NULL,
`answer` VARCHAR(255) NOT NULL,
`correct` VARCHAR(255) NOT NULL,
`dateanswered` DATE NOT NULL,
`wager` INT NOT NULL,
`winnings` INT NOT NULL, PRIMARY KEY  (`usertriviaid`)) ENGINE=MyISAM;

CREATE TABLE `session` (
`sessionid` int(11) NOT NULL auto_increment,
`userid` INT NOT NULL,
`hash` VARCHAR(255) NOT NULL,
`expires` DATETIME NOT NULL, PRIMARY KEY  (`sessionid`)) ENGINE=MyISAM;
