<?php

/*
 * The client requests a list of players ordered according to ranking.
 * Client sends the type of ranking desired as well as the user's current point total.
 * Simple controller that validates input, calls the model, and responds
 *
 * example:
 * User/Leaderboard?scope=f&type=kp&id=2932&rows=10
 *
 * response: 
 * 				{
 * 					{position: 5, name: "Santa Claus", points: 3948},
 *					{position: 6, name: "Mickey Mouse", points: 3290},
 *					{position: 7, name: "Sheik Yaboodi", points: 2988},
 *					{position: 8, name: "Joe User", points: 2932},
 *					{position: 9, name: "Ben Dover", points 2891},
 *					{position: 10, name: "Chump Change", points: 2218},
 *					{position: 11, name: "Agent Smart", points: 1893},
 *					{position: 12, name: "Howie Long", points: 1234},
 *					{position: 13, name: "Forrest Gump", points: 982},
 *					{position: 14, name: "A Rock", points: 12},
 *        		}
 *
 */

if (!isset($_GET['id'])) { $_GET['id'] = "undefine"; }
if (!isset($_GET['scope'])) { $_GET['scope'] = "w"; }
if (!isset($_GET['type'])) { $_GET['type'] = "kp"; }
if (!isset($_GET['rows'])) { $_GET['rows'] = "10"; }

$userid = $_GET['id'];
$scope = $_GET['scope'];
$type = $_GET['type'];
$numRows = $_GET['rows'];

// fill in default values if 

if (!filter_var($userid, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for userid'}";
	return;
}

if (!filter_var($scope, FILTER_SANITIZE_STRING))
{
	echo "{ code: 0, msg: 'format error for scope'}";
	return;
}

if (!filter_var($type, FILTER_SANITIZE_STRING))
{
	echo "{ code: 0, msg: 'format error for type'}";
	return;
}

if (!filter_var($numRows, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for rows'}";
	return;
}

//
// Ok, now that we have the input, make sure we have a valid user session
//

if (!isset($_SESSION['user']))
{  
	echo "{ code: 0, msg: 'not signed in'}";
	return;
}

// check if $type is the right value
if (!($type == "kp" || $type == "cp" || $type == "xp" || $type == "ip"))
{
	echo "{ code: 0, msg: 'unrecognized type value'}";
	return;	
}

$user = $_SESSION['user'];
$result = $user->GetLeaderboardListJson($userid, $scope, $type, $numRows);
echo $result;
