<?php

/*
 * The client registers or signs in a users
 *
 * example:
 * User/Register?uuid=1234&name=Joe%20User&zip=94111
 *
 * response: 
 * { code: 10, msg: "User already registered", userInfo: { ... }, questions: { ... } }
 *
 */

include_once('../../configuration.php');
include_once('../../objects/class.database.php');
include_once('../../objects/class.model.session.php');
include_once('../../objects/class.model.user.php');

if (!isset($_GET['uuid'])) { $_GET['uuid'] = ""; }
if (!isset($_GET['name'])) { $_GET['name'] = ""; }
if (!isset($_GET['zip'])) { $_GET['zip'] = "undefined"; }

$uuid = $_GET['uuid'];
$name = $_GET['name'];
$zip = $_GET['zip'];

if (!filter_var($uuid, FILTER_SANITIZE_FULL_SPECIAL_CHARS) || strlen($uuid) < 10)
{
	echo "{ code: 0, msg: 'format error for uuid'}";
	return;
}

if (!filter_var($name, FILTER_SANITIZE_FULL_SPECIAL_CHARS))
{
	// disabled until we decide to add it or not
	//echo "{ code: 0, msg: 'format error for name'}";
	//return;
}

if (!filter_var($zip, FILTER_SANITIZE_NUMBER_INT) || strlen($zip) != 5)
{
	// disabled until we decide to add it or not
	//echo "{ code: 0, msg: 'format error for zip'}";
	//return;
}

//
// Find this user by uuid
//

$userinstance = new ModelUser();
$userId = $userinstance->FindByUuid($uuid);
$code = 0;

if ($userId == "unknown")
{
	// new user. add them and sign in
	// TODO: add zip if we have that
	
	$user = new ModelUser();
	$user->deviceHash = $uuid;
	$user->level = 1;
	$user->coins = 10;
	$user->knowledge = 0;
	$user->lastName = $name;	// TODO FIXME do we want two names or one or more? based on facebook?
	$user->Save();
	
	$userId = $user->userId;
	
//	echo var_dump($user)."<br/>";
	
	$code = 1;
	$msg = "new user";
}
else
{
	// existing user. sign them in
	$user = new ModelUser();
	$user->Get($userId);
	$code = 10;
	$msg = "user already registered";
	
	// if we didn't have a name already then add it to the DB
	if ($user->lastName == "" && $name != "")
	{
		$user->lastName = $name;
		$user->Save();
	}
}

$session = new ModelSession();
$session->CreateNewSession($userId);
$sessionId = $session->sessionId;
$crlf = $session->hash;

//
// Return user deets and questions for this user
//

$userInfo = $user->UserDetails();
$questions = $user->NextQuestions();

//
// Respond with the appropriate code and details
//

echo "{ \"code\": {$code}, \"msg\": \"{$msg}\", \"session\": {$sessionId}, \"crlf\": \"{$crlf}\", \"userInfo\": {$userInfo}, \"questions\": {$questions} }";
return;
