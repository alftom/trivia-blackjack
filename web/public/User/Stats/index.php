<?php

/*
 * The client requests player stats.
 * Simple controller that validates input, calls the model, and responds
 *
 * example:
 * User/Stats?correct=45&incorrect=20
 *
 * response: 
 * 				{
 * 					code: 1,
 * 					triviaPercentile: 69,
 * 				}
 *
 */

if (!isset($_GET['correct'])) { $_GET['correct'] = "undefine"; }
if (!isset($_GET['incorrect'])) { $_GET['incorrect'] = "undefine"; }

$correct = $_GET['correct'];
$incorrect = $_GET['incorrect'];

if (!filter_var($correct, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for correct'}";
	return;
}

if (!filter_var($incorrect, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for incorrect'}";
	return;
}

//
// Ok, now that we have the input, make sure we have a valid user session
//

if (!isset($_SESSION['user']))
{  
	echo "{ code: 0, msg: 'not signed in'}";
	return;
}

$user = $_SESSION['user'];
$result = $user->GetPlayerStats($correct, $incorrect);
echo $result;
