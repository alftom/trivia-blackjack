<?php

/*
 * The client registers a users answers and gets a new set of questions to ask
 * Simple controller that validates input, calls the model, and responds
 *
 * example:
 * User/LevelUp?level=3&coin=2500&answers=1b2d3c
 *
 * response: 
 * { code: 1, msg: "Levelup successful", questions: { ... }}
 *
 */

include_once('../../objects/class.model.session.php');

if (!isset($_GET['level'])) { $_GET['level'] = "undefine"; }
if (!isset($_GET['coin'])) { $_GET['coin'] = "undefine"; }
if (!isset($_GET['answers'])) { $_GET['answers'] = "undefine"; }
if (!isset($_GET['session'])) { $_GET['session'] = "undefine"; }
if (!isset($_GET['crlf'])) { $_GET['crlf'] = "undefine"; }

$level = $_GET['level'];
$coin = $_GET['coin'];
$answers = $_GET['answers'];

$sessionId = $_GET['session'];
$sessionHash = $_GET['crlf'];

if (!filter_var($level, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for level'}";
	return;
}

if (!filter_var($coin, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for coin'}";
	return;
}

if (!filter_var($answers, FILTER_SANITIZE_FULL_SPECIAL_CHARS))
{
	echo "{ code: 0, msg: 'format error for answer'}";
	return;
}

if (!filter_var($sessionId, FILTER_SANITIZE_NUMBER_INT))
{
	echo "{ code: 0, msg: 'format error for session'}";
	return;
}

if (!filter_var($sessionHash, FILTER_SANITIZE_FULL_SPECIAL_CHARS))
{
	echo "{ code: 0, msg: 'format error for crlf'}";
	return;
}

//
// Ok, now that we have the input, make sure we have a valid user session
//

$session = new ModelSession();
$session->SetQueryValues($sessionId, $sessionHash);
if (!$session->IsValid())
{
	echo "{ code: 0, msg: 'not signed in'}";
	return;
}

$user = $session->userId;
$result = $user->RecordAnswers($level, $coin, $answers);
echo $result;
