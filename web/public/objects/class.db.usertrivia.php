<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `usertrivia` (
	`usertriviaid` int(11) NOT NULL auto_increment,
	`userid` INT NOT NULL,
	`triviaid` INT NOT NULL,
	`answer` VARCHAR(255) NOT NULL,
	`correct` VARCHAR(255) NOT NULL,
	`dateanswered` DATE NOT NULL,
	`wager` INT NOT NULL,
	`winnings` INT NOT NULL, PRIMARY KEY  (`usertriviaid`)) ENGINE=MyISAM;
*/

/**
* <b>UserTrivia</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=UserTrivia&attributeList=array+%28%0A++0+%3D%3E+%27userId%27%2C%0A++1+%3D%3E+%27triviaId%27%2C%0A++2+%3D%3E+%27answer%27%2C%0A++3+%3D%3E+%27correct%27%2C%0A++4+%3D%3E+%27dateAnswered%27%2C%0A++5+%3D%3E+%27wager%27%2C%0A++6+%3D%3E+%27winnings%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27INT%27%2C%0A++1+%3D%3E+%27INT%27%2C%0A++2+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++3+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++4+%3D%3E+%27DATE%27%2C%0A++5+%3D%3E+%27INT%27%2C%0A++6+%3D%3E+%27INT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class UserTrivia extends POG_Base
{
	public $usertriviaId = '';

	/**
	 * @var INT
	 */
	public $userId;
	
	/**
	 * @var INT
	 */
	public $triviaId;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $answer;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $correct;
	
	/**
	 * @var DATE
	 */
	public $dateAnswered;
	
	/**
	 * @var INT
	 */
	public $wager;
	
	/**
	 * @var INT
	 */
	public $winnings;
	
	public $pog_attribute_type = array(
		"usertriviaId" => array('db_attributes' => array("NUMERIC", "INT")),
		"userId" => array('db_attributes' => array("NUMERIC", "INT")),
		"triviaId" => array('db_attributes' => array("NUMERIC", "INT")),
		"answer" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"correct" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"dateAnswered" => array('db_attributes' => array("NUMERIC", "DATE")),
		"wager" => array('db_attributes' => array("NUMERIC", "INT")),
		"winnings" => array('db_attributes' => array("NUMERIC", "INT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function UserTrivia($userId='', $triviaId='', $answer='', $correct='', $dateAnswered='', $wager='', $winnings='')
	{
		$this->userId = $userId;
		$this->triviaId = $triviaId;
		$this->answer = $answer;
		$this->correct = $correct;
		$this->dateAnswered = $dateAnswered;
		$this->wager = $wager;
		$this->winnings = $winnings;
	}
	
	
	/**
	* Gets object from database
	* @param integer $usertriviaId 
	* @return object $UserTrivia
	*/
	function Get($usertriviaId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `usertrivia` where `usertriviaid`='".intval($usertriviaId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->usertriviaId = $row['usertriviaid'];
			$this->userId = $this->Unescape($row['userid']);
			$this->triviaId = $this->Unescape($row['triviaid']);
			$this->answer = $this->Unescape($row['answer']);
			$this->correct = $this->Unescape($row['correct']);
			$this->dateAnswered = $row['dateanswered'];
			$this->wager = $this->Unescape($row['wager']);
			$this->winnings = $this->Unescape($row['winnings']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $usertriviaList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `usertrivia` ";
		$usertriviaList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "usertriviaid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$usertrivia = new $thisObjectName();
			$usertrivia->usertriviaId = $row['usertriviaid'];
			$usertrivia->userId = $this->Unescape($row['userid']);
			$usertrivia->triviaId = $this->Unescape($row['triviaid']);
			$usertrivia->answer = $this->Unescape($row['answer']);
			$usertrivia->correct = $this->Unescape($row['correct']);
			$usertrivia->dateAnswered = $row['dateanswered'];
			$usertrivia->wager = $this->Unescape($row['wager']);
			$usertrivia->winnings = $this->Unescape($row['winnings']);
			$usertriviaList[] = $usertrivia;
		}
		return $usertriviaList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $usertriviaId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `usertriviaid` from `usertrivia` where `usertriviaid`='".$this->usertriviaId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `usertrivia` set 
			`userid`='".$this->Escape($this->userId)."', 
			`triviaid`='".$this->Escape($this->triviaId)."', 
			`answer`='".$this->Escape($this->answer)."', 
			`correct`='".$this->Escape($this->correct)."', 
			`dateanswered`='".$this->dateAnswered."', 
			`wager`='".$this->Escape($this->wager)."', 
			`winnings`='".$this->Escape($this->winnings)."' where `usertriviaid`='".$this->usertriviaId."'";
		}
		else
		{
			$this->pog_query = "insert into `usertrivia` (`userid`, `triviaid`, `answer`, `correct`, `dateanswered`, `wager`, `winnings` ) values (
			'".$this->Escape($this->userId)."', 
			'".$this->Escape($this->triviaId)."', 
			'".$this->Escape($this->answer)."', 
			'".$this->Escape($this->correct)."', 
			'".$this->dateAnswered."', 
			'".$this->Escape($this->wager)."', 
			'".$this->Escape($this->winnings)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->usertriviaId == "")
		{
			$this->usertriviaId = $insertId;
		}
		return $this->usertriviaId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $usertriviaId
	*/
	function SaveNew()
	{
		$this->usertriviaId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `usertrivia` where `usertriviaid`='".$this->usertriviaId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `usertrivia` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>