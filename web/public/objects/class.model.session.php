<?php

/*
 * For now I'm keeping sessions in the database instead of on disk or in memcached
 *
 * Our session data is super simple to start:
 * 'userId' which is kept internal (not the uuid)
 * 'hash' which is a CRLF style random string generated anew for each session
 * 'expires' which tells us if the session is timed out / expired
 *
 * Each request should check to see if our session is valid before proceeding, else reject the request
 * Each request that was successful though causes us to automatically update the expiration date of the session
 * New sessions generate a hash that's used in all subsequent API calls
 * API calls basically pass back the sessionId (could be obfuscated but basically is a DB id) and the hash to validate
 * We also need an API to expire a session on demand (for a 'signout' behavior)
 * 
 * Tests needed, see tests/session.php for these:
 * - create session
 * - session valid
 * - session invalid (bad id, bad hash)
 * - expire session
 * - session expired
 *
 * Housekeeping:
 * this table can grow unbounded. long term we should use a background process to periodically
 * purge old rows that have expired. for now just perform that purge as a query whenever we
 * detect an expired session.
 */

include_once('class.pog_base.php');
include_once('class.db.session.php');

//
// Default session duration defined by this constant
// e.g. "+2 hours", "+1 week"
//

define("SESSION_EXPIRATION", "+1 hour");

class ModelSession extends Session
{
	//
	// Static TestSession returns True/False based on passed params
	//
	
	function TestSession($sessionId, $hash)
	{
		$session = new ModelSession();
		$session->SetQueryValues($sessionId, $hash);
		return $session->IsValid();
	}
	
	//
	// Each API call should create a session, call this with the
	// values that were passed in the API, then call IsValid
	//
	
	function SetQueryValues($sessionId, $hash)
	{
		$this->sessionId = $sessionId;
		$this->hash = $hash;
	}
	
	//
	// CreateNewSession sets up a new session
	// Params: userId (not uuid)
	// Returns: Hash string
	//
	
	function CreateNewSession($userId)
	{		
		$this->userId = $userId;
		$this->hash = $this->GenerateRandomHash();
		$this->expires = $this->NewExpirationDate();
		
		parent::Save();
		
		return $this->hash;
	}
	
	//
	// ExpireSession removes a session
	//
	
	function ExpireSession()
	{
		return parent::Delete();
	}
	
	//
	// IsValid
	// Returns: true/false
	// Side effects: 
	//  - extends session expiration date if valid
	//  - causes database cleanup if not valid
	//
	
	function IsValid()
	{
		$test = new ModelSession();
		$result = $test->Get($this->sessionId);
		
		if ($test->hash == $this->hash && strlen($test->hash) == 17)
		{
			$this->expires = $this->NewExpirationDate();
			$this->Save();
			
			return true;
		}
		
		$this->DeleteExpiredSession();
		return false;
	}
	
	//
	// Helpers
	//
	
	function RandomFiveChars()
	{
		// relatively fast random string from 10000 to zzzzz
		return base_convert(mt_rand(0x19A100, 0x39AA3FF), 10, 36);
	}
	
	function GenerateRandomHash()
	{
		return $this->RandomFiveChars()."-".$this->RandomFiveChars()."-".$this->RandomFiveChars();
	}
	
	function NewExpirationDate()
	{
		// 
		// use strtotime for an easy way to note how long sessions last
		// and convert/keep these date strings in mysql DATETIME format
		//
		
		date_default_timezone_set('UTC');
		$time = strtotime(constant("SESSION_EXPIRATION"));
		return date( 'Y-m-d H:i:s', $time );
	}
	
	function DeleteExpiredSession()
	{
		// all expired sessions should have an expired field before the following date
		$datetime = $this->NewExpirationDate();
		
		// now delete them
		$connection = Database::Connect();
		$this->pog_query = "delete from `session` where `expired` < '".$datetime."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
}