<?php

/*
 * Where I add behavior beyond the simple, generated DB class
 *
 */

include_once('class.pog_base.php');
include_once('class.db.user.php');
include_once('class.model.trivia.php');
include_once('class.model.trivialevel.php');

class ModelUser extends User
{
	//
	// Given a device ID find the userId
	// WARNING: you must sanitize the input for $uuid from SQL injection attacks before calling this
	//
	
	function FindByUuid($uuid)
	{
		$connection = Database::Connect();
		$this->pog_query = "select userId from `user` where `deviceHash`='".intval($uuid)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			return $row['userid'];
		}
		return "unknown";
	}
	
	//
	// Returns a Json encoded summary of this user
	//
	
	function UserDetails()
	{	
		$result = array('uuid' => $this->deviceHash,
						'level' => $this->level,
						'coins' => $this->coins,
						'knowledge' => $this->knowledge,
						'name' => $this->lastName
						);
		return json_encode($result);
	}
	
	//
	// Based on this users level, get a list of suitable questions
	// TODO: sorted based on A/B test lists perhaps
	// TODO: send everything sorted at once? or just a set per level?
	//
	
	function NextQuestions()
	{
		$triviainstace = new ModelTrivia();
		$result = array('level' => $this->level,
						'list' => $triviainstace->GetQuestionsForLevel($this->level));
		return json_encode($result);
	}
	
	//
	// Adds knowledge to the user record
	// Checks to see if the user leveled up as well
	// Responds with "true" if new level
	//
	
	function AddKnowledge($knowledge)
	{
		$amount = intval($this->knowledge) + intval($knowledge);
		$this->knowledge = $amount;
		
		$trivialevelinstance = new ModelTriviaLevel();
		$newlevel = $trivialevelinstance->LevelFromKnowledge($amount);
		$oldlevel = $this->level;
		
		//
		// Update the user details
		//

		$this->knowledge = $amount;
		$this->level = $newlevel;
		parent::Save();
		
		return ($newlevel != $oldlevel);
	}
	
	//
	// Our class includes lots of extra stuff we don't want to send to the client
	// but we could send a subset for all valid responses
	//
	
	function RecordAnswersResponseJson($code)
	{
		$result = array("code" => $code, 
						"level" => $this->level,
						"coins" => $this->coins,
						"knowledge" => $this->knowledge);
		return json_encode($result);
	}
	
	//
	// Takes an answer string and modifies the user based on what they answered
	// Returns: a json summary of the user or an error
	//
	
	function RecordAnswers($level, $coins, $answers)
	{
		//
		// Verify that the client has the correct level and coins still
		//
		
		if ($level != $this->level) return "{code: 0, msg: 'incorrect level'}";
		if ($coins != $this->coins) return "{code: 0, msg: 'incorrect coins'}";
		
		//
		// Parse the list of responses and make sure they looked reasonable
		//
		
		$answerlist = explode('.', $answers);
		$answerdetails = array();
		foreach($answerlist as $item)
		{
			$result = preg_match("/([0-9]*)([a-e])([0-9]*)/i", $item, &$matches);
			
			if (!array_key_exists(1, $matches) || 
				!array_key_exists(2, $matches) || 
				!array_key_exists(3, $matches) ||
				$matches[1] == '' ||
				$matches[2] == '' ||
				$matches[3] == '')
			{
				return "{code: 0, msg: 'format error in answers'}";
			}
			
			$deets = array('id'=>$matches[1], 'choice'=>$matches[2], 'bet'=>$matches[3]);
			$answerdetails[] = $deets;
		}
		
		//
		// Verify the answers and increment knowledge and coins
		// format: 45a6.46c7.47a7dd means:
		//	question 45, answered a, bet 6 coins
		//	question 46, answered c, bet 7 coins
		//	question 47, answered a, bet 7 coins, doubled down
		//
				
		//
		// Get the list of correct answers and convert it into a hash based on question number
		// this is for quick reference (instead of finding them O(n^2) by iterating)
		//

		$triviainstance = new Trivia();
		$trivia = $triviainstance->GetList();
		
		$mappedTrivia = array();
		foreach($trivia as $item)
		{
			$mappedTrivia[$item->questionNumber] = $item;
		}
				
		//
		// now check each question
		//
		
		foreach($answerdetails as $answer)
		{
			$id = $answer['id'];
			$correct = $mappedTrivia["{$id}"];
						
			$theirs = $answer['choice'];
			$ours = $correct->answer;
			$amountBet = $answer["bet"];
			
			if ($theirs == $ours)
			{
				//
				// correct, add their bet amount
				//
				
				$this->coins += $answer["bet"];
				$this->knowledge += $correct->difficulty;
			}
			else
			{
				//
				// incorrect, no xp and deduct bet
				// TODO: what about hitting 0?
				//
				
				$this->coins -= $answer["bet"];
				if ($this->coins < 1)
					$this->coins = 1;	// alfred suggested not letting people go below 1 for now. TBD
			}
		}
		
		//
		// If our knowledge has increased to a new level then level up!
		//
		
		$trivialevelinstance = new ModelTriviaLevel();
		$newlevel = $trivialevelinstance->LevelFromKnowledge($this->knowledge);
		if ($newlevel != $this->level)
		{
			$this->level = $newlevel;
		}
		
		//
		// Update the DB with the results
		//
		
		parent::Save();

//		print $this->RecordAnswersResponseJson(1);
//		print json_encode($this);

		return $this->RecordAnswersResponseJson(1);
	}

	// Gets a list of players for a leaderboard.
	// $scope can be worldwide (w) or within user's friends-list (f).
	// $type can be kp (kp), xp (xp), or cp (cp).
	// $numRows is the number of players in the leaderboard list, including the user.
	// GetLeaderboardList tried to get a list 2x$numRows and then pairs the list down
	// depending on where the user is in the list.
	// TODO:  implement the friends leaderboard once we figure out how to store that (add FBid IN clause to WHERE )
	function GetLeaderboardList($userid, $scope, $type, $numRows)
	{
		$connection = Database::Connect();
		$this->pog_query = "(SELECT user1.firstname, user1.lastname, user1.$type AS points, user1.userid " .
						"FROM user AS user1 " .
						"WHERE user1.$type >= (SELECT $type FROM user WHERE user.userid = $userid)" .
						"ORDER BY user1.$type ASC LIMIT $numRows) " .
						"UNION " .
						"(SELECT user2.firstname, user2.lastname, user2.$type AS points, user2.userid " .
						"FROM user AS user2 " .
						"WHERE user2.$type < (SELECT $type FROM user WHERE user.userid = $userid)" .
						"ORDER BY user2.$type DESC LIMIT $numRows) " .
						"ORDER BY points DESC";
		$cursor = Database::Reader($this->pog_query, $connection);
		$returnArray = array();
		while ($row = Database::Read($cursor))
		{
			 $returnArray[] = $row;
		}

		$arraySize = count($returnArray);
		if ($arraySize <= $numRows+3) return array_slice($returnArray, 0, $numRows); // don't have to strip values
		
		// find where the user is in the array by comparing the userid
		$userIndex = 0;
		foreach ($returnArray as $value) {
			if ($value.userid == $userid)
				break;
			else $userIndex++;
		}

		if ($userIndex < ($numRows*.06)) {					// user is at the top of the list, so
			$userIndex = 0;									// just chop the end of array.
		} elseif ($userIndex > ($arraySize-$numRows/2)) {	// user is at the bottom of the list, so
			$userIndex = $arraySize-$numRows -1;			// chop the front or the array.
		} else {											// else just take the middle slice.
			$userIndex = floor($numRows/2);	// 			
		}
		$returnArray = array_slice($returnArray, $userIndex, $numRows);
		
		return $returnArray;
	}

	// 
	// TODO:  implement the friends leaderboard once we figure out how to store that (add FBid IN clause to WHERE )
	function GetLeaderboardPosition($userid, $scope, $type)
	{
		$connection = Database::Connect();
		$this->pog_query = "SELECT COUNT(user.firstname) AS position " .
						"FROM user " .
						"WHERE user.$type >= (SELECT $type FROM user WHERE user.userid = $userid)";
		$cursor = Database::Reader($this->pog_query, $connection);
		$row = Database::Read($cursor);
		return intval($row["position"]);
	}

	function GetLeaderboardListJson($userid, $scope="w", $type="kp", $numRows = 9)
	{
		// first convert type to the actual name of the User DB field
		switch ($type) {
			case 'kp':
				$type = "knowledge";
				break;
			case 'xp':
				$type = "experience";
				break;
			case 'cp':
				$type = "competition";
				break;
			case 'ip':
				$type = "influence";
				break;
		}
		
		$leaderArray = $this->GetLeaderboardList($userid, $scope, $type, $numRows);
		
		$firstUserId = $leaderArray[0]["userid"];
		$position = $this->GetLeaderboardPosition($firstUserId, $scope, $type);
		
		$result = array();
		foreach ($leaderArray as $player) {
			$result[] = array(
				"position" => $position++,
				"name" => $player.firstname . " " . $player.lastname,
				"kp" => $player.points,
			);
		}
		
		return json_encode($result);
	}
}
?>