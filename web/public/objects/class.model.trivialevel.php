<?php

/*
 * Where I add behavior beyond the simple, generated DB class
 *
 */

include_once('class.db.trivialevel.php');

class ModelTriviaLevel extends TriviaLevel
{
	//
	// Fills in object by level, not levelId
	//
	function GetFromLevel($lvl)
	{
		$list = parent::GetList();
		foreach($list as $item)
		{
			if ($item->level == $lvl)
			{
				parent::Get($item->trivialevelId);
				return true;
			}
		}
		return false;
	}
	
	//
	// Determines what level a user should be based on how much knowledge they have
	// Returns: "unknown" if no level matches that amount of knowledge
	//
	function LevelFromKnowledge($knowledge)
	{
		$amount = intval($knowledge);
		$list = parent::GetList();
		foreach($list as $item)
		{
			$min = intval($item->minKnowledge);
			$max = intval($item->maxKnowledge);
			if (($amount >= $min) &&
				($amount <= $max))
				return $item->level;
		}
		return "unknown";
	}
}
?>