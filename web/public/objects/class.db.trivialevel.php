<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `trivialevel` (
	`trivialevelid` int(11) NOT NULL auto_increment,
	`level` INT NOT NULL,
	`levelname` VARCHAR(255) NOT NULL,
	`minwager` INT NOT NULL,
	`maxwager` INT NOT NULL,
	`allowdd` TINYINT NOT NULL,
	`minknowledge` INT NOT NULL,
	`maxknowledge` INT NOT NULL, PRIMARY KEY  (`trivialevelid`)) ENGINE=MyISAM;
*/

/**
* <b>TriviaLevel</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=TriviaLevel&attributeList=array+%28%0A++0+%3D%3E+%27level%27%2C%0A++1+%3D%3E+%27levelName%27%2C%0A++2+%3D%3E+%27minWager%27%2C%0A++3+%3D%3E+%27maxWager%27%2C%0A++4+%3D%3E+%27allowDD%27%2C%0A++5+%3D%3E+%27minKnowledge%27%2C%0A++6+%3D%3E+%27maxKnowledge%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27INT%27%2C%0A++1+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++2+%3D%3E+%27INT%27%2C%0A++3+%3D%3E+%27INT%27%2C%0A++4+%3D%3E+%27TINYINT%27%2C%0A++5+%3D%3E+%27INT%27%2C%0A++6+%3D%3E+%27INT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class TriviaLevel extends POG_Base
{
	public $trivialevelId = '';

	/**
	 * @var INT
	 */
	public $level;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $levelName;
	
	/**
	 * @var INT
	 */
	public $minWager;
	
	/**
	 * @var INT
	 */
	public $maxWager;
	
	/**
	 * @var TINYINT
	 */
	public $allowDD;
	
	/**
	 * @var INT
	 */
	public $minKnowledge;
	
	/**
	 * @var INT
	 */
	public $maxKnowledge;
	
	public $pog_attribute_type = array(
		"trivialevelId" => array('db_attributes' => array("NUMERIC", "INT")),
		"level" => array('db_attributes' => array("NUMERIC", "INT")),
		"levelName" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"minWager" => array('db_attributes' => array("NUMERIC", "INT")),
		"maxWager" => array('db_attributes' => array("NUMERIC", "INT")),
		"allowDD" => array('db_attributes' => array("NUMERIC", "TINYINT")),
		"minKnowledge" => array('db_attributes' => array("NUMERIC", "INT")),
		"maxKnowledge" => array('db_attributes' => array("NUMERIC", "INT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function TriviaLevel($level='', $levelName='', $minWager='', $maxWager='', $allowDD='', $minKnowledge='', $maxKnowledge='')
	{
		$this->level = $level;
		$this->levelName = $levelName;
		$this->minWager = $minWager;
		$this->maxWager = $maxWager;
		$this->allowDD = $allowDD;
		$this->minKnowledge = $minKnowledge;
		$this->maxKnowledge = $maxKnowledge;
	}
	
	
	/**
	* Gets object from database
	* @param integer $trivialevelId 
	* @return object $TriviaLevel
	*/
	function Get($trivialevelId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `trivialevel` where `trivialevelid`='".intval($trivialevelId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->trivialevelId = $row['trivialevelid'];
			$this->level = $this->Unescape($row['level']);
			$this->levelName = $this->Unescape($row['levelname']);
			$this->minWager = $this->Unescape($row['minwager']);
			$this->maxWager = $this->Unescape($row['maxwager']);
			$this->allowDD = $this->Unescape($row['allowdd']);
			$this->minKnowledge = $this->Unescape($row['minknowledge']);
			$this->maxKnowledge = $this->Unescape($row['maxknowledge']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $trivialevelList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `trivialevel` ";
		$trivialevelList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "trivialevelid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$trivialevel = new $thisObjectName();
			$trivialevel->trivialevelId = $row['trivialevelid'];
			$trivialevel->level = $this->Unescape($row['level']);
			$trivialevel->levelName = $this->Unescape($row['levelname']);
			$trivialevel->minWager = $this->Unescape($row['minwager']);
			$trivialevel->maxWager = $this->Unescape($row['maxwager']);
			$trivialevel->allowDD = $this->Unescape($row['allowdd']);
			$trivialevel->minKnowledge = $this->Unescape($row['minknowledge']);
			$trivialevel->maxKnowledge = $this->Unescape($row['maxknowledge']);
			$trivialevelList[] = $trivialevel;
		}
		return $trivialevelList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $trivialevelId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `trivialevelid` from `trivialevel` where `trivialevelid`='".$this->trivialevelId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `trivialevel` set 
			`level`='".$this->Escape($this->level)."', 
			`levelname`='".$this->Escape($this->levelName)."', 
			`minwager`='".$this->Escape($this->minWager)."', 
			`maxwager`='".$this->Escape($this->maxWager)."', 
			`allowdd`='".$this->Escape($this->allowDD)."', 
			`minknowledge`='".$this->Escape($this->minKnowledge)."', 
			`maxknowledge`='".$this->Escape($this->maxKnowledge)."' where `trivialevelid`='".$this->trivialevelId."'";
		}
		else
		{
			$this->pog_query = "insert into `trivialevel` (`level`, `levelname`, `minwager`, `maxwager`, `allowdd`, `minknowledge`, `maxknowledge` ) values (
			'".$this->Escape($this->level)."', 
			'".$this->Escape($this->levelName)."', 
			'".$this->Escape($this->minWager)."', 
			'".$this->Escape($this->maxWager)."', 
			'".$this->Escape($this->allowDD)."', 
			'".$this->Escape($this->minKnowledge)."', 
			'".$this->Escape($this->maxKnowledge)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->trivialevelId == "")
		{
			$this->trivialevelId = $insertId;
		}
		return $this->trivialevelId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $trivialevelId
	*/
	function SaveNew()
	{
		$this->trivialevelId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `trivialevel` where `trivialevelid`='".$this->trivialevelId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `trivialevel` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>