<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `trivia` (
	`triviaid` int(11) NOT NULL auto_increment,
	`question` VARCHAR(255) NOT NULL,
	`answer` VARCHAR(255) NOT NULL,
	`a` VARCHAR(255) NOT NULL,
	`b` VARCHAR(255) NOT NULL,
	`c` VARCHAR(255) NOT NULL,
	`d` VARCHAR(255) NOT NULL,
	`e` VARCHAR(255) NOT NULL,
	`triviacategoryid` INT NOT NULL,
	`level` INT NOT NULL,
	`questionnumber` INT NOT NULL,
	`difficulty` INT NOT NULL,
	`explanation` TEXT NOT NULL,
	`more` TINYTEXT NOT NULL, PRIMARY KEY  (`triviaid`)) ENGINE=MyISAM;
*/

/**
* <b>Trivia</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=Trivia&attributeList=array+%28%0A++0+%3D%3E+%27question%27%2C%0A++1+%3D%3E+%27answer%27%2C%0A++2+%3D%3E+%27a%27%2C%0A++3+%3D%3E+%27b%27%2C%0A++4+%3D%3E+%27c%27%2C%0A++5+%3D%3E+%27d%27%2C%0A++6+%3D%3E+%27e%27%2C%0A++7+%3D%3E+%27triviaCategoryId%27%2C%0A++8+%3D%3E+%27level%27%2C%0A++9+%3D%3E+%27questionNumber%27%2C%0A++10+%3D%3E+%27difficulty%27%2C%0A++11+%3D%3E+%27explanation%27%2C%0A++12+%3D%3E+%27more%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++1+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++2+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++3+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++4+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++5+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++6+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++7+%3D%3E+%27INT%27%2C%0A++8+%3D%3E+%27INT%27%2C%0A++9+%3D%3E+%27INT%27%2C%0A++10+%3D%3E+%27INT%27%2C%0A++11+%3D%3E+%27TEXT%27%2C%0A++12+%3D%3E+%27TINYTEXT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class Trivia extends POG_Base
{
	public $triviaId = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $question;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $answer;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $a;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $b;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $c;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $d;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $e;
	
	/**
	 * @var INT
	 */
	public $triviaCategoryId;
	
	/**
	 * @var INT
	 */
	public $level;
	
	/**
	 * @var INT
	 */
	public $questionNumber;
	
	/**
	 * @var INT
	 */
	public $difficulty;
	
	/**
	 * @var TEXT
	 */
	public $explanation;
	
	/**
	 * @var TINYTEXT
	 */
	public $more;
	
	public $pog_attribute_type = array(
		"triviaId" => array('db_attributes' => array("NUMERIC", "INT")),
		"question" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"answer" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"a" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"b" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"c" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"d" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"e" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"triviaCategoryId" => array('db_attributes' => array("NUMERIC", "INT")),
		"level" => array('db_attributes' => array("NUMERIC", "INT")),
		"questionNumber" => array('db_attributes' => array("NUMERIC", "INT")),
		"difficulty" => array('db_attributes' => array("NUMERIC", "INT")),
		"explanation" => array('db_attributes' => array("TEXT", "TEXT")),
		"more" => array('db_attributes' => array("TEXT", "TINYTEXT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function Trivia($question='', $answer='', $a='', $b='', $c='', $d='', $e='', $triviaCategoryId='', $level='', $questionNumber='', $difficulty='', $explanation='', $more='')
	{
		$this->question = $question;
		$this->answer = $answer;
		$this->a = $a;
		$this->b = $b;
		$this->c = $c;
		$this->d = $d;
		$this->e = $e;
		$this->triviaCategoryId = $triviaCategoryId;
		$this->level = $level;
		$this->questionNumber = $questionNumber;
		$this->difficulty = $difficulty;
		$this->explanation = $explanation;
		$this->more = $more;
	}
	
	
	/**
	* Gets object from database
	* @param integer $triviaId 
	* @return object $Trivia
	*/
	function Get($triviaId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `trivia` where `triviaid`='".intval($triviaId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->triviaId = $row['triviaid'];
			$this->question = $this->Unescape($row['question']);
			$this->answer = $this->Unescape($row['answer']);
			$this->a = $this->Unescape($row['a']);
			$this->b = $this->Unescape($row['b']);
			$this->c = $this->Unescape($row['c']);
			$this->d = $this->Unescape($row['d']);
			$this->e = $this->Unescape($row['e']);
			$this->triviaCategoryId = $this->Unescape($row['triviacategoryid']);
			$this->level = $this->Unescape($row['level']);
			$this->questionNumber = $this->Unescape($row['questionnumber']);
			$this->difficulty = $this->Unescape($row['difficulty']);
			$this->explanation = $this->Unescape($row['explanation']);
			$this->more = $this->Unescape($row['more']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $triviaList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `trivia` ";
		$triviaList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "triviaid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$trivia = new $thisObjectName();
			$trivia->triviaId = $row['triviaid'];
			$trivia->question = $this->Unescape($row['question']);
			$trivia->answer = $this->Unescape($row['answer']);
			$trivia->a = $this->Unescape($row['a']);
			$trivia->b = $this->Unescape($row['b']);
			$trivia->c = $this->Unescape($row['c']);
			$trivia->d = $this->Unescape($row['d']);
			$trivia->e = $this->Unescape($row['e']);
			$trivia->triviaCategoryId = $this->Unescape($row['triviacategoryid']);
			$trivia->level = $this->Unescape($row['level']);
			$trivia->questionNumber = $this->Unescape($row['questionnumber']);
			$trivia->difficulty = $this->Unescape($row['difficulty']);
			$trivia->explanation = $this->Unescape($row['explanation']);
			$trivia->more = $this->Unescape($row['more']);
			$triviaList[] = $trivia;
		}
		return $triviaList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $triviaId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `triviaid` from `trivia` where `triviaid`='".$this->triviaId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `trivia` set 
			`question`='".$this->Escape($this->question)."', 
			`answer`='".$this->Escape($this->answer)."', 
			`a`='".$this->Escape($this->a)."', 
			`b`='".$this->Escape($this->b)."', 
			`c`='".$this->Escape($this->c)."', 
			`d`='".$this->Escape($this->d)."', 
			`e`='".$this->Escape($this->e)."', 
			`triviacategoryid`='".$this->Escape($this->triviaCategoryId)."', 
			`level`='".$this->Escape($this->level)."', 
			`questionnumber`='".$this->Escape($this->questionNumber)."', 
			`difficulty`='".$this->Escape($this->difficulty)."', 
			`explanation`='".$this->Escape($this->explanation)."', 
			`more`='".$this->Escape($this->more)."' where `triviaid`='".$this->triviaId."'";
		}
		else
		{
			$this->pog_query = "insert into `trivia` (`question`, `answer`, `a`, `b`, `c`, `d`, `e`, `triviacategoryid`, `level`, `questionnumber`, `difficulty`, `explanation`, `more` ) values (
			'".$this->Escape($this->question)."', 
			'".$this->Escape($this->answer)."', 
			'".$this->Escape($this->a)."', 
			'".$this->Escape($this->b)."', 
			'".$this->Escape($this->c)."', 
			'".$this->Escape($this->d)."', 
			'".$this->Escape($this->e)."', 
			'".$this->Escape($this->triviaCategoryId)."', 
			'".$this->Escape($this->level)."', 
			'".$this->Escape($this->questionNumber)."', 
			'".$this->Escape($this->difficulty)."', 
			'".$this->Escape($this->explanation)."', 
			'".$this->Escape($this->more)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->triviaId == "")
		{
			$this->triviaId = $insertId;
		}
		return $this->triviaId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $triviaId
	*/
	function SaveNew()
	{
		$this->triviaId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `trivia` where `triviaid`='".$this->triviaId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `trivia` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>