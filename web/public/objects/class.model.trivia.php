<?php

/*
 * Where I add behavior beyond the simple, generated DB class
 *
 */

include_once('class.db.trivia.php');

class ModelTrivia extends Trivia
{
	//
	// All questions for a given level
	// Returns: array
	//
	
	function GetQuestionsForLevel($lvl)
	{
		$result = array();
		$index = 0;
		
		$list = parent::GetList();
		
		foreach($list as $item)
		{	
			if ($item->level == $lvl)
			{
				$result[$index++] = $this->SanitizedQuestion($item);
			}
		}
		return $result;
	}
	
	//
	// Just the details that the client wants to see and nothing more
	// Returns: array
	//
	
	function SanitizedQuestion($item)
	{
		$result = array(
			"questionNumber" => $item->questionNumber,
			"question" => $item->question,
			"answer" => $item->answer,
			"a" => $item->a,
			"b" => $item->b,
			"c" => $item->c,
			"d" => $item->d,
			"e" => $item->e,
			"difficulty" => $item->difficulty
		);
		return $result;
	}
}