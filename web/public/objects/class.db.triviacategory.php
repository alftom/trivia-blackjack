<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `triviacategory` (
	`triviacategoryid` int(11) NOT NULL auto_increment,
	`triviacategoryname` VARCHAR(255) NOT NULL,
	`parentid` INT NOT NULL, PRIMARY KEY  (`triviacategoryid`)) ENGINE=MyISAM;
*/

/**
* <b>TriviaCategory</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=TriviaCategory&attributeList=array+%28%0A++0+%3D%3E+%27triviaCategoryName%27%2C%0A++1+%3D%3E+%27parentID%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++1+%3D%3E+%27INT%27%2C%0A%29
*/
include_once('class.pog_base.php');
class TriviaCategory extends POG_Base
{
	public $triviacategoryId = '';

	/**
	 * @var VARCHAR(255)
	 */
	public $triviaCategoryName;
	
	/**
	 * @var INT
	 */
	public $parentID;
	
	public $pog_attribute_type = array(
		"triviacategoryId" => array('db_attributes' => array("NUMERIC", "INT")),
		"triviaCategoryName" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"parentID" => array('db_attributes' => array("NUMERIC", "INT")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function TriviaCategory($triviaCategoryName='', $parentID='')
	{
		$this->triviaCategoryName = $triviaCategoryName;
		$this->parentID = $parentID;
	}
	
	
	/**
	* Gets object from database
	* @param integer $triviacategoryId 
	* @return object $TriviaCategory
	*/
	function Get($triviacategoryId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `triviacategory` where `triviacategoryid`='".intval($triviacategoryId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->triviacategoryId = $row['triviacategoryid'];
			$this->triviaCategoryName = $this->Unescape($row['triviacategoryname']);
			$this->parentID = $this->Unescape($row['parentid']);
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $triviacategoryList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `triviacategory` ";
		$triviacategoryList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "triviacategoryid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$triviacategory = new $thisObjectName();
			$triviacategory->triviacategoryId = $row['triviacategoryid'];
			$triviacategory->triviaCategoryName = $this->Unescape($row['triviacategoryname']);
			$triviacategory->parentID = $this->Unescape($row['parentid']);
			$triviacategoryList[] = $triviacategory;
		}
		return $triviacategoryList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $triviacategoryId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `triviacategoryid` from `triviacategory` where `triviacategoryid`='".$this->triviacategoryId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `triviacategory` set 
			`triviacategoryname`='".$this->Escape($this->triviaCategoryName)."', 
			`parentid`='".$this->Escape($this->parentID)."' where `triviacategoryid`='".$this->triviacategoryId."'";
		}
		else
		{
			$this->pog_query = "insert into `triviacategory` (`triviacategoryname`, `parentid` ) values (
			'".$this->Escape($this->triviaCategoryName)."', 
			'".$this->Escape($this->parentID)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->triviacategoryId == "")
		{
			$this->triviacategoryId = $insertId;
		}
		return $this->triviacategoryId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $triviacategoryId
	*/
	function SaveNew()
	{
		$this->triviacategoryId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `triviacategory` where `triviacategoryid`='".$this->triviacategoryId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `triviacategory` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>