<?php
/*
	This SQL query will create the table to store your object.

	CREATE TABLE `session` (
	`sessionid` int(11) NOT NULL auto_increment,
	`userid` INT NOT NULL,
	`hash` VARCHAR(255) NOT NULL,
	`expires` DATETIME NOT NULL, PRIMARY KEY  (`sessionid`)) ENGINE=MyISAM;
*/

/**
* <b>session</b> class with integrated CRUD methods.
* @author Php Object Generator
* @version POG 3.0f / PHP5
* @copyright Free for personal & commercial use. (Offered under the BSD license)
* @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=session&attributeList=array+%28%0A++0+%3D%3E+%27userId%27%2C%0A++1+%3D%3E+%27hash%27%2C%0A++2+%3D%3E+%27expires%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27INT%27%2C%0A++1+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++2+%3D%3E+%27DATETIME%27%2C%0A%29
*/
include_once('class.pog_base.php');
class session extends POG_Base
{
	public $sessionId = '';

	/**
	 * @var INT
	 */
	public $userId;
	
	/**
	 * @var VARCHAR(255)
	 */
	public $hash;
	
	/**
	 * @var DATETIME
	 */
	public $expires;
	
	public $pog_attribute_type = array(
		"sessionId" => array('db_attributes' => array("NUMERIC", "INT")),
		"userId" => array('db_attributes' => array("NUMERIC", "INT")),
		"hash" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"expires" => array('db_attributes' => array("TEXT", "DATETIME")),
		);
	public $pog_query;
	
	
	/**
	* Getter for some private attributes
	* @return mixed $attribute
	*/
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}
	
	function session($userId='', $hash='', $expires='')
	{
		$this->userId = $userId;
		$this->hash = $hash;
		$this->expires = $expires;
	}
	
	
	/**
	* Gets object from database
	* @param integer $sessionId 
	* @return object $session
	*/
	function Get($sessionId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `session` where `sessionid`='".intval($sessionId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->sessionId = $row['sessionid'];
			$this->userId = $this->Unescape($row['userid']);
			$this->hash = $this->Unescape($row['hash']);
			$this->expires = $row['expires'];
		}
		return $this;
	}
	
	
	/**
	* Returns a sorted array of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param string $sortBy 
	* @param boolean $ascending 
	* @param int limit 
	* @return array $sessionList
	*/
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `session` ";
		$sessionList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "sessionid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$session = new $thisObjectName();
			$session->sessionId = $row['sessionid'];
			$session->userId = $this->Unescape($row['userid']);
			$session->hash = $this->Unescape($row['hash']);
			$session->expires = $row['expires'];
			$sessionList[] = $session;
		}
		return $sessionList;
	}
	
	
	/**
	* Saves the object to the database
	* @return integer $sessionId
	*/
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `sessionid` from `session` where `sessionid`='".$this->sessionId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `session` set 
			`userid`='".$this->Escape($this->userId)."', 
			`hash`='".$this->Escape($this->hash)."', 
			`expires`='".$this->expires."' where `sessionid`='".$this->sessionId."'";
		}
		else
		{
			$this->pog_query = "insert into `session` (`userid`, `hash`, `expires` ) values (
			'".$this->Escape($this->userId)."', 
			'".$this->Escape($this->hash)."', 
			'".$this->expires."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->sessionId == "")
		{
			$this->sessionId = $insertId;
		}
		return $this->sessionId;
	}
	
	
	/**
	* Clones the object and saves it to the database
	* @return integer $sessionId
	*/
	function SaveNew()
	{
		$this->sessionId = '';
		return $this->Save();
	}
	
	
	/**
	* Deletes the object from the database
	* @return boolean
	*/
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `session` where `sessionid`='".$this->sessionId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}
	
	
	/**
	* Deletes a list of objects that match given conditions
	* @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...} 
	* @param bool $deep 
	* @return 
	*/
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `session` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}
}
?>