<?php

/*
* This unit test simulates JSON posts of questions that the user answered, verifies that they're correct
* (not cheating), and then records them in the DB and updates the user level and such. Then pulls the
* information for the next level and verifies that it's correct from the text fixture. 
* Also, it gets user stats and leaderboard info.
*/

include_once("common.php");
?>
<style>

td {
border-top:1px solid grey;
border-left:1px solid grey;
border-right:1px solid grey;
padding-left:10px;
padding-right:10px;
}

</style>
<?php
//
// test fixture setup
//

?><table border =0><?php

$userinstance = new ModelUser();
$list = $userinstance->GetList();

TestCaseRow(
"No users initially in blank DB",
PassFailResult(sizeof($list) == 0),
"");

$level = new ModelTriviaLevel($level='1', $levelName='1', $minWager='0', $maxWager='10000', $allowDD='0', $minKnowledge='0', $maxKnowledge='10');
$id = $level->Save();

$leveltest = new ModelTriviaLevel();
$result = $leveltest->GetFromLevel('1');
if (!$result) print "FAIL";

TestCaseRow(
	"Add first level",
	PassFailResult(($leveltest->level == '1') &&
		($leveltest->minWager == '0') &&
		($leveltest->maxWager == '10000') &&
		($leveltest->maxKnowledge == '10')),
	"");

TestCaseRow("Adding more levels", "n/a", "");

$next = new ModelTriviaLevel($level='2', $levelName='2', $minWager='0', $maxWager='10000', $allowDD='1', $minKnowledge='11', $maxKnowledge='20');
$next->Save();
$next = new ModelTriviaLevel($level='3', $levelName='3', $minWager='0', $maxWager='10000', $allowDD='1', $minKnowledge='21', $maxKnowledge='30');
$next->Save();
$next = new ModelTriviaLevel($level='4', $levelName='4', $minWager='0', $maxWager='10000', $allowDD='1', $minKnowledge='31', $maxKnowledge='40');
$next->Save();
$next = new ModelTriviaLevel($level='5', $levelName='5', $minWager='0', $maxWager='10000', $allowDD='1', $minKnowledge='41', $maxKnowledge='50');
$next->Save();

//
// Add the first user and try leveling up manually
//

$user = new ModelUser($username='joeschmidt', $password='7758729742529458357', $deviceHash='754829345782345', $facebook='', $firstName='Joe', $lastName='Schmidt', $email='', $level='1', $coins='10000', $knowledge='0');
$userid = $user->Save();
$user->Get($userid);

TestCaseRow(
	"Add first user and find",
	PassFailResult ($user->firstName == "Joe"),
	"");

$level = new ModelTriviaLevel();
$level->GetFromLevel($user->level);
$needed = intval($level->maxKnowledge) + 1;
$user->AddKnowledge($needed);

TestCaseRow(
	"Level up brute force",
	PassFailResult ($user->level == "2"),
	"");

$result = $user->RecordAnswers('1', '10000', '1a1.2a2.3a3');

TestCaseRow(
	"Level up bad level",
	PassFailResult ($result == "{code: 0, msg: 'incorrect level'}"),
	 $result);

$result = $user->RecordAnswers('2', '10001', '1a1.2a2.3a3');

TestCaseRow(
	"Level up bad coins",
	PassFailResult ($result == "{code: 0, msg: 'incorrect coins'}"),
	$result);

//
// Ok, now test answering questions. So first add some questions to the test fixture
//

TestCaseRow(
	"Adding basic test questions",
	"n/a",
	"");

$category = new TriviaCategory();
$category->Save();

$trivia = new Trivia($question='some question', $answer='a', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '1', $difficulty = '1');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='b', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '2', $difficulty = '2');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='c', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '3', $difficulty = '3');
$trivia->Save();

$result = $user->RecordAnswers('2', '10000', '1a1.32z');

TestCaseRow(
	"Bogus answers 1",
	PassFailResult ($result == "{code: 0, msg: 'format error in answers'}"),
	$result);

$result = $user->RecordAnswers('2', '10000', 'dhahdfjakhfjksahfa');

TestCaseRow(
	"Bogus answers 2",
	PassFailResult ($result == "{code: 0, msg: 'format error in answers'}"),
	$result);
		
$result = $user->RecordAnswers('3', '10000', '1b1.2c9.3a10');

TestCaseRow(
	"Incorrect level passed",
	PassFailResult ($result == "{code: 0, msg: 'incorrect level'}"),
	$result);
			
// pass incorrect current coins
$result = $user->RecordAnswers('2', '10010', '1b1.2c9.3a10');
TestCaseRow(
	"Incorrect coins passed",
	PassFailResult ($result == "{code: 0, msg: 'incorrect coins'}"),
	$result);

// three bad answers
$result = $user->RecordAnswers('2', '10000', '1b1.2c9.3a10');

TestCaseRow(
	"Push three bad answers",
	PassFailResult ($user->coins == '9980'),
	$result);
TestCaseRow(
	"&nbsp;&nbsp;and experience should not have changed",
	PassFailResult ($user->knowledge == '11'),
	"");

// push three successful answers
$result = $user->RecordAnswers('2', '9980', '1a1.2b9.3c20');

TestCaseRow(
	"Push three successful answers",
	PassFailResult ($user->coins == '10010'),
	$result);
TestCaseRow(
	"&nbsp;&nbsp;and experience should change",
	PassFailResult ($user->knowledge == '17'),
	"");

//
// Now a mix of passed and failed
//

$trivia = new Trivia($question='some question', $answer='a', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '4', $difficulty = '10');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='b', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '5', $difficulty = '20');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='c', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '6', $difficulty = '1');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='c', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '7', $difficulty = '1');
$trivia->Save();

//
// Test leveling up
//

// one good answer and leveled
$result = $user->RecordAnswers('2', '10010', '4a10');

TestCaseRow(
	"Level up after one successful answer",
	PassFailResult ($user->coins == '10020'),
	$result);
TestCaseRow(
	"&nbsp;&nbsp;and experience should not have changed",
	PassFailResult ($user->knowledge == '27'),
	"");
TestCaseRow(
	"&nbsp;&nbsp;and we should have leveled up",
	PassFailResult ($user->level == '3'),
	"");

// double level with one answer
$result = $user->RecordAnswers('3', '10020', '5b10');

TestCaseRow(
	"Double level up after one more successful answer",
	PassFailResult ($user->coins == '10030'),
	"");
TestCaseRow(
	"&nbsp;&nbsp;and experience should not have changed",
	PassFailResult ($user->knowledge == '47'),
	"");
TestCaseRow(
	"&nbsp;&nbsp;and we should have leveled up twice",
	PassFailResult ($user->level == '5'),
	"");

// one corrent and one incorrect
$result = $user->RecordAnswers('5', '10030', '6b10.7c10');

TestCaseRow(
	"One correct and one incorrect answer",
	PassFailResult ($user->coins == '10030'),
	$result);
TestCaseRow(
	"&nbsp;&nbsp;and experience should not have changed",
	PassFailResult ($user->knowledge == '48'),
	"");
TestCaseRow(
	"&nbsp;&nbsp;and we should have stayed the same level",
	PassFailResult ($user->level == '5'),
	"");

//
// Now reset the user and try some more similar tests
//

$user = new ModelUser($username='maryjane', $password='7758729742529458357', $deviceHash='754829345782345', $facebook='', $firstName='Mary', $lastName='Jane', $email='', $level='1', $coins='10000', $knowledge='0');
$userid = $user->Save();
$user->Get($userid);

TestCaseRow(
	"Add new user mary",
	PassFailResult ($user->firstName == "Mary"),
	"");

$result = $user->RecordAnswers('1', '10000', '1a10.2b10.3c10.4a10.5b10.6b10.7c10');

TestCaseRow(
	"Mary answers a bunch at once with one incorrect",
	PassFailResult ($user->coins == '10050'),
	$result);
TestCaseRow(
	"&nbsp;&nbsp;and experience should not have changed",
	PassFailResult ($user->knowledge == '37'),
	"");
TestCaseRow(
	"&nbsp;&nbsp;and we should have leveled up",
	PassFailResult ($user->level == '4'),
	"");

//
// TODO: test minWager and maxWager so people cannot bet more per level then we constrain to
//

	// TODO
	
// 
// Test the leaderboard API in class.model.user.php.  Start adding new users and leveling them up.
// 

$maryuserid = $userid;	// store away Mary's userid because she's the main one

$user = new ModelUser($username='joeuser', $password='passwordjoe', $deviceHash='284743829204930', $facebook='', $firstName='Joe', $lastName='User', $email='', $level='1', $coins='100');
$userid = $user->Save();
$user->Get($userid);

TestCaseRow(
	"Add new user joe",
	PassFailResult ($user->firstName == "Joe"),
	"");

$user = new ModelUser($username='johndoe', $password='passwordjohn', $deviceHash='392756382902857', $facebook='', $firstName='John', $lastName='Doe', $email='', $level='1', $coins='100');
$userid = $user->Save();
$user->Get($userid);

TestCaseRow(
	"Add new user john",
	PassFailResult ($user->firstName == "John"),
	"");

$result = $user->GetLeaderboardList($maryuserid, "w", "knowledge", 3);

TestCaseRow(
	"Test GetLeaderboardList",
	PassFailResult (count($result) == 3),
	"leaderboard players: " . count($result));
	
//var_dump($result[0]["userid"]);
$result = $user->GetLeaderboardPosition($result[0]["userid"], "w", "knowledge");

TestCaseRow(
	"Test GetLeaderboardPosition",
	PassFailResult ($result == 1),
	"top leaderboard position: " . $result);

//TODO:  add more players and start leveling them up.  Call GetLeaderBoardList and make sure Mary is in the right position based on kp

//
// Cleanup
//
	
TestCaseRow(
	"Test case cleanup",
	"n/a",
	"");
TestCaseRow(
	"Deleting users",
	"n/a",
	"");

DeleteUsers();

TestCaseRow(
"Deleting trivia levels and questions",
"n/a",
"");

DeleteTrivia();
?>
</table>
