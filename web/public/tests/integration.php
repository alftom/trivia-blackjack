<?php

/*
 * Integration tests, this makes JSON requests to the API's using curl
 * and then tests responses for correctness
 *
 * Perform these tests after all unit tests have passed
 *
 */

//
// Setup a cUrl cookie jar so I can keep session cookies across requests
// TODO: change the API so that it doesn't rely on cookies but passes a cookie ID
// and add a class and a table with session info in the DB to manage sessions ourselves
//

$ckfile = tempnam ("/tmp", "CURLCOOKIE");

function Fetch($url, $signin, $ckfile)
{
	$ch = curl_init($url);

	if ($signin == True)
		curl_setopt ($ch, CURLOPT_COOKIEJAR, $ckfile);
	else
		curl_setopt ($ch, CURLOPT_COOKIEFILE, $ckfile);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

include_once("common.php");
?>
<style>

td {
border-top:1px solid grey;
border-left:1px solid grey;
border-right:1px solid grey;
padding-left:10px;
padding-right:10px;
}

</style>
<?php
//
// Setup the database with some basics
//

//$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
//print $json;
//var_dump(json_decode($json));

?><table border =0><?php
TestCaseRow(
	"Adding basic test questions",
	"n/a",
	"");

$category = new TriviaCategory();
$category->Save();

$trivia = new Trivia($question='some question', $answer='a', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '1', $difficulty = '1');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='b', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '2', $difficulty = '2');
$trivia->Save();
$trivia = new Trivia($question='some question', $answer='c', $a='a', $b='b', $c='c', $d='d', $e='e', $triviaCategoryId='', $level='1', $questionNumber = '3', $difficulty = '3');
$trivia->Save();

//
// Attempt to signin then see if I'm signed in
//

$data = Fetch("http://localhost/User/Register/?uuid=abracadabra&name=john%20doe", true, $ckfile);
$response = json_decode($data, true);
//print var_dump($response);

TestCaseRow(
"Attempting signin and response is not NULL",
PassFailResult (NULL != $response),
"");

$hasCode = array_key_exists("code",$response);

TestCaseRow(
"Response includes response code",
PassFailResult (True == $hasCode),
"");

TestCaseRow(
"Response code is 1",
PassFailResult (True == $hasCode && 1 == $response['code']),
"");

TestCaseRow(
"Response includes session id and crlf hash",
PassFailResult (array_key_exists("session",$response) && array_key_exists("crlf",$response)),
"");

//
// TODO: check all other responses to make sure that we have user info that matches expectations
// and that we got a list of questions that matches above...
//
// TODO: then level up this user a little and make sure things are still kosher
//
// TODO: then try to level up using an incorrect session and user info, etc.
//

//echo "<br/>";
//
//$data = Fetch("http://localhost/User/Register/?uuid=abracadabra&name=jane%20doe", false, $ckfile);
//echo $data;


//
// Cleanup
//
	
TestCaseRow(
	"Test case cleanup",
	"n/a",
	"");
TestCaseRow(
	"Deleting users",
	"n/a",
	"");

DeleteUsers();

TestCaseRow(
"Deleting trivia levels and questions",
"n/a",
"");

DeleteTrivia();
?>
