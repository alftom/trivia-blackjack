<?php

/*
 * This unit test simulates JSON request to register a new user or sign in
 *
 * - test users already registered
 * - test new user
 * - test existing user returns correct name, level
 * - for all cases prep a list of questions for that person's level
 *
 */

include_once("common.php");

?>

<h4>Register unit tests</h4>
<p>

<?php


	//
	// test fixture setup
	//

$userinstance = new ModelUser();
$list = $userinstance->GetList();

?>No users initially in blank DB - <?php

	PassFail(sizeof($list) == 0);

?>Add first level - <?php

	$level = new ModelTriviaLevel($level='1', $levelName='1', $minWager='0', $maxWager='10000', $allowDD='0', $minKnowledge='0', $maxKnowledge='10');
	$id = $level->Save();
	
	$leveltest = new ModelTriviaLevel();
	$result = $leveltest->GetFromLevel('1');
	if (!$result) print "FAIL";
	
	PassFail(($leveltest->level == '1') &&
		($leveltest->minWager == '0') &&
		($leveltest->maxWager == '10000') &&
		($leveltest->maxKnowledge == '10'));
	
?>Add first user and find - <?php

	$first = new ModelUser($username='joeschmidt', $password='7758729742529458357', $deviceHash='754829345782345', $facebook='', $firstName='Joe', $lastName='Schmidt', $email='', $level='1', $coins='0', $knowledge='0');
	$userid = $first->Save();
	$first->Get($userid);
	PassFail ($first->firstName == "Joe");
	
?>Correct initial level - <?php

	PassFail($first->level == "1");
	
?>Find bogus user id - <?php

	$second = new ModelUser();
	$second->Get($first->userId + 1);
	PassFail($second->firstName != "Joe");
	
	//
	// Response questions for level 1 
	//
	
	$level = new ModelTriviaLevel($first->level);
	
	//
	// Cleanup
	//
	
   ?><p>Cleanup</br/>Deleting users<br/><?php

	DeleteUsers();
	
   ?>Deleting trivia levels and questions<br/><?php

	DeleteTrivia();

?>
