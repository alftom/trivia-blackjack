<?php

/*
 * Common support for unit tests
 *
 */

include_once "../configuration.php";
include_once "../objects/class.database.php";

include_once "../objects/class.db.trivia.php";
include_once "../objects/class.db.triviacategory.php";

include_once "../objects/class.model.session.php";
include_once "../objects/class.model.user.php";
include_once "../objects/class.model.trivialevel.php";

//
// Helpers (move to central spot maybe)
//

function TestCaseRow($test, $passfail, $result)
{
	print "<tr><td>".$test."</td><td>".$passfail."</td><td>".$result."</td></tr>";
}

function PassFailResult($bool)
{
	if ($bool)
	{
		return "passed";
	}
	else
	{
		return "<span style='color:red;font-weight:bold;'>FAILED</span>";
	}
}

function PassFail($bool, $result = '')
{
	if ($result != '')
		$result = " - response: ".$result;
	
	if ($bool)
	{
		?>passed<?php echo $result?><br/><?php
	}
	else 
	{
		?><span style="color:red;font-weight:bold;">FAILED<?php echo $result?></span><br/><?php
	}
}

function DeleteList($list)
{
	foreach($list as $item)
	{
		$item->Delete();
	}
}

function DeleteSessions()
{
	$sessioninstance = new ModelSession();
	DeleteList($sessioninstance->GetList());	// note: could be faster with comparitor and DB class DeleteList
}

function DeleteUsers()
{
	$userinstance = new ModelUser();
	DeleteList($userinstance->GetList());
}

function DeleteTrivia()
{
	$trivialevelinstance = new ModelTriviaLevel();
	$triviainstance = new Trivia();
	$categoryinstance = new TriviaCategory();
	
	$list = array($trivialevelinstance, $triviainstance, $categoryinstance);
	foreach ($list as $item)
	{
		DeleteList($item->GetList());
	}
}