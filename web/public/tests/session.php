<?php

/*
* This unit test perform basic tests of our session tracking
*/

include_once("common.php");

?>
<style>

td {
border-top:1px solid grey;
border-left:1px solid grey;
border-right:1px solid grey;
padding-left:10px;
padding-right:10px;
}

</style>
<?php

$sessioninstance = new ModelSession();
$list = $sessioninstance->GetList();

?><table border =0><?php

TestCaseRow(
"No sessions initially in blank DB",
PassFailResult(sizeof($list) == 0),
"");

//
// session not found
//

$session = new ModelSession();
$result = $session->IsValid();

TestCaseRow(
"Invalid session id",
PassFailResult(False == $result),
"result: ".$result." for id: '".$session->sessionId."' user='".$session->userId."' hash='".$session->hash."'");

//
// create a session
//

$session = new ModelSession();
$hash = $session->CreateNewSession(1);

TestCaseRow(
"Hash should be 17 characters long",
PassFailResult(strlen($hash) == 17),
$hash." is ".strlen($hash)." long");

TestCaseRow(
"Session valid",
PassFailResult($session->IsValid()),
"id = ".$session->sessionId);

$id = $session->sessionId;
$hash = $session->hash;

TestCaseRow(
"Session valid using TestSession",
PassFailResult($sessioninstance->TestSession($id, $hash)),
"");

//
// test an invalid hash
//

TestCaseRow(
"Hash too short",
PassFailResult(!$sessioninstance->TestSession($id, substr($hash, 0, 15))),
substr($hash, 0, 15));

TestCaseRow(
"Bogus hash",
PassFailResult(!$sessioninstance->TestSession($id, "bogus")),
"");

//
// Expire a session
//

$testsession = new ModelSession();
$testsession->Get($id);
$result = $testsession->ExpireSession();
$test = $testsession->IsValid();

TestCaseRow(
"Session expired properly",
PassFailResult(False == $test),
"expired {$id} result: ".$result." and isValid now: ".$test);

TestCaseRow(
"Session expired by TestSession",
PassFailResult(!$sessioninstance->TestSession($id, $hash)),
"");

//
// Cleanup
//

TestCaseRow(
"Deleting sessions",
"n/a",
"");

//DeleteSessions();