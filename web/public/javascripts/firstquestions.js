var questions = {
	1: {
		question: "How many states are in the United States?",
		choices: {
			a: "13",
			b: "15",
			c: "48",
			d: "50",
			e: "55"
		},
		answer: "d",
		difficulty: 1,
		category: "government",
		explanation: "This is not a trick question...",
		more: ""
	},
	2: {
		question: "The highest political office in the United States is",
		choices: {
			a: "the emperor",
			b: "the king/queen",
			c: "the president",
			d: "the prime minister",
		},
		answer: "d",
		difficulty: 1,
		category: "constitution",
		explanation: "The president sports the biggest bulls-eye on his back as well.",
		more: ""
	},
	3: {
		question: "The President of the United States today is",
		choices: {
			a: "Barack Obama",
			b: "George Bush",
			c: "Anheuser Busch",
			d: "Hillary Clinton",
			e: "Paris Hilton"
		},
		answer: "a",
		difficulty: 1,
		category: "current",
		explanation: "A year from now?  Maybe not...",
		more: ""
	},
	4: {
		question: 'Which US president "did not inhale?',
		choices: {
			a: "George W Bush",
			b: "Bill Clinton",
			c: "Ronald Reagan",
			d: "Barak Obama",
		},
		answer: "b",
		difficulty: 2,
		category: "important",
		explanation: "Bill Clinton admitted to smoking marijuana but said he did not inhale.  Please�",
		more: ""
	},
	5: {
		question: "Where does the US president live?",
		choices: {
			a: "Capitol",
			b: "Independence Hall",
			c: "Pentagon",
			d: "Washington Monument",
			e: "White House"
		},
		answer: "e",
		difficulty: 1,
		category: "government",
		explanation: "All 55,000 square feet of it.",
		more: ""
	},
	6: {
		question: "Where does the US president work?",
		choices: {
			a: "Capitol",
			b: "Independence Hall",
			c: "Pentagon",
			d: "Washington Monument",
			e: "White House"
		},
		answer: "e",
		difficulty: 1,
		category: "government",
		explanation: "Yes, the White House is a big live/work loft.",
		more: ""
	},
	7: {
		question: "Which US president tried to give his German counterpart (Angela Merkel) a back rub during a G8 Summit meeting?",
		choices: {
			a: "Barak Obama",
			b: "Bill Clinton",
			c: "George W Bush",
			d: "Ronald Reagan",
		},
		answer: "c",
		difficulty: 3,
		category: "important",
		explanation: "She screamed.  Really.  We're not making this up...",
		more: ""
	},
	8: {
		question: "The government body that passes laws in the US is",
		choices: {
			a: "Congress",
			b: "Federal Reserve System",
			c: "Office of the President",
			d: "Parliament",
			e: "Supreme Court"
		},
		answer: "a",
		difficulty: 1,
		category: "constitution",
		explanation: "In the simplest terms, Congress passes the laws and the president executes them.",
		more: ""
	},
	9: {
		question: "The US Congress meets in which location?",
		choices: {
			a: "Capitol",
			b: "Independence Hall",
			c: "Pentagon",
			d: "Washington Monument",
			e: "White House"
		},
		answer: "a",
		difficulty: 1,
		category: "government",
		explanation: "That's the big domed white building on Capitol Hill.",
		more: ""
	},
	10: {
		question: "The highest judicial court in the US is the",
		choices: {
			a: "Appeals Court",
			b: "District Court",
			c: "Federal Court",
			d: "State Court",
			e: "Supreme Court"
		},
		answer: "e",
		difficulty: 1,
		category: "constitution",
		explanation: 'The Supreme Court is the "highest court in the land" according to the US Consitution.',
		more: ""
	},
	11: {
		question: "Which of the following US documents guarantees the people's rights?",
		choices: {
			a: "Articles of Confederation",
			b: "Constitution",
			c: "Declaration of Independence",
			d: "Emancipation Proclamation",
			e: "Federalist Papers"
		},
		answer: "b",
		difficulty: 1,
		category: "constitution",
		explanation: "The Constitution also includes the Bill of Rights.  The Declaration of Independence merely stated that men have rights, but did not guarantee them by law.",
		more: ""
	},
	12: {
		question: 'Is there an explicit "right to privacy" defined in the US Constitution?',
		choices: {
			a: "Yes",
			b: "No",
		},
		answer: "b",
		difficulty: 2,
		category: "constitution",
		explanation: "This is the hardest question so far.  There is no express right to privacy in the Constitution.  However, starting in the 1920's the Supreme Court made several rulings that interpreted the Bill of Rights as giving the right to privacy.",
		more: ""
	},
	13: {
		question: "Presidential elections and other US elections are held in",
		choices: {
			a: "October",
			b: "November",
			c: "December",
			d: "January",
			e: "February"
		},
		answer: "b",
		difficulty: 1,
		category: "campaigns",
		explanation: "Why November?  That's question for later on.  Hint- the US was an agricultural society way back in 1789...",
		more: ""
	},
	14: {
		question: "What is an incumbent?",
		choices: {
			a: "the candidate currently holding the contested office in an election",
			b: "a candidate running for office that does not currently hold the office",
			c: 'another word for "necessary" or "obligatory"',
			d: "a person that previously held the office but no longer does",
		},
		answer: "a",
		difficulty: 1,
		category: "campaigns",
		explanation: "Just wanted to make sure you knew what it is before we ask the next question.",
		more: ""
	},
	15: {
		question: "In the US, what percent of the time do incumbents win House elections?",
		choices: {
			a: "50%",
			b: "60%",
			c: "70%",
			d: "80%",
			e: "90%"
		},
		answer: "e",
		difficulty: 3,
		category: "campaigns",
		explanation: "Congress' approval ratings are abysmal but people still vote for their incumbents year after year.  Does that make sense?",
		more: ""
	},
	16: {
		question: "How much money was spent on the 2008 US elections?",
		choices: {
			a: "$100 million",
			b: "$500 million",
			c: "$1 billion",
			d: "$5 billion",
			e: "$10 billion"
		},
		answer: "d",
		difficulty: 3,
		category: "campaigns",
		explanation: "It just might be closer to $10 billion before November rolls around this year.",
		more: ""
	},
	17: {
		question: "How much money did Sarah Palin spend on her 2008 campaign wardrobe?",
		choices: {
			a: "$150",
			b: "$1500",
			c: "$15,000",
			d: "$150,000",
			e: "$1.5 million"
		},
		answer: "d",
		difficulty: 4,
		category: "important",
		explanation: "Certainly a lot of money, but compare it to the answer for the previous question.",
		more: ""
	},
	18: {
		question: "What percentage of eligible voters actually voted in the 2008 election?",
		choices: {
			a: "40%",
			b: "50%",
			c: "60%",
			d: "70%",
			e: "80%"
		},
		answer: "c",
		difficulty: 3,
		category: "campaigns",
		explanation: "2008 was a presidential election year.  Turnout was worse in 2010.  We're hoping this game improves the situation a little.",
		more: ""
	},
	19: {
		question: "Let's start talking about budget and economics.  The next few questions are to make sure you understand the basic terms.  First- what is an interest rate?",
		choices: {
			a: "A measure of the effectiveness of an interest group",
			b: "A periodic charge for a loan, expressed as a percentage of the amount of money loaned",
			c: "The percentage someone owns in an entity (such as a corporation)",
			d: "The percentage someone owns in an entity (such as a corporation)",
			e: "The total amount of money that needs to be paid back on a loan separate from the amount of the loan itself"
		},
		answer: "b",
		difficulty: 1,
		category: "budget",
		explanation: "It costs money to borrow money and this cost is usually expressed as an interest rate.  For example, if want to borrow $100 with a 5% interest rate it will cost you $5 a year.",
		more: ""
	},
	20: {
		question: 'What is "inflation"?',
		choices: {
			a: "an increase in the stock market value over time",
			b: "interest on a loan",
			c: "a rise in prices in an economy over time",
			d: "what the universe did after the Big Bang",
			e: "when you have to really have to screw up to get a C in a class"
		},
		answer: "c",
		difficulty: 1,
		category: "budget",
		explanation: "For various reasons, every economy needs at least a little inflation for stability.",
		more: ""
	},
	21: {
		question: "1 billion equals",
		choices: {
			a: "10 million",
			b: "100 million",
			c: "1000 million",
			d: "10,000 million"
		},
		answer: "c",
		difficulty: 1,
		category: "budget",
		explanation: "Counting to 1 billion will take you 32 years.",
		more: ""
	},
	22: {
		question: "1 trillion equals",
		choices: {
			a: "10 billion",
			b: "100 billion",
			c: "1000 billion",
			d: "10,000 billion",
		},
		answer: "c",
		difficulty: 1,
		category: "budget",
		explanation: "If you spent one dollar every second it would take you 32,000 years to spend a trillion dollars.",
		more: ""
	},
	23: {
		question: "What is government debt?",
		choices: {
			a: "The total accumulation of government spending minus government revenues",
			b: "When government spending is greater that government revenue for a period of time (usually a year)",
			c: "When the value of imported goods is GREATER than the value of exported goods over a period of time (usually a year)",
			d: "When the value of imported goods is LESS than the value of exported goods over a period of time (usually a year)",
		},
		answer: "a",
		difficulty: 2,
		category: "budget",
		explanation: "It can also be described as the accumulation of yearly deficits.",
		more: ""
	},
	24: {
		question: "What number is closest to current US federal debt?",
		choices: {
			a: "$100 billion",
			b: "$1 trillion",
			c: "$10 trillion",
			d: "$15 trillion",
			e: "$100 trillion"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "A big number that will get much bigger if nothing is done.",
		more: ""
	},
	25: {
		question: "What is a government budget deficit?",
		choices: {
			a: "The total accumulation of government spending minus government revenues",
			b: "When government spending is greater than government revenue for a period of time (usually a year)",
			c: "When the value of imported goods is GREATER than the value of exported goods over a period of time (usually a year)",
			d: "When the value of imported goods is LESS than the value of exported goods over a period of time (usually a year)",
		},
		answer: "b",
		difficulty: 2,
		category: "budget",
		explanation: "So, debt measures the accumulation of yearly deficits over time.  The opposite of a budget deficit is a budget surplus.",
		more: ""
	},
	26: {
		question: "What number is closest to the current annual US budget deficit?",
		choices: {
			a: "$100 billion",
			b: "$1 trillion",
			c: "$10 trillion",
			d: "$15 trillion",
			e: "$100 trillion"
		},
		answer: "b",
		difficulty: 3,
		category: "budget",
		explanation: "The 2010 deficit was $1.3 trillion.",
		more: ""
	},
	27: {
		question: "What is the definition of GDP (gross domestic product)?",
		choices: {
			a: "The amount of currency in circulation in a country's economy",
			b: "The entire supply of money in a country's economy",
			c: "A measure that relates to the weighted average of a basket of commonly-used consumer goods and services",
			d: "The total market value of all final goods and services produced in a country in a given year",
		},
		answer: "d",
		difficulty: 2,
		category: "budget",
		explanation: "GDP is often tracked to see how fast a country's economy is growing.",
		more: ""
	},
	28: {
		question: "What number is closest to the US gross domestic product (GDP)?",
		choices: {
			a: "$100 billion",
			b: "$1 trillion",
			c: "$10 trillion",
			d: "$15 trillion",
			e: "$100 trillion"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "This is the highest in the world.  As a comparision, China's GDP is about $6 trillion and the whole European Union is about $16 trillion.",
		more: ""
	},
	29: {
		question: "What is the best measurement for the magnitude of government debt?",
		choices: {
			a: "debt per household",
			b: "debt per resident",
			c: "gross annual debt",
			d: "total public debt",
			e: "total public debt as a percentage of annual GDP"
		},
		answer: "e",
		difficulty: 2,
		category: "budget",
		explanation: 'This measurement adjusts the total debt the country is in relative to the size of the economy, e.g.- "pound for pound" which country has the biggest debt problem.',
		more: ""
	},
	30: {
		question: "Is all government debt bad, no matter what magnitude?",
		choices: {
			a: "Yes",
			b: "No",
		},
		answer: "b",
		difficulty: 2,
		category: "budget",
		explanation: "Studies have shown that until debt gets to be 90% of GDP there is little impact on economic prosperity.",
		more: ""
	},
	31: {
		question: "What number is closest to the current US federal debt as a percentage of US GDP?",
		choices: {
			a: "20%",
			b: "40%",
			c: "60%",
			d: "80%",
			e: "100%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "Historically, anything about 90% is really bad for growth.  Yes Ground Control we have a problem...",
		more: ""
	},
	32: {
		question: 'If Congress keeps things as they are now what will US government debt be in the future (in other words, what is the "Fiscal Gap")?',
		choices: {
			a: "$15 trillion",
			b: "$50 trillion",
			c: "$100 trillion",
			d: "$150 trillion",
			e: "$200 trillion"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "This means if US Congress continues to refuse higher taxes and changes to entitlements (like Social Security and Medicare), US government debt will eventually reach $200 trillion.  Maybe we should do something?",
		more: ""
	},
	33: {
		question: "OK let's take a break from depressing talk about deficits and start testing your knowledge about elections since the US has a big one this year.  First question- how is the US president elected?",
		choices: {
			a: "majority of popular vote",
			b: "majority of electoral votes",
			c: "majority of votes in the Senate",
			d: "most money raised from special interest groups",
		},
		answer: "b",
		difficulty: 2,
		category: "campaign",
		explanation: "Sometimes it may seem like the answer is D, at least for Congressmen, but B is the reality.",
		more: ""
	},
	34: {
		question: "How many electoral votes are there for the US presidential election?",
		choices: {
			a: "100",
			b: "435",
			c: "438",
			d: "535",
			e: "538"
		},
		answer: "e",
		difficulty: 3,
		category: "campaign",
		explanation: "A US presidential candidate needs 270 electoral votes to win.",
		more: ""
	},
	35: {
		question: "The minimum voting age in the US is",
		choices: {
			a: "13",
			b: "18",
			c: "21",
			d: "25",
			e: "30"
		},
		answer: "b",
		difficulty: 2,
		category: "campaign",
		explanation: "Old enough to vote and watch pornography, but not old enough to drink.  There's a societal message in there somewhere...",
		more: ""
	},
	36: {
		question: "Any US citizen of proper age can vote.",
		choices: {
			a: "True",
			b: "False",
		},
		answer: "",
		difficulty: 2,
		category: "campaign",
		explanation: "Only registered voters can vote.",
		more: ""
	},
	37: {
		question: "Which of the following occurs earliest in a US presidential election?",
		choices: {
			a: "choosing a running mate",
			b: "electoral college election",
			c: "national party conventions",
			d: "popular election",
			e: "state primaries and caucuses"
		},
		answer: "e",
		difficulty: 2,
		category: "campaign",
		explanation: "The right order is:  primaries/caucuses, choosing a running mate, national conventions, popular vote, and finally the electoral vote.",
		more: ""
	},
	38: {
		question: "US primary elections are held in order to",
		choices: {
			a: "allow state legislatures the opportunity to determine political districts",
			b: "expand the field of candidtes within a political party",
			c: "give the voters more choice in the general election",
			d: "increase participation of third-party candidates",
			e: "narrow down the field of candidates within a political party"
		},
		answer: "e",
		difficulty: 2,
		category: "campaign",
		explanation: "Lucky for us, the primary season will keep generating lost of new content for this game.",
		more: ""
	},
	39: {
		question: "The national convention serves what major purpose for a US political party?",
		choices: {
			a: "allow the people to participate in party organization",
			b: "establish the rules of party campaigning",
			c: "raise money, recruit, and publicize the party",
			d: "select the party's candidate",
			e: "vote on party policy"
		},
		answer: "e",
		difficulty: 2,
		category: "campaign",
		explanation: "These conventions are worthwhile watching because future presidential candidates are often offered speaking time.",
		more: ""
	},
	40: {
		question: "How does a political party elect its US federal election candidates?",
		choices: {
			a: "majority of delegate votes",
			b: "majority of popular votes from the whole voting public",
			c: "majority of votes from party members",
			d: "party leaders pick the candidate",
		},
		answer: "a",
		difficulty: 3,
		category: "campaign",
		explanation: 'In the "good old days" the party leaders picked the candidate.  Now the public votes for delegates who in turn vote for the candidate.',
		more: ""
	},
	41: {
		question: "And a question to see if you were breathing during the last US presidential election- which political figure was parodied by Tina Fey on Saturday Night Live?",
		choices: {
			a: "Hillary Clinton",
			b: "Janet Jackson",
			c: "Sarah Palin",
			d: "Michelle Backmann",
			e: "Nancy Pelosi"
		},
		answer: "c",
		difficulty: 2,
		category: "important",
		explanation: "We didn't want to bring Sarah Palin in again, but this really is must see TV.",
		more: ""
	},
	42: {
		question: "Now let's test your knowledge of influence in campaigns.  First question- what is the name of a private organization that attempts to influence government officials?",
		choices: {
			a: "interest group",
			b: "lobbyist",
			c: "pressure group",
			d: "political pressure group",
			e: "political power group"
		},
		answer: "a",
		difficulty: 3,
		category: "campaign",
		explanation: "Interest groups include those that work for the common good as well as those that work for a particular company or industry.",
		more: ""
	},
	43: {
		question: "Interest groups try to influence Congress by doing all of the following EXCEPT:",
		choices: {
			a: "appealing to the public for support",
			b: "giving financial support for TV ads",
			c: "influencing party platforms",
			d: "nominating candidates for public office",
			e: "taking an issue to court"
		},
		answer: "d",
		difficulty: 2,
		category: "campaign",
		explanation: "Only political parties nominate candidates for elections.",
		more: ""
	},
	44: {
		question: "What is a PAC?",
		choices: {
			a: "A company that raises money to publicize issues, but not to expressly promote voting for or against a candidate or ballot measure",
			b: "A company that spends money on ads and promotion to influence a federal election",
			c: "A political party",
			d: "a video game character",
		},
		answer: "b",
		difficulty: 2,
		category: "campaign",
		explanation: "PACs (political action committees) are stricly regulated in terms of who they can raise money from and how much they can accept from people.",
		more: ""
	},
	45: {
		question: "What is a lobbyist?",
		choices: {
			a: "An individual that gets paid to meet with and influence lawmakers on behalf of a special interest",
			b: "An individual that on occasion volunteers to meet with and influence lawmakers on behalf of a special interest ",
			c: "A political interest group that employs individuals to meet with and influence lawmakers to act in the group's interest",
			d: "A service employee that aids customers on the first floor of a building",
		},
		answer: "a",
		difficulty: 2,
		category: "campaign",
		explanation: "Lobbyists are hired by organizations such as interest groups to meet with government officials.",
		more: ""
	},
	46: {
		question: "The maximum amount an individual can give to a candidate running in a US federal election is",
		choices: {
			a: "$500",
			b: "$1,000 ",
			c: "$2,500 ",
			d: "$5,000 ",
			e: "no limit"
		},
		answer: "c",
		difficulty: 4,
		category: "campaign",
		explanation: "So, when you hear how much a candidate got from Wall Street it means a whole lot of people working in finance donated $2500, not a few big banks giving millions.",
		more: ""
	},
	47: {
		question: "Are campaign ads required by law to be truthful?",
		choices: {
			a: "Yes",
			b: "No",
		},
		answer: "b",
		difficulty: 3,
		category: "campaign",
		explanation: "Slander laws are much more lenient when concerning public figures, so be very skeptical about what you hear in a campaign ad.",
		more: "",
		doubledown: 1
	},
	48: {
		question: "Does the US media have a political bias?",
		choices: {
			a: "Yes, and it's a conservative bias",
			b: "Yes, and it�s a liberal bias",
			c: "No bias",
		},
		answer: "b",
		difficulty: 2,
		category: "campaign",
		explanation: "Surveys have repeadedly shown that there is a liberal media bias.  One notable exception is Fox News, which has a conservative bias.",
		more: ""
	},
	49: {
		question: "Does C-SPAN have a political bias?",
		choices: {
			a: "Yes, and it's a conservative bias",
			b: "Yes, and it�s a liberal bias",
			c: "No bias",
		},
		answer: "c",
		difficulty: 2,
		category: "campaign",
		explanation: "C-SPAN is purely unedited footage, so there is no room for bias.",
		more: ""
	},
	50: {
		question: "So we vote people into office to accomplish something and sometimes they behave badly.  Let's test you on politicians gone wild.  Who was the only US president to resign from office for breaking into the Democratic National Convention headquarters at Watergate?",
		choices: {
			a: "Andrew Jackson",
			b: "Bill Clinton",
			c: "Lyndon Johnson",
			d: "Richard Nixon",
			e: "Ronald Reagan"
		},
		answer: "d",
		difficulty: 3,
		category: "history",
		explanation: "Richard Nixon tried to cover up his involvement in the break in but his conversations were recorded and this lead to his impeachment and resignation.",
		more: ""
	},
	51: {
		question: "Which US president was impeached for perjury on an inquiry into his relationship with a certain White House intern?",
		choices: {
			a: "Andrew Jackson",
			b: "Bill Clinton",
			c: "Lyndon Johnson",
			d: "Richard Nixon",
			e: "Ronald Reagan"
		},
		answer: "b",
		difficulty: 2,
		category: "important",
		explanation: "This was a highly politicized scandal in 1998 with Republicans in one corner and Democrats in the other.  And rumble they did.",
		more: ""
	},
	52: {
		question: "Which US president's administration came under fire for illegally selling arms to Iran in exchange for the release of hostages?",
		choices: {
			a: "Andrew Jackson",
			b: "Bill Clinton",
			c: "Lyndon Johnson",
			d: "Richard Nixon",
			e: "Ronald Reagan"
		},
		answer: "e",
		difficulty: 4,
		category: "history",
		explanation: "This was called the Iran-Contra affair in 1986 because the proceeds from an arms sale to Iran were used to fund the Contra rebels in Nicaragua.  Reagan's involvement was never proven.",
		more: ""
	},
	53: {
		question: "Which governor resigned after a federal wiretap revealed multiple payments to $1000/hour prostitutes, including an all expenses paid trip for one to meet him at the Mayflower hotel in Washington D.C.?",
		choices: {
			a: "Anthony Weiner",
			b: "Eliot Spitzer",
			c: "Herman Cain",
			d: "John Edwards",
			e: "Mark Sanford"
		},
		answer: "b",
		difficulty: 4,
		category: "important",
		explanation: "OK so after investigation no taxpayer funds were found to be used, but $1000/hour?  What's the breakdown on that cost?  Never mind...",
		more: ""
	},
	54: {
		question: "Which congressional representative resigned from his House seat after admitting to sending sexually explicit photos to women over Twitter?",
		choices: {
			a: "Anthony Weiner",
			b: "Eliot Spitzer",
			c: "Herman Cain",
			d: "John Edwards",
			e: "Mark Sanford"
		},
		answer: "a",
		difficulty: 4,
		category: "important",
		explanation: "We are now instituting a ban on weiner jokes.  Enough is enough...",
		more: ""
	},
	55: {
		question: "Which governor said he was hiking the Appalachian Trail for a week but instead was visiting his mistress (ahem) \"soul mate\" in Argentina?",
		choices: {
			a: "Anthony Weiner",
			b: "Eliot Spitzer",
			c: "Herman Cain",
			d: "John Edwards",
			e: "Mark Sanford"
		},
		answer: "e",
		difficulty: 4,
		category: "important",
		explanation: "South Carolina Governor Mark Sanford was censured by the state legislator in December 2009 but was allowed to server the last month of his term.  By the way, this week we'll be \"wine tasting in Napa\" for Doug's bachelor's party in case you're wondering where we are...",
		more: ""
	},
	56: {
		question: "Which 2012 presidential candidate suspended his candidacy after being accused of sexual harassment by four women?",
		choices: {
			a: "Anthony Weiner",
			b: "Eliot Spitzer",
			c: "Herman Cain",
			d: "John Edwards",
			e: "Mark Sanford"
		},
		answer: "c",
		difficulty: 4,
		category: "important",
		explanation: "Well, he wasn't voted in yet but he was the Republican front-runner when this all came out.  Rumor has it that when somebody told him Anita Hill was on the line he replied, \"Does she want to endorse me?\"  Priceless!",
		more: ""
	},
	57: {
		question: "Which former US senator and presidential candidate had an extramarital affair while his wife was undergoing treatment for breast cancer?",
		choices: {
			a: "Anthony Weiner",
			b: "Eliot Spitzer",
			c: "Herman Cain",
			d: "John Edwards",
			e: "Mark Sanford"
		},
		answer: "d",
		difficulty: 4,
		category: "important",
		explanation: "If John Kerry beat George W Bush in the 2004 US presidential election John Edwards would have been your vice president.  Now that would be an interesting parallel universe to visit.",
		more: ""
	},
	58: {
		question: 'Which political leader wrote a song about former US Secretary of State Condoleezza Rice and called her his "African Queen"?',
		choices: {
			a: "Ali Abdullah Saleh",
			b: "Bashar al-Assad",
			c: "Ben Ali",
			d: "Mahmoud Ahmadinejad",
			e: "Muammar Gaddafi "
		},
		answer: "e",
		difficulty: 3,
		category: "important",
		explanation: 'Ms. Rice called the fixation "a little creepy".  Just a little?',
		more: ""
	},
	59: {
		question: "Match the women to the politician:  John F Kennedy",
		choices: {
			a: "Ashley Rae Maika DiPietro",
			b: "Mar�a Bel�n Chapur",
			c: "Marilyn Monroe",
			d: "Monica Lewinsky",
			e: "Rielle Hunter"
		},
		answer: "c",
		difficulty: 3,
		category: "important",
		explanation: "We could have put a lot of other women's names here but chose the obvious.  Stay tuned...",
		more: ""
	},
	60: {
		question: "Match the women to the politician:  Bill Clinton",
		choices: {
			a: "Ashley Rae Maika DiPietro",
			b: "Mar�a Bel�n Chapur",
			c: "Marilyn Monroe",
			d: "Monica Lewinsky",
			e: "Rielle Hunter"
		},
		answer: "d",
		difficulty: 3,
		category: "important",
		explanation: "Ditto.",
		more: ""
	},
	61: {
		question: "Match the women to the politician:  Mark Sanford",
		choices: {
			a: "Ashley Rae Maika DiPietro",
			b: "Mar�a Bel�n Chapur",
			c: "Marilyn Monroe",
			d: "Monica Lewinsky",
			e: "Rielle Hunter"
		},
		answer: "b",
		difficulty: 3,
		category: "important",
		explanation: "According to Wikipedia, which is always accurate (NOT), Maria is a commodities broker in Buenos Aires.",
		more: ""
	},
	62: {
		question: "Match the women to the politician:  Eliot Spitzer",
		choices: {
			a: "Ashley Rae Maika DiPietro",
			b: "Mar�a Bel�n Chapur",
			c: "Marilyn Monroe",
			d: "Monica Lewinsky",
			e: "Rielle Hunter"
		},
		answer: "a",
		difficulty: 3,
		category: "important",
		explanation: "Her original name was Ashley Youmans and her call name was Ashley Alexandra Dupr�.",
		more: ""
	},
	63: {
		question: "Match the women to the politician:  John Edwards",
		choices: {
			a: "Ashley Rae Maika DiPietro",
			b: "Mar�a Bel�n Chapur",
			c: "Marilyn Monroe",
			d: "Monica Lewinsky",
			e: "Rielle Hunter"
		},
		answer: "e",
		difficulty: 3,
		category: "important",
		explanation: "Her original name was Lisa Jo Druck.  Are we sensing a pattern here with name changes?  What about you Ochocinco?",
		more: ""
	},
	64: {
		question: "We can't keep our heads in the sand so let's get back to deficits and focus on government spending.  What number is closest to total US federal government spending in one year?",
		choices: {
			a: "$500 billion",
			b: "$1 trillion",
			c: "$3 trillion",
			d: "$5 trillion",
			e: "$10 trillion"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "In FY2010 government spending was $3.5 trillion.  Total government spending in the US (including states and local) was $6 trillion.",
		more: ""
	},
	65: {
		question: "How many people work for the US government?",
		choices: {
			a: "100,000",
			b: "200,000",
			c: "500,000",
			d: "1 million",
			e: "5 million"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "The US government employs over 5 million people, including military and postal.",
		more: ""
	},
	66: {
		question: 'Which of the following is closest to the total amount of spending allocated by the US American Recovery and Reinvestment Act of 2009 (the "Stimulus Bill")?',
		choices: {
			a: "$100 billion",
			b: "$500 billion",
			c: "$800 billion",
			d: "$1.3 trillion",
			e: "$15 trillion"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "That's about 25% of the average yearly federal budget.  So far about $700 billion has been spent.",
		more: ""
	},
	67: {
		question: "Which of the following is closest to the total amount of US federal spending associated with the wars in Iraq and Afghanistan through 2011?",
		choices: {
			a: "$100 billion",
			b: "$500 billion",
			c: "$800 billion",
			d: "$1.3 trillion",
			e: "$15 trillion"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "That's spread over about 10 years.",
		more: ""
	},
	68: {
		question: 'What is a government "entitlement"?',
		choices: {
			a: "a legal right of citizens",
			b: "a natural right of citizens",
			c: "a program created by a law that guarantees citizens access to benefits ",
			d: "a title bestowed on an individual by a government",
		},
		answer: "d",
		difficulty: 2,
		category: "budget",
		explanation: "Government spending on entitlements is required by law unless Congress changes the law.",
		more: ""
	},
	69: {
		question: "Which of the following is NOT a US entitlement program?",
		choices: {
			a: "defense",
			b: "Medicare",
			c: "Medicaid",
			d: "Social Security",
			e: "unemployment insurance"
		},
		answer: "a",
		difficulty: 2,
		category: "budget",
		explanation: "Entitlement programs have provisions that require the government to pay money to an individual or entity if certain conditions are met (e.g.- a certain age is reached).  Defense spending has no such conditions and can be cut if Congress so desires.  Therefore it is not an entitlement program.",
		more: ""
	},
	70: {
		question: "US social security works like:",
		choices: {
			a: "a bank account you put money in while you are working and start withdrawing from when you retire",
			b: "insurance, where you only get payment if certain contractual conditions are met (e.g.- reach a certain age, work for a minimum amount of time, etc.)",
			c: 'a welfare system where only the "needy" get social security payments',
			d: "a big money pit where you throw cash in and nothing ever comes out",
		},
		answer: "b",
		difficulty: 2,
		category: "budget",
		explanation: "A bank account holds your money which you are entitled to without conditions, which is different from Social Security.  With a welfare system only those in need get payments whereas Social Security payments are need-blind (even the wealthy get Social Security payments, although at a lower percentage of their working income).",
		more: ""
	},
	71: {
		question: "What percentage of the 2010 annual US federal budget was spent on Social Security?",
		choices: {
			a: "5%",
			b: "10%",
			c: "15%",
			d: "20%",
			e: "25%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: 'Social Security is "paid for" by social security taxes.',
		more: ""
	},
	72: {
		question: "What percentage of the 2010 annual US federal budget was spent on defense?",
		choices: {
			a: "5%",
			b: "10%",
			c: "15%",
			d: "20%",
			e: "25%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "This is a big number, but it can be cut much easier than entitlements can.",
		more: ""
	},
	73: {
		question: "What percentage of the 2010 annual US federal budget was spent on Medicare/Medicaid?",
		choices: {
			a: "5%",
			b: "10%",
			c: "15%",
			d: "20%",
			e: "25%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "As you can see this is the elephant in the room.  And the elephant will only get bigger.",
		more: ""
	},
	74: {
		question: "What percentage of the 2010 annual US federal budget was spent on interest on Treasury Bills?",
		choices: {
			a: "5%",
			b: "10%",
			c: "15%",
			d: "20%",
			e: "25%"
		},
		answer: "a",
		difficulty: 3,
		category: "budget",
		explanation: "Interest rates are so low on T-bills that it doesn't amount to much.  Greece and Italy on the other hand don't have it so good...",
		more: ""
	},
	75: {
		question: 'What is an "earmark"?',
		choices: {
			a: "An item in a bill that is marked for future debate",
			b: "An item in a bill inserted to gain votes from legislators that would otherwise oppose the bill",
			c: "An item in a bill that designates federal funds for a specific state project (thus benefiting a congressman's constituencies)",
			d: "a mark such as the one Mike Tyson put on Evander Holyfield's ear",
		},
		answer: "c",
		difficulty: 2,
		category: "budget",
		explanation: 'An earmark is also called "pork" or "pork barrel spending"',
		more: ""
	},
	76: {
		question: "What percentage of the US federal budget is used on earmarks?",
		choices: {
			a: "1%",
			b: "2%",
			c: "5%",
			d: "10%",
			e: "20%"
		},
		answer: "a",
		difficulty: 3,
		category: "budget",
		explanation: "Lots of fuss about earmarks, but not a lot of real impact to the budget.",
		more: ""
	},
	77: {
		question: "Now we know what the government spends money on, so let's now focus on taxes and income.  What was the top US tax rate in 2010?",
		choices: {
			a: "25%",
			b: "31%",
			c: "33%",
			d: "35%",
			e: "39.6%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "Of course this does not include all the deductions people use.",
		more: ""
	},
	78: {
		question: "What was the top US tax rate in 1994?",
		choices: {
			a: "25%",
			b: "31%",
			c: "33%",
			d: "35%",
			e: "39.6%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: 'The reduction from 39.6% (when Bill Clinton was president) to the current 35% was the result of the so-called "Bush tax cuts" that are temporary in nature.',
		more: ""
	},
	79: {
		question: "What percentage of total US income earned goes to the top 1% of wage earners?",
		choices: {
			a: "5%",
			b: "15%",
			c: "25%",
			d: "35%",
			e: "45%"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "So, a quarter of all money earned in the US goes to the top 1%.",
		more: ""
	},
	80: {
		question: "What percentage of the total personal net worth in the US is owned by the top 1% of households?",
		choices: {
			a: "5%",
			b: "15%",
			c: "25%",
			d: "35%",
			e: "45%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "The top 1% own over a third of the personal wealth in the US.",
		more: ""
	},
	81: {
		question: "What percentage of total US federal income taxes do the top 1% of wage earners pay?",
		choices: {
			a: "10%",
			b: "20%",
			c: "30%",
			d: "40%",
			e: "50%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "The top 1% pay 40% of total US federal income taxes.  Compare this to the previous two answers.",
		more: ""
	},
	82: {
		question: "What percentage of the total personal net worth in the US is owned by the top 10% of households?",
		choices: {
			a: "10%",
			b: "30%",
			c: "50%",
			d: "70%",
			e: "90%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "Most of the personal wealth in the US is owned by the top 10%.",
		more: ""
	},
	83: {
		question: "What percentage of total US federal income taxes do the top 10% of wage earners pay?",
		choices: {
			a: "10%",
			b: "30%",
			c: "50%",
			d: "70%",
			e: "90%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "The top 10% pay 70% of total US income taxes.  The top 10% pretty much fund the US government.",
		more: ""
	},
	84: {
		question: "What percentage of total US federal income taxes do the top 50% of wage earners pay?",
		choices: {
			a: "55%",
			b: "65%",
			c: "75%",
			d: "85%",
			e: "95%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "This is about $1 trillion (from IRS 2010 statistics).  Social Security taxes bring in another trillion.",
		more: ""
	},
	85: {
		question: "What percent of US income earners did not pay income taxes in 2010?",
		choices: {
			a: "5%",
			b: "15%",
			c: "25%",
			d: "35%",
			e: "45%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "However, this figure does not include payroll taxes such as Social Security.  If it did it would be a much closer to zero.",
		more: ""
	},
	86: {
		question: "What percent of US income earners did not pay income taxes in 1994?",
		choices: {
			a: "5%",
			b: "15%",
			c: "25%",
			d: "35%",
			e: "45%"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "More people paid income taxes back then because people made more money.  The poor economy has had a big impact.",
		more: ""
	},
	87: {
		question: "To be in the top 50% of wage earners in the US you must make more than:",
		choices: {
			a: "$42,000",
			b: "$67,000",
			c: "$115,000",
			d: "$160,000",
			e: "$380,000"
		},
		answer: "a",
		difficulty: 3,
		category: "budget",
		explanation: "That's per year of course.  Of all wage earners, half make above $42,000 and half make below that amount.",
		more: ""
	},
	88: {
		question: "To be in the top 25% of wage earners in the US you must make more than:",
		choices: {
			a: "$42,000",
			b: "$67,000",
			c: "$115,000",
			d: "$160,000",
			e: "$380,000"
		},
		answer: "b",
		difficulty: 3,
		category: "budget",
		explanation: "In other words, 25% of all wage earners make above $67,000 and 75% make less.",
		more: ""
	},
	89: {
		question: "To be in the top 10% of wage earners in the US you must make more than:",
		choices: {
			a: "$42,000",
			b: "$67,000",
			c: "$115,000",
			d: "$160,000",
			e: "$380,000"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "So the top 10% of wage earners can buy a Corvette ZR1 with one year's salary if they give the IRS the finger.",
		more: ""
	},
	90: {
		question: "To be in the top 5% of wage earners in the US you must make more than:",
		choices: {
			a: "$42,000",
			b: "$67,000",
			c: "$115,000",
			d: "$160,000",
			e: "$380,000"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "At 5% you can blow off taxes and child support to buy an Aston Martin Vantage.",
		more: ""
	},
	91: {
		question: "To be in the top 1% of wage earners in the US you must make more than:",
		choices: {
			a: "$42,000",
			b: "$67,000",
			c: "$115,000",
			d: "$160,000",
			e: "$380,000"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "If you made it to the 1%, congratulations.  Thanks to the \"Occupy\" movement you're now lumped in with the Wall Street folk (if you're not already).",
		more: ""
	},
	92: {
		question: "If you make $50,000 a year in Atlanta, how much would you need to make for the same lifestyle in Manhattan?",
		choices: {
			a: "$30,000",
			b: "$50,000",
			c: "$70,000",
			d: "$90,000",
			e: "$110,000"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "So this means if live in Manhattan you have to be in the top 10% of wage earners nationwide in order to have the same lifestyle as someone who makes $50,000 in Atlanta.  No wonder people in New York are so grumpy!",
		more: ""
	},
	93: {
		question: "On average something that costs one dollar in 1993 would cost how much in 2011?",
		choices: {
			a: "$1",
			b: "$1.25",
			c: "$1.50",
			d: "$1.75",
			e: "$2"
		},
		answer: "c",
		difficulty: 3,
		category: "budget",
		explanation: "This means if you made $50,000 in 1993 you need to be making $75,000 now to have the same lifestyle.  Inflation has increased the cost of living by 50%.  Good luck keeping up with that!",
		more: ""
	},
	94: {
		question: "What is the current US federal minimum wage?",
		choices: {
			a: "$4.25",
			b: "$5.25",
			c: "$6.25",
			d: "$7.25",
			e: "$8.25"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "At minimum wage you're making a little over $15,000 a year assuming 40 hour weeks and a week of vacation.",
		more: ""
	},
	95: {
		question: "What was the US federal minimum wage in 1993?",
		choices: {
			a: "$4.25",
			b: "$5.25",
			c: "$6.25",
			d: "$7.25",
			e: "$8.25"
		},
		answer: "a",
		difficulty: 3,
		category: "budget",
		explanation: "That's about $9,000 a year, but since we know that $1 in 1993 is worth $1.50 now that amount is equivalent $13,000 today.  So, minimum wage earners are doing better today ($15,000/year) than they were in 1993.",
		more: ""
	},
	96: {
		question: "What percentage of 2010 US government revenue came from income and payroll taxes (including social security, Medicare, etc.)?",
		choices: {
			a: "50%",
			b: "60%",
			c: "70%",
			d: "80%",
			e: "90%"
		},
		answer: "d",
		difficulty: 3,
		category: "budget",
		explanation: "The rest came from corporate taxes, tariffs, etc.",
		more: ""
	},
	97: {
		question: "What percentage of 2010 US government revenue came from corporate taxes?",
		choices: {
			a: "10%",
			b: "20%",
			c: "30%",
			d: "40%",
			e: "50%"
		},
		answer: "a",
		difficulty: 3,
		category: "budget",
		explanation: "Corporations don't pay as much tax as individuals, but US corporate tax rates are about the same as the other large economies in the world.",
		more: ""
	},
	98: {
		question: "We've looked at how the US federal government raises money and how it spends it.  Now let's take a closer look at the actual debt.  How much of US debt is owed to foreign entities (governments, companies, and individuals)?",
		choices: {
			a: "10%",
			b: "30%",
			c: "50%",
			d: "70%",
			e: "90%"
		},
		answer: "d",
		difficulty: 4,
		category: "budget",
		explanation: "By comparison almost all of Japan's government debt is owned by residents of Japan.",
		more: ""
	},
	99: {
		question: "Assuming the same amount of US government spending, how much would total tax revenues have to increase to balance the yearly budget?",
		choices: {
			a: "20%",
			b: "30%",
			c: "40%",
			d: "50%",
			e: "60%"
		},
		answer: "e",
		difficulty: 3,
		category: "budget",
		explanation: "US tax receipts in 2010 was almost $2.2 trillion.  The deficit was $1.3 trillion.  Getting from 2.2 to 3.5 is a 60% increase in tax revenue if there are no spending cuts.  Ouch.",
		more: ""
	},
	100: {
		question: "Assuming the same amount of tax revenue, how much would the US government have to cut spending to balance the yearly budget?",
		choices: {
			a: "20%",
			b: "30%",
			c: "40%",
			d: "50%",
			e: "60%"
		},
		answer: "b",
		difficulty: 3,
		category: "budget",
		explanation: "That's $1.3 trillion, or Social Security, Medicare, and Medicaid combined.  Congratulations, you've just finished the 100 question sprint!That's $1.3 trillion, or Social Security, Medicare, and Medicaid combined.  Congratulations, you've just finished the 100 question sprint and are well equiped for November!",
		more: ""
	},
}