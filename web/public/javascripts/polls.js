/**
 * @file polls.js
 * @author Alfred Tom
 * 
 *  (c) 2011 Alfred Tom
 */

RR.Polls = {
	// status fields
	pollStatusArray: null,	// this is an array of elements- one element per poll.  Each element stores the status for the corresponding poll.
	madeFirstVote: 0,		// 0:  haven't voted yet, 1:  made a vote and went through the intro
	
	// functions and methods
	Init: function()
	{
		// RR.Polls field setup
		RR.Polls.pollStatusArray = new Array();

		// UI setup
		$("#pollcontent").attr("data-theme", "a");
		$("#pollquestions").css("min-height", $(window).height()-180);
		
		// enable the radio buttons in case they aren't
		$("#yeschoice1").removeAttr("disabled");
		$("#yeschoice1").removeAttr("checked");
		$("#nochoice1").removeAttr("disabled");
		$("#nochoice1").removeAttr("checked");
		
		// enable nav bar functionality.  *** eventually move "bind" to the href for performance
		//$("#tab1").bind("vmouseup", function() {
		//	RR.Polls.TabChoose(1);
		//});
		$("#tab2").bind("vmouseup", function() {
			RR.Polls.TabChoose(2);			
		});
		$("#tab3").bind("vmouseup", function() {
			RR.Polls.TabChoose(3);			
		});
		$("#tab4").bind("vmouseup", function() {
			RR.Polls.TabChoose(4);			
		});
		
		// create the first tab's poll here
		RR.Polls.pollStatusArray[1] = {
			//showMoreText: "show results",
			//hideMoreText: "hide results",
			//moreResultsStatus: 0,
			currentPositionStatus: 0,		// The current voting position.  0: not votedyet, 1: first option, 2:  second option
			currentPositionAmount: 0,		// amount of the position
			1: "Yes",						// the text of the first option
			2: "No",						// the text of the second option
		}

		// bind functions to UI elements
		$("#votebuttons1").bind("change", function(event){
			RR.Polls.HandlePrediction(event);
		});
		
		// hide the navbar at first to concentrate user on the first prediction
		$("#pollnavbar").hide();
		
		// hide all the betting fields
		$(".positionlabel").hide();
		$(".betcontainer").hide();
		$(".detailscontainer").hide();
		$(".positionsummary").hide();
		//$(".betfirst").hide();
		
		// only show first tab content for now
		$("#tab2content").hide();
		$("#tab3content").hide();
		$("#tab4content").hide();
		
	},
	
	// The first prediction made introduces the betting paradigm
	HandlePrediction: function(event)
	{
		var index = parseInt(event.currentTarget.id.substring(11));	// strip the "votebuttons" from the currentTarget's id to get the index
		var yesorno = $("input[name=votechoice" + index + "]:checked").val();	// get the vote
		
		// update the state variables
		RR.Polls.pollStatusArray[index].currentPositionStatus = ((yesorno == "yes") ? 1 : 2);	// save it in status
		
		$("#betexplanation" + index).append($("#votebox" + index));
		//$("#votebox" + index).insertAfter("#predictionheader" + index);
		$("#currentvote" + index).text($("input[name=votechoice" + index + "]:checked").val());	// set the "currentvote" value
		RR.Polls.ShowBetting(index);

		// remove HandlePrediction from vote buttons
		$("#votebuttons1").unbind();
		
		// bind new function to vote buttons
		$("#votebuttons1").bind("change", function(){
			//$("#currentvote1").text($("input[name=votechoice1]:checked").val());
			RR.Polls.ShowBetting(index);		
		});
		
		if (!RR.Polls.madeFirstVote) {
			$('#fade').fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 
	        $('#popupcontainer').show();
		}
	},
	
	// goes between the different poll tabs
	TabChoose: function(index)
	{
		var i;
		for (i = 1; i <= 4; i++) {
			if (i == index) {
				$("#tab" + i + "content").show();
			}
			else {
				$("#tab" + i + "content").hide();
			}
		}
		
		// show poll of the day as well
		if (index == 2) $("#tab1content").show();
	},
	
	// called when user hits the hide/show results link
	/*ToggleBetting: function(index)
	{
		if (RR.Polls.pollStatusArray[index].moreResultsStatus) {
			RR.Polls.HideBetting(index);
			RR.Polls.pollStatusArray[index].moreResultsStatus = 0;
		} else {
			RR.Polls.ShowBetting(index);
			RR.Polls.pollStatusArray[index].moreResultsStatus = 1;
		}
	},*/
	
	// shows the results graphs and either betting window or position window
	ShowBetting: function(index)
	{
		// get the choice from the radio cluster
		//var voteChoice = $("#betchoice" + index).val();
		
		// change the show/hide link and update the show status
		//$("#resultstext" + index).text(RR.Polls.pollStatusArray[index].hideMoreText);
		//RR.Polls.pollStatusArray[index].moreResultsStatus = 1;
		
		// show the bet and details container
		$("#betcontainer" + index).show("slow");
		$("#detailscontainer" + index).show("slow");

		// show the bet first/betting window/position window
		/*if ($("input[name=votechoice1]:checked").val() == null) {
			// hasn't even voted yet so just show the bet first text
			$("#betform" + index).hide();
			$("#changeform" + index).hide();
			$("#betfirst" + index).show();
		} else */
		if (!RR.Polls.pollStatusArray[index].currentPositionAmount) {
			// player hasn't bet yet so show betting window
			var currentVotePosition = $("input[name=votechoice" + index + "]:checked").val();
			//if (currentVotePosition == "Yes") $("#changepositiontext" + index).text("No");
			//else $("#changepositiontext" + index).text("Yes");
			
			//$("#betfirst" + index).hide();
			$("#changeform" + index).hide();
			//$("#positionlabel" + index).show();
			//$("#betform" + index).show();
			$("#betcontainer" + index).show("slow");

			//$(".currentvote1").text(currentVotePosition);
			if (currentVotePosition == "Yes") {
				$(".payout" + index).text(154);
				$("#historytext" + index).text("% Yes");
				$("#historygraph" + index).attr("src", "images/historygraphyes.png");
			} else {
				$(".payout" + index).text(286);
				$("#historytext" + index).text("% No");
				$("#historygraph" + index).attr("src", "images/historygraphno.png");
			}
			//$("#betchoice" + index).change(function() {
			//		$(".payout" + index).text(voteChoice);});
		} else {
			// player already bet so show cash out form 
			$("#betform" + index).hide();
			$("#changeform" + index).show();
			$("#betcontainer" + index).show("slow");
		}
		
	},
	
	// hides the results graphs and betting window
	HideBetting: function(index)
	{
		//$("#resultstext" + index).text(RR.Polls.pollStatusArray[index].showMoreText);
		$("#betcontainer" + index).hide("slow");	
	},
	
	PlaceBet: function(index)
	{
		// store the current voting position and bet amount
		var currentVotePosition = $("input[name=votechoice" + index + "]:checked").val();
		//var currentBetAmount = parseInt($("#betchoice" + index + " option:selected").text());
		var currentPayout = parseInt($("#payout" + index).text());
		
		// first update the current position text to reflect the bet
		//$("#betamount" + index).text(currentBetAmount);
		$("#positionvote" + index).text(currentVotePosition);
		if (currentVotePosition == "Yes") $("#positionvote" + index).css("color", "green");
		else $("#positionvote" + index).css("color", "red");
		$("#winamount" + index).text(currentPayout);
		
		// get and insert market value (*** eventually should be moved into a periodic background polling function)
		var marketValue = 110;	// *** hard code for now, but get from server in future
		$("#marketvalue" + index).text(marketValue);
		if (100 < marketValue) $("#marketvalue" + index).css("color", "green");
		else if (100 > marketValue) $("#marketvalue" + index).css("color", "red");

		$("#positionsummary" + index).show();
		
		// update the state of the object
		RR.Polls.pollStatusArray[index].currentPositionAmount = 100;
		//RR.Polls.pollStatusArray[index].showMoreText = "show details";
		//RR.Polls.pollStatusArray[index].hideMoreText = "hide details";
		//$("#resultstext" + index).text(RR.Polls.pollStatusArray[index].showMoreText);
		
		//RR.Polls.pollStatusArray[index].moreResultsStatus = 0;		
		//$("#betfirst" + index).hide();
		$("#betform" + index).hide();

		// diable the radio buttons
		$("#yeschoice" + index).attr("disabled", "disabled");
		$("#nochoice" + index).attr("disabled", "disabled");
		RR.Polls.HideBetting(index);

		// decrease the coin balance if not first bet
		if (RR.Polls.madeFirstVote) {
			var $coinElement = $("#topCoins");
			var newBalance = parseInt($coinElement.text()) -100;
			$coinElement.text(newBalance.toString());
			
		} else {
	        RR.Polls.madeFirstVote = 1;
	        $("#betcost" + index).text("Cost: 100");
			$("#pollnavbar").show();
			$("#tab2content").show();
		}
	},
	
	CashOut:  function(index, value)
	{
		// first increase the coin balance
		var $coinElement = $("#topCoins");
		var newBalance = parseInt($coinElement.text()) + value;
		$coinElement.text(newBalance.toString());
		
		// throw up the popup
    	$("#popuptext").text("Congratulations!  You made a profit of 10 coins on your 100 coin bet!");		
		$('#fade').fadeIn();
        $('#popupcontainer').show();
		
		RR.Polls.ResetPosition(index);
	},
	
	// this removes all state so that it looks like the user never voted on the poll
	ResetPosition: function(index) {
		// remove the current position
		RR.Polls.pollStatusArray[index].currentPositionStatus = 0;
		RR.Polls.pollStatusArray[index].currentPositionAmount = 0;
		$("#positionsummary" + index).hide();
		
		//RR.Polls.pollStatusArray[index].showMoreText = "show results";
		//RR.Polls.pollStatusArray[index].hideMoreText = "hide results";
		//$("#resultstext" + index).text(RR.Polls.pollStatusArray[index].showMoreText);
		
		//RR.Polls.pollStatusArray[index].moreResultsStatus = 0;		
		$("#betform" + index).show();
		RR.Polls.HideBetting(index);

		// enable the radio buttons again
		$("#yeschoice" + index).removeAttr("disabled");
		$("#nochoice" + index).removeAttr("disabled");
		
		// move and reset them
		$("#yeschoice" + index).removeAttr("checked").checkboxradio("refresh");
		$("#nochoice" + index).removeAttr("checked").checkboxradio("refresh");
		$("#polldescription" + index).append($("#votebox" + index));
		
		// change the vote buttons function
		$("#votebuttons1").unbind();
		$("#votebuttons1").bind("change", function(event){
			RR.Polls.HandlePrediction(event);		
		});
		
	},
	
	ChangeBet:  function(index)
	{
		// store the current voting position and bet amount
		var currentBetAmount = parseInt($("#changechoice" + index + " option:selected").text());
		
		// first update the current position text to reflect the bet
		$("#betamount" + index).text(currentBetAmount);
		$("#winamount" + index).text($("#changechoice" + index).val());
		
		// get and insert market value (*** eventually should be moved into a periodic background polling function)
		var marketValue = currentBetAmount;	// *** hard code for now, but get from server in future
		$("#marketvalue" + index).text(marketValue);
		if (currentBetAmount < marketValue) $("#marketvalue" + index).css("color", "green");
		else if (currentBetAmount > marketValue) $("#marketvalue" + index).css("color", "red");
		else $("#marketvalue" + index).css("color", "#222");
				
		//RR.Polls.pollStatusArray[index].moreResultsStatus = 0;		
		RR.Polls.HideBetting(index);
		
	},
	
	/*ChangePosition: function(index) {
		var currentVotePosition = $("input[name=votechoice" + index + "]:checked").val();
		//alert(currentVotePosition);
		if (currentVotePosition == "Yes") {
			$("#nochoice1").attr("checked",true).checkboxradio("refresh");
			$("#yeschoice1").removeAttr("checked").checkboxradio("refresh");
		}
		else {
			$("#yeschoice1").attr("checked",true).checkboxradio("refresh");
			$("#nochoice1").removeAttr("checked").checkboxradio("refresh");
		}
		$("input[type='radio']").attr("checked",true).checkboxradio("refresh");
		//RR.Polls.ShowBetting(1);		
	},*/
	
    ClosePopup: function()
    {
        $('#popupcontainer').hide();
        $('#fade').hide();
    },
}
