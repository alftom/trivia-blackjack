/**
 * @file triviabj.js
 * @author Sherk Chung
 * 
 *  (c) 2011 Sherk Chung
 */

/*
 * Overall flow:
 *    (all JSON calls are GET to support x-domain queries)
 * 
 *    1. On app launch, check if "user" (device) is registered.
 *    2. If not registered make JSON call:
 *            - User/Register?uuid=1234&name=Joe%20User&zip=94111
 *                - If no uuid provided, generate one (i.e., use "tbj<userId>" or something)
 *                - Look up whether uuid is already registered
 *                - If registered:
 *                    - Add name and zip to user record if not already stored
 *                    - Return code 10 (user already registered) with user info (level, name, zip) and questions for the user level
 *                - If not registered:
 *                    - Register user (create row in Users table) 
 - Return code 1 (success) with questions for level 0
 *                - Response:
 *                    { 
 *                        code: 10,    // 10 == user already registered
 *                        msg: "User already registered",
 *                        userInfo: {
 *                            uuid: 1234,
 *                            level: 3,        // would be 0 if not already registered
 *                            coin: 35,        // last known balance, or the starting amount if not already registered
 *                            name: "Joe User",    
 *                            zip: "94111"    
 *                        },
 *                        questions: {
 *                            level: 3,
 *                            list: {
 *                                <questionId - e.g., 11>: { level:1, question: 'Who is the president of the US?', answer: 'b', choices: {a: 'Michelle Pfiefer', b:'Barack Obama', c:'Donald Trump', d:'Donna Summers', e:'Charlie Sheen'},
 *                                12: { level:1, question: 'How long is a presidential term?', answer: 'c', choices: {a: '1 year', b:'2 years', c:'4 years', d:'6 years', e:'8 years'},
 *                                13: { level:1, question: 'How many senators are there in Congress?', answer: 'a', choices: {a: '50', b:'100', c:'200', d:'400', e:'435'}
 *                            ]
 *                        }
 *                    }
 *    3. On levelup, make JSON call:
 *        - User/LevelUp?level=3&coin=2500&answers=1b2d3c
 *            - Verifies that:
 *                - the level provided (current user level) is the same as the user level in DB
 *                - the answers provided are all correct
 *                    - to shorten url lengh, answers are compacted into form <questionId><answer><questionId><answer>...
 *                    - e.g., 36c means the user answered question Id 36 with the answer "c"
 *                    - 36c37a38b means user answered question 36 with "c", question 37 with "a", and question 38 with "b"
 *                    - this has to be a GET call since POST doesn't work cross-domain  
 *                - (if either is false, then return error with no questions)
 *            - Increments user level by 1
 *            - Stores the user coin balance (maybe do a check to see if it's realistic based on balance from last level)
 *            - Returns the following response:
 *                {
 *                    code: 1, // 1 == no error, anything else signifies an error
 *                    msg: "Levelup successful",
 *                    questions: { ... }
 *                }
 *        
 */

/*
 var testOrder = [
 10: 10,
 11: 11,
 12: 12
 ];

 var testAnswers = [
 10: 'b',
 11: 'a',
 12 'c'
 ];
 */

try {

RR.TriviaBJ = {
	userKey : "rr.user",
	triviaKey : "rr.questions",
	orderKey : "rr.order",
	answersKey : "rr.answers",

	START : "state_start",
	QUESTION : "state_question",
	RESULT : "state_result",
	BETTING : "state_betting",
	LEVELUP : "state_levelup",
	currentState : "state_start",
	resetGame : false,

	// UI elements (can't assign them pre-dom-ready)
	$qCard : null,
	$scrollable : null,
	$cardContent : null,
	$question : null,
	$answers : null,
	$choices : null,
	$potChips : null,
	$betAmount : null,
	$betMsg : null,
	$chips : null,
	$notification : null,
	$notifTitle : null,
	$notifText : null,
	$notifButton: null,
	$dealButton : null,
	$changeBetButton : null,
	$chiparea : null,
	$betarea : null,
	$resultMsg : null,
	$showVX : null,
	$innerCard : null,

};

RR.TriviaBJ = $.extend( RR.TriviaBJ, {
	// local storage key, random chars to make it un-obvious as to what they are
	// to the casual user browsing localStorage 
	keys : [ RR.TriviaBJ.userKey,  RR.TriviaBJ.triviaKey,
			 RR.TriviaBJ.orderKey, RR.TriviaBJ.answersKey ],

	Init : function() {
		Log("RR.TriviaBJ.Init()");
		var _this = RR.TriviaBJ;

		//Set up dummy localStorage if not supported (will not store)
		if (!('localStorage' in window && window['localStorage'] != null)) {
			Log("   No local storage! Faking it...");
			window.localStorage = {}; // empty associative array
		}

		// cache all the UI elements that are needed on a frequent basis
		_this.$headerCoins  = $("#balance");	// coin balance in header
		_this.$headerLevel	= $("#level"); 		// level in header
		_this.$headerLevelText	= _this.$headerLevel.children("div.level"); // level text in header
		_this.$headerProgressBar= _this.$headerLevel.children("div.XP"); // level text in header
		_this.$qCard 		= $("#qcard");
		_this.$cardContent	= $("#cardContent");
		_this.$scrollable	= _this.$cardContent.closest("div.scrollable"); // closest parent
		_this.$question		= $("span.question-text", "#question");
		_this.$answers		= $("#answers");
		_this.$choices 		= {
			a : $("div.answer-text.a", _this.$answers),
			b : $("div.answer-text.b", _this.$answers),
			c : $("div.answer-text.c", _this.$answers),
			d : $("div.answer-text.d", _this.$answers),
			e : $("div.answer-text.e", _this.$answers)
		};
		_this.$showVX = {
			a : _this.$choices.a.next(),
			b : _this.$choices.b.next(),
			c : _this.$choices.c.next(),
			d : _this.$choices.d.next(),
			e : _this.$choices.e.next()
		};

		_this.$potChips		= $("#potchips"); // chips that appear in bet area when wager is set
		_this.$maxBet		= $("#maxbet");
		_this.$betAmount	= $("#betamount");
		_this.$betMsg		= $("#warningMsg"); // Status bar for messages to user
		_this.$chips		= $("#chips").find("div.chip"); // chips used to increase wager by 1,5,25,etc.
		_this.$notification = $("#notification");
		_this.$notifTitle	= $("div.notificationTitle", _this.$notification);
		_this.$notifText	= $("div.notificationText", _this.$notification);
		_this.$splashImg 	= $("div.splash", _this.$notification);	// splash screen
		_this.$notifButton	= $("a", _this.$notification );
		_this.$resultMsg	= $("#resultMsg");
		_this.$dealButton	= $("#dealbutton");
		_this.$clearButton	= $("#clearbutton");
		_this.$changeButton	= $("#changebutton");
		_this.$chiparea		= $("#chiparea");
		_this.$betarea		= $("#betarea");
		_this.$innerCard	= $("#innercard_id");
		_this.$placeBets	= $("#placeBets");
		_this.$socialMedia	= $("#socialMediaContainer");

		// Highlight button on fast click
		_this.$answers.children("a").bind( "vmouseup", function(event) {
			var $t = $(this);
			$t.removeClass("ui-btn-up-c").addClass( "ui-btn-down-b " );
			_this.ProcessAnswer($t.attr( "choice" ));
			setTimeout(function() {
				$t.removeClass( "ui-btn-down-b" ).addClass( "ui-btn-up-c" );
			}, 100);
		});

		//_this.Load(); // Load on creation of object

		// Check to see if we have user info and matching level questions
		var userInfo = _this[_this.userKey];

		var questions = _this[_this.triviaKey];
		if (!userInfo || !questions || !(userInfo.uuid && userInfo.level == questions.level)) {
			Log("   No cached userInfo or questions found, useInfo: ", userInfo, "questions: ", questions);
			_this.Register(_this.Start, RR.Page.Error); // Register user/device if cached info not present
		} else {
			if (userInfo.coin <= 0) {
				_this.Register(_this.Start, RR.Page.Error); // Register user/device if cached info not present
			} else {
				_this.Start();
			}
		}
	},
	
	Splash: function()
	{
		Log("RR.TriviaBJ.Splash()");
		var _this = RR.TriviaBJ;
		
	},
	
	Start : function() {
		Log("RR.TriviaBJ.Start()");
		var _this = RR.TriviaBJ;

		// Welcome user
		_this.Notification("" /*"Trivia Blackjack"*/, "",
//			"Welcome. Answer correctly and watch your coins grow.  You might be surprised at how much you know, and how much you don't.",
			"Start",
			function() {
				try {
					Log("---> Notification CB !! ");
					_this.$headerCoins.show();
					_this.$headerLevel.show();
	
					Log( "Notif widgets: ", _this.$notification, _this.$notifTitle, _this.$notifText, _this.$splashImg );
	
					_this.$notifTitle.show();
					_this.$notifText.show();
					_this.$splashImg.hide();
	
					// Set level and balance on UI
					// TODO: Make this an XP progress bar instead of a level bar
					var userLevel = _this[_this.userKey].level;
					_this.SetLevel();
					_this.SetBalance();
					_this.MaxBet(userLevel*1000);
					_this.AddWager( 0 , true );
	
					if ( _this[_this.userKey].level == 0) {
						_this.$notification.parent().hide();
						_this.NextQuestion();
					} else {
						_this.$notification.parent().hide();
						_this.Continue();
					}
				} catch(e) {alert(e);}
		});
		
		_this.$notifTitle.hide();
		_this.$notifText.hide();

		// Hide innercard
		_this.$innerCard.hide();

		_this.$headerCoins.hide();
		_this.$headerLevel.hide();

		//_this.$socialMedia.hide();

		_this.$dealButton.hide();
		_this.$clearButton.hide();
		_this.$changeButton.hide();

		RR.Page.$viewPort.show();
	},
	
	MaxBet : function( maxBet )
	{
		Log("RR.TriviaBJ.MaxBet(", maxBet, ")");
		var _this = RR.TriviaBJ;
		
		if ( typeof( maxBet ) == 'undefined' ) {
			return( _this[_this.userKey].maxBet );
		} 
		_this[_this.userKey].maxBet = maxBet;
		_this.$maxBet.html(maxBet); // in header
	},

	SetBalance : function() {
		Log("RR.TriviaBJ.SetBalance()");
		var _this = RR.TriviaBJ;
		var coinBalance = _this[_this.userKey].coin;
		_this.$headerCoins.html(coinBalance); // in header
	},
	
	// Only updates the level if 'level' is supplied, otherwise just updates the XP
	SetLevel : function( level )
	{
		Log("RR.TriviaBJ.SetLevel(", level, ")");
		var _this = RR.TriviaBJ;

		// Find out what percentage
		var numTotalQuestions = Object.keys(_this[_this.triviaKey].list).length;
		var numLeftQuestions = _this[_this.orderKey].length;
		var percentComplete = (( numTotalQuestions - numLeftQuestions ) * 100 ) / numTotalQuestions;
		Log( "   total: ", numTotalQuestions, "left: ", numLeftQuestions, " percent: ", percentComplete );
		
		// Set the percentage on the UI
		_this.$headerProgressBar.html( percentComplete.toFixed(0) + "%" );

		// Now show the progress bar 
		var barLevel = ( Math.floor( percentComplete / 10 ) * 10 );
		Log("   showing level bar # ", barLevel );
		_this.$headerProgressBar.css("background-image", "url(./images/progress_bar_" + barLevel + ".png)");
		
		if ( typeof( level ) == 'number' || typeof( level ) == 'string' ) {
			_this.$headerLevelText.html( "Level "+level );
		}
	},

	RefreshScrollable : function() {
		Log("RR.TriviaBJ.RefreshScrollable()");
		var _this = RR.TriviaBJ;
		if (RR.Device.isMobile) {
			_this.$scrollable.touchScroll(); // Update scrollable height
		}
	},

	OrientationChanged : function() {
		Log("RR.TriviaBJ.OrientationChanged()");
		var _this = RR.TriviaBJ;
		if (_this.currentState == _this.QUESTION) {
			if (RR.Page.IsLandscape()) {
				_this.$betarea.hide();
			} else {
				_this.$betarea.show();
			}
		}

	},

	Continue : function() {
		Log("RR.TriviaBJ.Continue()");
		var _this = RR.TriviaBJ;

		return( _this.NextQuestion() );
	},

	ShowModalWindow : function(msg, w, h) {
		Log("RR.TriviaBJ.ShowModalWindow()");
		var _this = RR.TriviaBJ;

		betMsg = msg;
		_this.$betMsg.html(betMsg);
		_this.$betMsg.show();

	},

	ShowBetScreen : function() {
		Log("RR.TriviaBJ.ShowBetScreen()");
		var _this = RR.TriviaBJ;
		var currWager = parseInt(_this.$betAmount.text());
		var coinBalance = _this[_this.userKey].coin;

		_this.currentState = _this.BETTING;

		if (currWager > coinBalance) {
			_this.ClearWager(false);
		}

		_this.$notification.parent().hide(); // Hide notification

		_this.$question.parent().hide();
		_this.$answers.hide();
		_this.$cardContent.show(); // Show the card content        

		_this.$placeBets.show(); // Show "Place your bets" msg

		_this.$innerCard.addClass('flipAnimation').show();

		_this.$resultMsg.hide();

		_this.$dealButton.show();
		_this.$changeButton.hide();
		_this.$clearButton.show();

		_this.EnableBettingChips(); // Enable betting chips        
		_this.ShowChipArea();
		_this.$betarea.show();

		/*
		if (currWager <= 0) {
			_this.$dealButton.addClass("ui-disabled");
		}
		*/

		var order = _this[_this.orderKey];
		if (typeof (order) != "object")
			return (void (0)); // TODO: display error msg, refresh questions

		// Check if there are any questions left
		var orderLen = order.length;
		//Log("   order: ", order, ", length: ", orderLen);
		if (orderLen <= 0) {
			Log("   No more questions, leveling up from ShowQuestion!");
			_this.LevelUp();
		}
	},

	// Deals the next question
	DealQuestion : function() {
		Log("RR.TriviaBJ.DealQuestion()");
		var _this = RR.TriviaBJ;
		try {
			var userInfo = _this[_this.userKey];
			var order    = _this[_this.orderKey];
			if (typeof (order) != "object") {
				Log("   order of wrong type, aborting! ", order);
				return (void (0)); // TODO: display error msg, refresh questions
			}
	
			// Check if there are any questions left
			//Log("   order: ", order, ", length: ", Object.keys(order).length);
			var orderLen = order.length;
			if (orderLen <= 0) {
				Log("   No more questions, leveling up from DealQuestion!");
				_this.LevelUp();
				return (void (0)); // because this method is called in href
			}
	
			// get first element of array
			var qIndex = 0;
			if ( userInfo.level > 7 ) {
				// Show random question
				//qIndex = Math.floor(orderLen * Math.random());
			}
			var nextQuestionId = order[ qIndex ];

			var question = _this[_this.triviaKey].list[nextQuestionId];
			Log("   pulling up question in index: ", qIndex);
			Log("   question #", nextQuestionId, ": ", question);
	
			// Populate UI with question
			_this.$question.html(question.question);
			_this.$question.jqmData("qid", nextQuestionId); // store questionId
	
			_this.$qCard.show(); // Show question
			_this.ShowChoices(nextQuestionId); // populate answser choices
	
			_this.RefreshScrollable();
	
			return (void (0)); // because this method is called in href
		} catch(e) { alert(e); }
	},

	ShowChoices : function(questionId) {
		Log("RR.TriviaBJ.ShowChoices(", questionId, ")");
		var _this = RR.TriviaBJ;

		if (!questionId)
			questionId = _this.$question.jqmData("qid");
		var question = _this[_this.triviaKey].list[questionId];

		// populate UI with answer choices
		for (choice in question.choices) {
			var choiceText = question.choices[choice];
			var choiceLength = choiceText.length | 0;
			if (choiceLength) {
				_this.$choices[choice].html(choiceText);
				_this.$choices[choice].closest("a").show();
				_this.$choices[choice].closest("a").removeClass("right wrong").css({opacity : 1});
				//_this.$choices[ choice ].closest("a").button( "enable" );
			} else {
				_this.$choices[choice].closest("a").hide();
				//_this.$choices[ choice ].closest("a").button( "disable" );
				Log("   hiding choice ", choice, ": length: ",choiceLength, " : ", choiceText);
			}
		}
	},

	// Closes correct/wrong dialog and moves on to next question
	NextQuestion : function(id) {
		Log("RR.TriviaBJ.NextQuestion(", id, ")");
		var _this = RR.TriviaBJ;
		var userInfo = _this[_this.userKey];
		
		/*
		if (userInfo.level > 0 && parseInt(_this.$betAmount.html()) <= 0) {
			return;
		}
		*/

		_this.currentState = _this.QUESTION;

		_this.$question.parent().show();
		_this.$answers.show();

		_this.$placeBets.hide();

		_this.$notification.parent().hide(); // Hide notification
		_this.$cardContent.show(); // Show the card content

		_this.$innerCard.show();

		_this.$resultMsg.hide();
//		_this.$socialMedia.hide();

		_this.$dealButton.hide();
		_this.$changeButton.hide();
		_this.$clearButton.hide();

		// hide bet area when showing the answer choice screen
		_this.$potChips.children().removeClass("chip1-toss-in chip1-toss-out");
		_this.$potChips.children().removeClass("chip5-toss-in chip5-toss-out");
		_this.$potChips.children().removeClass("chip10-toss-in chip10-toss-out");
		_this.$potChips.children().removeClass("chip25-toss-in chip25-toss-out");

		_this.$betarea.show();

		for ( var key in _this.$choices) {
			_this.$choices[key].closest("a").hide();
		}
		for ( var key in _this.$showVX) {
			_this.$showVX[key].hide();
		}

		_this.$question.parent().show();
		_this.$betMsg.hide();

		if (RR.Page.IsLandscape()) {
			_this.$betarea.hide();
			_this.$chiparea.hide();
		} else {
			_this.HideChipArea();
		}

		return (_this.DealQuestion());
	},

	HideChipArea : function() {
		Log("RR.TriviaBJ.HideChipArea()");
		var _this = RR.TriviaBJ;
		if (_this.$chiparea.is(':visible')) {
			_this.$chiparea.hide();

			//_this.$chiparea.addClass("chipAreaSlideDown").delay(400).queue(function() {
			//    $(this).removeClass("chipAreaSlideDown").hide();
			//    $(this).dequeue();
			//});
		}
	},

	ShowChipArea : function() {
		Log("RR.TriviaBJ.ShowChipArea()");
		var _this = RR.TriviaBJ;
		if (!_this.$chiparea.is(':visible')) {
			_this.$chiparea.addClass("chipAreaSlideUp").show()
					.delay(700).queue(function() {
						_this.$chiparea.removeClass("chipAreaSlideUp");
						_this.$chiparea.dequeue();
					});
		} else {
			Log("   ChipArea is already visible, ingoring...");
		}
	},

	EnableBettingChips : function() {
		Log("RR.TriviaBJ.EnableBettingChips()");
		var _this = RR.TriviaBJ;
		var userInfo = _this[_this.userKey];
		_this.EnableDisableChips(userInfo.maxBet, userInfo.coin);
	},

	EnableDisableChips : function( maxBet, amount) {
		Log("RR.TriviaBJ.EnableDisableChips(", maxBet, amount, ")");
		
		var _this = RR.TriviaBJ;

		_this.$chips.each( function() {
			var $t = $(this);
			var chipValue = parseInt($t.attr("value"));
			if (chipValue > maxBet || chipValue > amount) {
				$t.addClass("ui-disabled");
				Log("      chip value ", chipValue, " has been disabled ");
			} else {
				$t.removeClass("ui-disabled");
				Log("      chip value ", chipValue, " has been enabled");
			}
		});
	},

	// Adds an amount to the pot
	AddWager : function(amount, force) {
		Log("RR.TriviaBJ.AddWager(", amount, force, ")");
		var _this = RR.TriviaBJ;

		if ( ! force ) {
			if (_this.currentState != _this.BETTING ) {
				Log("   not betting state, ignoring click...");
				return (void (0));
			}
	
			// Check if chip is allowed to be used
			_this.$chips.each(function() {
				var $t = $(this);
				var chipValue = parseInt($t.attr("value"));
	
				if (chipValue == amount && $t.hasClass("ui-disabled")) {
					Log("   chips are disabled, ignoring click...");
					return( void(0) );
				}
			});
		}
		var userInfo = _this[_this.userKey];

		// Cap wager at max bet based on level
		var maxWager  = userInfo.maxBet;
		var currWager = parseInt(_this.$betAmount.text());
		Log("   currWager: " + currWager);

		var newWager = currWager + amount;
		var betMsg = "Place your bets...";

		if ( newWager > maxWager ) {
			Log("   total wager of ", newWager, " exceeds level, capping at ", userInfo.level);
			
			if ( currWager == maxWager ) {
				// If already at max, tell user they can't bet more.  
				betMsg = "Your total wager cannot exceed <b>"+maxWager+"</b> coins.";
				_this.Notification("Max Wager Exceeded", betMsg, "Continue", _this.ShowBetScreen );
				return( void(0) );
			} else {
				// If not at max yet, just cap it off at max
				amount   = maxWager - currWager;
				newWager = maxWager;
			}
		}

		// Cap wager at max bet based on available funds 
		if (newWager > userInfo.coin) {
			Log("   total wager of ", newWager, " exceeds coin balance, capping at ", userInfo.coin);

			if ( currWager == maxWager ) {
				// If already at max, tell user they can't bet more.  
				var plural = ( userInfo.coin > 1 )? "s" : "";
				betMsg = "Your wager cannot exceed your balance of <b>" + userInfo.coin + "</b> coin" + plural + ".";
				_this.Notification("Max Wager Exceeded", betMsg, "Continue", _this.ShowBetScreen );
				return( void(0) );
			} else {
				// If not at max yet, just cap it off at max
				amount   = userInfo.coin - currWager;
				newWager = userInfo.coin;
			}
			
			newWager = userInfo.coin;
			return (void (0));
		}
		_this.$betMsg.html(betMsg);

		_this.$betAmount.html(newWager);

		var maxBet = userInfo.coin - (newWager);

		// Grey out chips for bet amounts they can't afford
		Log("   enabling/disabling individual betting chips...");

		// _this.EnableDisableChips(userInfo.level, maxBet);  // not necessary here, is done at showbetscreen()

		_this.UpdatePotChips(amount);

		return (void (0));
	},

	ClearWager : function(enableAnimation) {
		Log("RR.TriviaBJ.ClearWager(", enableAnimation, ")");
		var _this = RR.TriviaBJ;
		if (_this.currentState != _this.BETTING) {
			Log("   not betting state, ignoring click...");
			return (void (0));
		}

		_this.$betMsg.show();
		_this.AddWager(-parseInt(_this.$betAmount.html()));
		if (enableAnimation) {
			_this.$potChips.children().addClass("chip1-toss-out").delay(500).queue(function() {
				_this.$potChips.empty();
				$(this).dequeue();
			});
		} else {
			_this.$potChips.empty();
		}

		/*
		var userInfo = _this[_this.userKey];
		var maxBet = userInfo.coin;
		_this.EnableDisableChips(userInfo.level, maxBet);  // not necessary here, done at showbetscreen()
		*/
		return (void (0));
	},

	UpdatePotChips : function(amount) {
		Log("RR.TriviaBJ.UpdatePotChips(", amount,")");
		var _this = RR.TriviaBJ;
		// Show/hide pot chips to match if there is a bet
		Log("   showing pot chips");
		var count = _this.$potChips.children().size();
		
		while ( amount > 0 ) {
			var chipAmount = 0;
			if ( amount >= 1000 && amount < 5000 ) chipAmount = 1000;
			else if ( amount >= 5000 && amount < 10000 ) chipAmount = 5000;
			else if ( amount >= 10000 && amount < 25000 ) chipAmount = 10000;
			else if ( amount >= 25000 ) chipAmount = 25000;
			amount -= chipAmount;
			var topPos = 5 + Math.random() * 25;
			var leftPos = 5 + Math.random() * 50;
			var className = "chip"+chipAmount/1000;

			_this.$potChips.append("<div class='chip " + className
					+ " "+className+"-toss-in' style='margin: " + topPos
					+ "px 0 0 " + leftPos + "px' value='"+chipAmount
					+ "'>" + chipAmount/1000
					+ "<div class='denom'>K</div>"
					+ "</div>");
			count++;
		}
		

		// try to convert to larger coin (if possible)
		var currentBetCoins = new Array();

		_this.$potChips.find("div.chip").each(function() {
			var $t = $(this);
			var chipValue = parseInt($t.attr("value"));
			currentBetCoins.push(chipValue);
		});
		var convertedBetCoins = _this.ConvertCoins(currentBetCoins);

		var converted = currentBetCoins.length != convertedBetCoins.length;
		for ( var i = 0; converted == false && i < currentBetCoins.length; ++i) {
			converted = currentBetCoins[i] != convertedBetCoins[i];
		}

		if (converted) {
			Log("   showing converted chips");
			_this.$potChips.empty();

			for ( var i = 0; i < convertedBetCoins.length; ++i) {
				var chipAmount = convertedBetCoins[i];
				var topPos = 5 + Math.random() * 25;
				var leftPos = 5 + Math.random() * 50;
				var className = "chip"+chipAmount/1000;
				_this.$potChips.append("<div class='chip " + className
						+ " "+className+"-toss-in' style='margin: " + topPos
						+ "px 0 0 " + leftPos + "px' value='"+chipAmount
						+ "'>" + chipAmount/1000
						+ "<div class='denom'>K</div>"
						+ "</div>");
			}
		}

		// done converting
		_this.$potChips.show();

		return (void (0));
	},

	CloseNotice : function() {
		Log("RR.TriviaBJ.CloseNotice()");
		var _this = RR.TriviaBJ;

		if (_this.resetGame == true) {
			location.reload(true);
			return;
		}
	},

	ConvertCoins : function(currentCoins) {
		Log("RR.TriviaBJ.ConvertCoins(", currentCoins, ")");
		var _this = RR.TriviaBJ;
		try { 
			var total = 0;
			for ( var i = 0; i < currentCoins.length; ++i)
				total += currentCoins[i];
	
			var requiredCoins25 = Math.floor(total / 25000);
			total = total % 25000;
			var requiredCoins10 = Math.floor(total / 10000);
			total = total % 10000;
			var requiredCoins5 = Math.floor(total / 5000);
			total = total % 5000;
			var requiredCoins1 = total/1000;
	
			var convertedCoins = new Array();
			for ( var i = 0; i < currentCoins.length; ++i) {
				var value = currentCoins[i];
				if (value == 25000 && requiredCoins25 > 0) {
					convertedCoins.push(value);
					requiredCoins25 -= 1;
				} else if (value == 10000 && requiredCoins10 > 0) {
					convertedCoins.push(value);
					requiredCoins10 -= 1;
				} else if (value == 5000 && requiredCoins5 > 0) {
					convertedCoins.push(value);
					requiredCoins5 -= 1;
				} else if (value == 1000 && requiredCoins1 > 0) {
					convertedCoins.push(value);
					requiredCoins1 -= 1;
				}
			}
	
			for ( var i = 0; i < requiredCoins25; ++i)
				convertedCoins.push(25000);
			for ( var i = 0; i < requiredCoins10; ++i)
				convertedCoins.push(10000);
			for ( var i = 0; i < requiredCoins5; ++i)
				convertedCoins.push(5000);
			for ( var i = 0; i < requiredCoins1; ++i)
				convertedCoins.push(1000);
	
			Log("   returning converted coins: ", convertedCoins);
			return convertedCoins;
		} catch(e) { alert(e); }
	},


	NotificationCB: function()
	{
		Log("RR.TriviaBJ.NotificationCB()");
		var _this = RR.TriviaBJ;
		if ( typeof( _this.notificationCallback ) == 'function' ) {
			Log("   executing callback");
			_this.notificationCallback();
		} else {
			Log("   no callback found, ignoring");
		}
		return( void(0) );
	},
	
	Notification : function(title, body, buttonTitle, buttonCallback ) {
		Log("RR.TriviaBJ.Notification('", title, body, buttonTitle, typeof( buttonCallback ) ,"')");
		var _this = RR.TriviaBJ;

		_this.$notifTitle.html(title);
		_this.$notifText.html(body);

//		_this.$dealButton.show();
//		_this.$clearButton.show();

		_this.$cardContent.hide();
		_this.$innerCard.hide();
		
		_this.$notifButton.find("span.ui-btn-text").html( buttonTitle );

		_this.$notification.parent().show();
		_this.$notification.parent().css({
			width: RR.Page.$viewPort.width(),
			top: (RR.Page.$viewPort.height() - _this.$notification.outerHeight()) / 3 + "px"
		});

		_this.RefreshScrollable();
		_this.notificationCallback = buttonCallback;
	},


	// Process user answer 
	ProcessAnswer : function(answer) {
		Log("RR.TriviaBJ.ProcessAnswer('", answer, "')");
		var _this = RR.TriviaBJ;
		try {
			if (_this.currentState != _this.QUESTION) {
				Log("   not in Question state, ignoring...");
				return (void (0));
			}
	
			_this.currentState = _this.RESULT;
	
			if (! RR.Page.IsLandscape()) {
				Log("   in portrait mode, showing bet area...");
				_this.$betarea.show();
			}
	
			// First check if answer is correct
			var correct;
			var questionId = _this.$question.jqmData("qid"); // retrieve stored qid
			Log("   Retrieved qid: ", questionId);
	
			var correctAnswer = _this[_this.triviaKey].list[questionId].answer;
			correctAnswer = correctAnswer.replace(" ", "");

			// remove question from the beginning of order list 
			_this[_this.orderKey].splice(0,1);

			if (answer != correctAnswer) {
				Log("   Wrong answer!");
				correct = -1;
				// Move question to end of order array so it's asked again later
				_this[_this.orderKey][_this[_this.orderKey].length] = questionId;
			} else {
				// Add to list of answers 
				Log("   Correct answer!");
				_this[_this.answersKey][questionId] = answer;
				_this.Store(_this.answersKey); // update answers in DB  
				correct = 1;
			}
			_this.Store(_this.orderKey); // update question order in DB

			// Apply wager 
			var wager = parseInt(_this.$betAmount.html());
			if (wager > 0) {
				var coin = _this[_this.userKey].coin += wager
						* correct;
				_this.Store(_this.userKey); // update user info in DB
				_this.$headerCoins.html(coin); // Update balance in header
	
			}
	
			// hide unselected answers 
			for ( var key in _this.$choices) {
				if (key != answer && key != correctAnswer)
					_this.$choices[key].closest("a").fadeOut();
			}
	
			// Display "good job" or "ehh!" to user
			var question = _this[_this.triviaKey].list[questionId];
	
			for ( var key in _this.$showVX) {
				_this.$showVX[key].removeClass("v");
				_this.$showVX[key].removeClass("x");
				_this.$showVX[key].show();
			}
	
			if (correct == 1) {
	
				var title = "V";
				//var title = "<strong>Correct Answer.</strong><br /> The answer is <b>" + answer.toUpperCase() + 
				//                "</b><br /><br />";
				/*var body  = "The answer is <b>" + answer.toUpperCase() + 
				                "</b>: " + question.choices[ answer ];*/
	
				_this.$choices[answer].closest("a").addClass("right");
				_this.$showVX[answer].addClass("v");
	
			} else {
				var title = "X";
				//var title = "<font style='color: red;'><strong>Wrong Answer.</strong><br /> The answer is NOT <b>" + answer.toUpperCase() + 
				//                "</b></font><br /><br />";
				/*var body  = "The answer is NOT <b>" + answer.toUpperCase() + 
				                "</b>: " + question.choices[ answer ];*/
	
				_this.$choices[answer].closest("a").addClass("wrong");
				_this.$showVX[answer].addClass("x");
	
				_this.$choices[correctAnswer].closest("a").addClass("right");
				_this.$showVX[correctAnswer].addClass("v");
	
			}
			
			if ( question.explanation ) _this.$resultMsg.html( question.explanation ).slideDown();
			
			_this.$innerCard.removeClass('flipAnimation');
	
			_this.$dealButton.show();
			if ( _this[_this.userKey].level > 0 ) _this.$changeButton.show();
			
			_this.$betMsg.html("");
			_this.$betarea.show();
	//		_this.$socialMedia.show();
	
			// TODO: Handle case where user runs out of money!
			var coinBalance = _this[_this.userKey].coin;
			
			// Update XP
			_this.SetLevel();
	
			return (void (0));
		} catch(e) {alert(e);}
	},

	Load : function(key) {
		Log("RR.TriviaBJ.Load(", key, ")");
		var _this = RR.TriviaBJ;

		// Load info from localStorage
		var keys = _this.keys; // if no keys supplied, load all keys
		if (key) { // turn key into array if not already
			if (typeof (key) != "object") key = [ key ];
			keys = key;
		}

		Log("   loading keys: ", keys, " of length ", keys.length);

		// Loads cached object into TriviaBJ.<key>, e.g., the cached object
		// with key "userInfo" would be loaded into TriviaBJ.userInfo
		for ( var i = 0; i < keys.length; i++) {
			Log("   now loading ", keys[i]);
			var cacheContents = window.localStorage[keys[i]];
			if (cacheContents) {
				var cachedObject = $.secureEvalJSON(cacheContents);
				if (cachedObject) {
					_this[keys[i]] = cachedObject;
					Log("   Loaded Cached ", keys[i], " : ", cachedObject);
				} else {
					Log("   No Cached ", keys[i], ", skipping...");
				}
			}
		}
	},

	Store : function(key) {
		Log("RR.TriviaBJ.Store(", key, ")");
		var _this = RR.TriviaBJ;

		// Store info to localStorage
		var keys = _this.keys; // if no keys supplied, store all keys
		if (key) { // turn key into array if not already
			if (typeof (key) != "object")
				key = [ key ];
			keys = key;
		}

		// Stores cached object from TriviaBJ.<key>, e.g., the cached object
		// TriviaBJ.userInfo would be stored with key "userInfo"
		for ( var i = 0; i < keys.length; i++) {
			var serializedObj = $.toJSON(_this[keys[i]]);
			if (serializedObj) {
				window.localStorage[keys[i]] = serializedObj;
				Log("   stored object ", keys[i], " as ",
						serializedObj);
			} else {
				Log("   object ", keys[i],
						" not found, skipping...");
			}
		}
	},

	// Given a list of questions, generates an order array
	GenerateOrder : function(questions) {
		Log("RR.TriviaBJ.GenerateOrder(", questions, ")");
		var _this = RR.TriviaBJ;
		var order = [];
		if (typeof (questions) != "object") return (order);

		for (questionId in questions) {
			order.push( questionId );
		}
		Log("   Order :", order);
		return (order);
	},

	// Calls server side API to register user/device, replaces the cached user 
	// info and questions with whatever the server returns (this should not be
	// called if the user info is already cached at the client)
	// onError should take an argument for the error text
	Register : function(onSuccess, onError) {
		Log("RR.TriviaBJ.Register(", onSuccess, ",", onError, ")");
		var _this = RR.TriviaBJ;

		// pass uuid to JSON API
		/*
		$.ajax({
		    url: RR.Page.AbsoluteURL("triviabj/register"),
		    dataType: 'jsonp',
		    crossDomain: true,
		    data: { uuid: RR.Device.uuid },
		    complete: function() { Log ("complete");},
		    success: function (json, status) {
		        Log("   $.ajax.success()");
		        if ( 1 == json.code || 10 == json.code ) {    // success

		            // Cache the user and info returned (device is registered as a user now)
		            _this[ _this.userKey ] = json.userInfo;
		            _this[ _this.triviaKey ] = json.questions;
		            _this[ _this.orderKey ] = _this.GenerateOrder( json.questions.list );  
		            _this[ _this.answersKey ] = [];  // empty
		            
		            RR.TrviaBJ.Store();    // replace all objects
		            Log("   Registered device, caching objects ", _this);

		            // Execute onSuccess callbak
		            if (typeof(onSuccess) == "function") onSuccess();
		            else Log("skipping onSuccess because it is a ", typeof(onSuccess));
		            
		        } else {    // Registration failed
		            // Execute error callback
		            if (typeof(onError) == "function") onError( json.msg );
		            else Log("skipping onError because it is a ", typeof(onError));
		        }
		    }, // success()
		    error: function (xhr, status ) {
		        // Execute error callback
		        Log("   $.ajax.success()");
		        if (typeof(onError) == "function") onError( status );
		        else Log("skipping onError because it is a ", typeof(onError));
		    } // error()
		    
		}); // ajax
		Log("   done ajax");
		 */
		/* For testing */
		_this[_this.userKey] = Test.userInfo;
		_this[_this.triviaKey] = Test[0];
		_this[_this.orderKey] = _this.GenerateOrder(Test[0].list);
		_this[_this.answersKey] = {}; // empty

		Log("   Registered device, caching objects ", _this);
		_this.Store(); // replace all objects

		// Execute onSuccess callbak
		if (typeof (onSuccess) == "function") onSuccess();
		else Log("skipping onSuccess because it is a ", typeof (onSuccess));
		/* end test code */
	},

	// Calls server side API to level user up and get next level questions
	LevelUp : function(onSuccess, onError) {
		Log("RR.TriviaBJ.LevelUp()");
		var _this = RR.TriviaBJ;

		// Generate answer string
		var answerString = "";
		var answers = _this[_this.answersKey];
		for (questionId in answers)
			answerString += questionId + answers[questionId];
		Log("   Answer string: ", answerString);

		// Generate data to pass to json call
		var data = {
			level : _this[_this.userKey].level,
			coin : _this[_this.userKey].coin,
			answers : answerString
		};
		// JSON call to level user up
		/*
		$.ajax({
		    url: RR.Page.AbsoluteURL("triviabj/levelup"),
		    dataType: 'jsonp',
		    crossDomain: true,
		    data: data,
		    success: function (json, status) {
		        if ( 1 == json.code ) {    // success

		            // Store the new questions returned
		            var level = json.questions.level
		            _this[ _this.userKey ].level = level;
		            _this[ _this.triviaKey ] = json.questions;
		            _this[ _this.orderKey ] = _this.GenerateOrder( json.questions.list );  
		            _this[ _this.answerKey ] = {};  // empty
		            
		            _this.Store();
		            Log("   Registered new level questions ", _this);
		            _this.Notification( "Congratulations", "You have reached <b>Level "+level+"</b>");
		            _this.$headerLevelText.html( level );

		            // Execute onSuccess callbak
		            if (typeof(onSuccess) == "function") onSuccess();
		            
		        } else {    // Registration failed
		            // Execute error callback
		            if (typeof(onError) == "function") onError( json.msg );
		        }
		    }, // success()
		    error: function (xhr, status ) {
		        // Execute error callback
		        if (typeof(onError) == "function") onError( status );
		    } // error()
		    
		}); // ajax
		 */
		/* For testing  */
		var level = _this[_this.userKey].level + 1;
		if (Test[level]) {
			_this[_this.userKey].level = level;
			_this[_this.triviaKey] = Test[level];
			_this[_this.orderKey] = _this.GenerateOrder(Test[level].list);
			_this[_this.answerKey] = {}; // empty
			
			var maxBet = level * 1000;

			_this.Store();
			Log("   Registered new level questions ", _this);
			var text = "You have reached <b>Level " + level
					+ "</b><br>"
					+ "<i>You can now bet up to <b>" + maxBet
					+ "</b> coins each hand.</i>";
			if (level == 1) {
				text += "<br>(you can modify your bet hitting 'Ok' and then using the betting chips at the bottom of the table)";
				//_this.$betWidgets.show();
				//_this.$chips.show();
				// _this.$chips.removeClass( "disabled" );
				//_this.EnableBettingChips(); // will enable/disable the right widgets
			}

			_this.Notification("Congratulations", text, "Ok", _this.ShowBetScreen );
			_this.currentState = _this.LEVELUP;

			// Set level and balance on UI
			_this.SetLevel( level );
			_this.SetBalance();
			_this.MaxBet(maxBet);

			if ( level == 1 ){
				_this.ShowChipArea(); // show the betting chips to show user what they look like
				_this.$changeButton.hide();
			}

			//_this.ClearWager(true);
			_this.AddWager(maxBet, true); // update bet to max wager
			
		} else {
			var text = "You've completed all the available questions, stay tuned for more to come!";
			_this.Notification("Congratulations", text,"Ok", _this.Continue );
			_this.$dealButton.hide();
			_this.$notifButton.hide();

		}
		/* End test code */
	}

});

//-----------------( on BodyLoad )-------------------

function GlobalInit() {
	Log("Global Init");
	// For testing
	/*
	RR.Device.isPC = false;
	RR.Device.isApple = false;
	RR.Device.isMobile = true;
	// End test code */
	Log("RR:", RR);
	Log("RR.Page", RR.Page);
	RR.Page.Init(); // caches frequently used $jquery elements, calls Page.Resize()

	Log("Binding pageshow");
	$("div[data-role='page']").live(
			"pagecreate",
			function() {
				//$("div[data-role='page'], div[data-role='dialog']", RR.Page.$viewPort).live('pageshow', RR.ViewPort.PageShow); 
				$("div[data-role='page'], div[data-role='dialog']").live(
						"pageshow", RR.ViewPort.PageShow);
			});
	RR.TriviaBJ.Init();

	// Give the "loading" icon a little color 
	$("div.ui-loader").addClass("ui-btn-active");
};

} catch (e) { alert(e); }