var lbTestData = {
	userInfo: {
		uuid: 1234,
		level: 10,
		kp: 2932,
		coin: 128,
		name: "Joe User",
		correct: 45,
		incorrect: 20,
		accuracyPercentile: "56%",
	},
	friendsList: [
		{position: 5, name: "Santa Claus", kp: 3948},
		{position: 6, name: "Mickey Mouse", kp: 3290},
		{position: 7, name: "Sheik Yaboodi", kp: 2988},
		{position: 8, name: "Joe User", kp: 2932},
		{position: 9, name: "Ben Dover", kp: 2891},
		{position: 10, name: "Chump Change", kp: 2218},
		{position: 11, name: "Agent Smart", kp: 1893},
		{position: 12, name: "Howie Long", kp: 1234},
		{position: 13, name: "Forrest Gump", kp: 982},
		{position: 14, name: "A Rock", kp: 12},
	],
	worldList: [
		{position: 38929, name: "Marty Funk", kp: 2985},
		{position: 38930, name: "Whacha Name", kp: 2983},
		{position: 38931, name: "Ho Dover", kp: 2976},
		{position: 38932, name: "May Eye", kp: 2954},
		{position: 38933, name: "Joe User", kp: 2932},
		{position: 38934, name: "Jennifer Booze", kp: 2918},
		{position: 38935, name: "Juan Delacruz", kp: 2901},
		{position: 38936, name: "George Custanza", kp: 2896},
		{position: 38937, name: "Frodo Baggins", kp: 2885},
		{position: 38938, name: "Luke Skywalker", kp: 3876},
	],
};

function lbTestInit()
{
    Log("lbTestInit", lbTestData);
};

lbTestInit();
