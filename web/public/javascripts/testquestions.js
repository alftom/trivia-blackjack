var Test = {
	userInfo: {
		uuid: 1234,
		level: 0,
		xp: 0,
		coin: 10000,
		name: "Joe User",	// optional 
		zip: "94111"		// optional 
	},

	questions: {
		1: { xp: 1, category: 'Federal', question: 'Who is the president of the US?', answer: 'b', choices: {a: 'Michelle Pfiefer', b:'Barack Obama', c:'Donald Trump', d:'Donna Summers', e:''}, explanation: 'The black dude' }, 
	    2: { xp: 1, category: 'Federal', question: 'How long is a presidential term?', answer: 'c', choices: {a: '1 year', b:'2 years', c:'4 years', d:'6 years', e:'8 years'}, explanation: 'explanation placeholder' },
	    3: { xp: 2, category: 'Federal', question: 'How many senators are there in Congress?', answer: 'a', choices: {a: '50', b:'100', c:'200', d:'400', e:'435'}, explanation: 'explanation placeholder' },
		30: { xp: 2, category: 'history', question: 'Where does the US president work?', answer: 'b', choices: { a: 'Capitol', b: 'White House', c: 'Pentagon', d: 'Washington Monument', e: 'Independence Hall'}, explanation: 'explanation placeholder' },
		31: { xp: 3, category: 'history', question: 'Where is the location of the current US government?', answer: 'a', choices: { a: 'Washington DC', b: 'New York', c: 'Delaware', d: 'Philadelphia', e: ''}, explanation: 'explanation placeholder' }, 
		54: { xp: 3, category: 'campaigns', question: 'Are campaign ads required by law to be truthful?', answer: 'b', choices: { a: 'Yes', b: 'No', c: '', d: '', e: ''}, explanation: 'explanation placeholder' },
		55: { xp: 4, category: 'campaigns', question: 'How is the US president elected?', answer: 'b', choices: { a: 'majority of popular vote', b: 'majority of electoral votes', c: 'majority of votes in the Senate', d: 'most money raised from special interest groups', e: ''}, explanation: 'explanation placeholder' },
		56: { xp: 4, category: 'campaigns', question: 'The minimum voting age in the US is', answer: 'b', choices: { a: '13', b: '18', c: '21', d: '25', e: '30'} },
		57: { xp: 4, category: 'campaigns', question: 'Any US citizen can vote', answer: 'b', choices: { a: 'TRUE', b: 'FALSE', c: '', d: '', e: ''} },
		58: { xp: 5, category: 'campaigns', question: 'What is "hard money" in the political campaign sense?', answer: 'a', choices: { a: 'Money donated directly to individual candidates', b: 'Money donated to a political party for day-to-day operations', c: 'Money backed by gold', d: 'Cash (as opposed to shares)', e: 'Coins (as opposed to bills)'} },
		59: { xp: 5, category: 'campaigns', question: 'What is "soft money" in the political campaign sense?', answer: 'b', choices: { a: 'Money donated directly to individual candidates', b: 'Money donated to a political party for day-to-day operations', c: 'Money backed by gold', d: 'Cash (as opposed to shares)', e: 'Coins (as opposed to bills)'} },
		60: { xp: 5, category: 'campaigns', question: 'What is a PAC?', answer: 'c', choices: { a: 'A political party', b: 'A company that raises money to publicize issues, but not to expressly promote voting for or against a candidate or ballot measure', c: 'A company that raises money to influence a federal election, often by running ads that specifically endorse a candidate.', d: 'a video game character', e: ''} },
		61: { xp: 6, category: 'campaigns', question: 'The new US president is inaugurated when?', answer: 'd', choices: { a: 'October', b: 'November', c: 'December', d: 'January', e: 'February'} },
		62: { xp: 6, category: 'campaigns', question: 'What is a lobbyist?', answer: 'a', choices: { a: 'An individual that gets paid to meet with and influence lawmakers on behalf of a special interest', b: 'An individual that on occasion volunteers to meet with and influence lawmakers on behalf of a special interest ', c: 'A service employee that aids customers on the first floor of a building', d: 'A political interest group that employs individuals to meet with and influence lawmakers to act in the group\'s interest', e: ''} },
		63: { xp: 6, category: 'campaigns', question: 'What is a political party?', answer: 'a', choices: { a: 'an organization�that seeks to influence government policy by nominating their own candidates and trying to seat them in political office', b: 'an organization that attempts to get government officials to respond to its philosophy and way of thinking on particular issues', c: 'an event who\'s purpose is to raise money for a political candidate', d: 'an event for celebrating a political accomplishment such as passing a bill or winning an election', e: 'an entity formed to circumvent strict limits on individual campaign donations'} },
		64: { xp: 7, category: 'campaigns', question: 'Does the US media have a political bias?', answer: 'a', choices: { a: 'Yes, and it�s a liberal bias', b: 'Yes, and it\'s a conservative bias', c: 'No bias', d: '', e: ''} },
		65: { xp: 7, category: 'campaigns', question: 'Does C-SPAN have a political bias?', answer: 'c', choices: { a: 'Yes, and it�s a liberal bias', b: 'Yes, and it\'s a conservative bias', c: 'No bias', d: '', e: ''} },
		66: { xp: 7, category: 'campaigns', question: 'US primary elections are held in order to', answer: 'a', choices: { a: 'narrow down the field of candidates within a political party', b: 'expand the field of candidtes within a political party', c: 'give the voters more choice in the general election', d: 'allow state legislatures the opportunity to determine political districts', e: 'increase participation of third-party candidates'} },
		67: { xp: 8, category: 'campaigns', question: 'A private organization that attempts to get government officials to respond to its philosophy and way of thinking on issues is a', answer: 'c', choices: { a: 'pressure group', b: 'political pressure group', c: 'interest group', d: 'political power group', e: 'lobbyist'} },
		68: { xp: 8, category: 'campaigns', question: 'Interest groups attempt to influence members of US Congress by doing all of the following EXCEPT:', answer: 'a', choices: { a: 'nominating candidates for public office', b: 'taking an issue to court', c: 'influencing party platforms', d: 'utilizing political action committees (PACs)', e: 'appealing to the public for support'} },
		69: { xp: 8, category: 'campaigns', question: 'Which of the following actions is NOT appropriate when conducting an unbiased scientific poll?', answer: 'c', choices: { a: 'controlling how the poll is taken', b: 'sampling', c: 'formulating public policy', d: 'analyzing and reporting the results', e: 'determining the margin of error of the poll'} },
		70: { xp: 9, category: 'campaigns', question: 'Which of the following is NOT a role of the media?', answer: 'b', choices: { a: 'shaping public opinion', b: 'formulating public policy', c: 'providing a link between citizens and the government', d: 'serving as an investigator of personalities', e: 'popularizing academic subject matter that the public may otherwise not hear about'} },
		71: { xp: 9, category: 'campaigns', question: 'The national convention serves what major purpose for a US political party?', answer: 'c', choices: { a: 'to allow the people to participate in party organization', b: 'to establish the rules of party campaigning', c: 'to select the party\'s candidate', d: 'to vote on party policy', e: 'to raise money, recruit, and publicize the party'} },
		72: { xp: 9, category: 'campaigns', question: 'Which of the following occurs earliest in a US presidential election?', answer: 'c', choices: { a: 'national party conventions', b: 'popular election', c: 'state primaries and caucuses', d: 'electoral college election', e: 'choosing a running mate'} },
		73: { xp: 10, category: 'campaigns', question: 'Compared to a political conservative, a liberal generally supports', answer: 'b', choices: { a: 'a smaller government', b: 'active government involvement in individual welfare', c: 'a stronger military', d: 'traditional values and lifestyles', e: 'a more limited government role in entitlements'} },
		74: { xp: 10, category: 'campaigns', question: 'The biggest factor in influencing whether a person approves or disapproves of a president\'s job performance is', answer: 'a', choices: { a: 'political party identification', b: 'geographic location', c: 'race', d: 'income level', e: 'gender'} },
		75: { xp: 10, category: 'campaigns', question: 'Which of the following is NOT a common complaint about US media coverage of politics in general?', answer: 'd', choices: { a: 'favoring sensational stories at the expense of serious ones', b: 'making a big deal about insignficant actions or unintended comments by politicians', c: 'liberal bias', d: 'conservative bias', e: 'reporting what everyone else is reporting rather than finding a new angle'} },
		76: { xp: 11, category: 'constitution', question: 'The United States government is a:', answer: 'd', choices: { a: 'monarchy', b: 'oligarchy', c: 'direct democracy', d: 'representative democracy', e: 'empire'} }
	},
	
	0: {
		level: 0,
		list: {
			10: { category: 'Federal', question: 'Who is the president of the US?', answer: 'b', choices: {a: 'Michelle Pfiefer', b:'Barack Obama', c:'Donald Trump', d:'Donna Summers', e:''}, explanation: 'The black dude' },
		}
	},

	1: {
		level: 1,
		list: {
//			10: { category: 'budget', question: 'What is "interest" in terms of economics?', answer: 'a', choices: { a: 'A charge for a loan', b: 'A share in something', c: 'A short expression for an interest group', d: 'A state of curiosity, concern, or attention to something', e: 'Regard for one\'s own benefit'} },
			31: { category: 'history', question: 'Where is the location of the current US government?', answer: 'a', choices: { a: 'Washington DC', b: 'New York', c: 'Delaware', d: 'Philadelphia', e: ''} }
		}
	},
	
	2: {
		level: 2,
		list: {
//			32: { category: 'budget', question: 'What is the definition of GDP (gross domestic product)?', answer: 'a', choices: { a: 'The total market value of all final goods and services produced in a country in a given year', b: 'The amount of currency in circulation in a country\'s economy', c: 'The entire supply of money in a country\'s economy', d: 'A measure that relates to the weighted average of a basket of commonly-used consumer goods and services', e: ''} },
			152: { category: 'social', question: 'In political terms, what does "right" refer to?', answer: 'b', choices: { a: 'liberal ideas', b: 'conservative ideas', c: '', d: '', e: ''} }
		}
	},
	
	3: {
		level: 3,
		list: {
//			153: { category: 'budget', question: 'What number is closest to the current US federal debt as a percentage of US GDP?', answer: 'e', choices: { a: '0.2', b: '0.4', c: '0.6', d: '0.8', e: '1'} },
			362: { category: 'constitution', question: 'Which US government body is supposed to be closest to the people?', answer: 'a', choices: { a: 'House of Representatives', b: 'Senate', c: 'president', d: 'Supreme Court', e: ''} }
		}
	},
	
	4: {
		level: 4,
		list: {
//			363: { category: 'budget', question: 'How much of US government debt is owed to foreign entities (governments, companies, and individuals)?', answer: 'd', choices: { a: '0.1', b: '0.3', c: '0.5', d: '0.7', e: '0.9'} },
			557: { category: 'campaigns', question: 'When a country is under threat the effect on the country\'s president\'s approval ratings is usually', answer: 'a', choices: { a: 'a sharp increase', b: 'a sharp decline', c: 'a slight increase', d: 'a slight decline', e: 'no effect'} }
		}
	},
	
	5: {
		level: 5,
		list: {
//			558: { category: 'budget', question: 'Which of the following US federal government initiatives puts the most financial burden on states?', answer: 'c', choices: { a: 'categorical grants in which the federal government gives states money to comply with a certain law', b: 'block grants which give states money in a less restrictive manner than categorical grants', c: 'unfunded mandates which impose requirements on states without providing funding', d: 'grants-in-aid programs that allocate money for specific programs', e: ''} },
			638: { category: 'history', question: 'US Congress passed a bill in 1994 that gave the president a "line item veto".  What was the effect of this bill?', answer: 'b', choices: { a: 'reduction in spending that allowed the government to realize a budget surplus', b: 'none- the law was declared unconstitutional by the Supreme Court', c: 'none- Congress overrode the line item veto each time it was used', d: 'spending went up because Congress put more spending items in bills to compensate for the line item veto', e: ''} }
		}
	},
	
	6: {
		level: 6,
		list: {
//			639: { category: 'constitution', question: 'In A650 the US Supreme Court declared that', answer: 'b', choices: { a: 'suspects in police custody must be informed of their rights', b: 'state courts must provide an attorney to poor defendants accused of a felony', c: 'the death penalty is anstitutional when it is imposed based on the circumstances of the case', d: 'evidence obtained without a search warrant is excluded from trial in state courts', e: 'searches of criminal subjects are constitutional'} },
			725: { category: 'government', question: 'How are the members of the US president\'s Council of Economic Advisors chosen?', answer: 'b', choices: { a: 'The president choses the members, no approval required', b: 'The president chooses the members, but the Senate must approve the appointments', c: 'The president chooses the members, but the House must approve the appointments', d: 'The president chooses the members, but both the House and Senate must approve the appointments', e: 'The president\'s political party chooses the members'} }
		}
	}
};

function TestInit()
{
    Log("TestInit", Test);
};

TestInit();

