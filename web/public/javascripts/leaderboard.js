/**
 * @file leaderboard.js
 * @author Alfred Tom
 * 
 *  (c) 2011 Alfred Tom
 */

/*
 * JSON calls:
 * 
 *		1. Get user stat information:
 * 			call:  User/Stats?correct=45&incorrect=20
 * 				- correct:  total number of questions answered correctly
 * 				- incorrect:  ttoal number of questions answered incorrectly
 * 			response:
 * 				{
 * 					code: 1,
 * 					triviaPercentile: 69,
 * 				}
 * 
 * 		2. Get leaderboard info:
 * 			call: User/Leaderboard?scope=f&type=kp&points=2932&rows=10
 * 				- scope:  either f (for friends) or w (for worldwide)
 * 				- type:  xp (total experience), kp (knowledge), cp (competition), or ip (influence)
 * 				- points:  current point level of the "type"
 * 				- rows:  number of players in the leaderboard, including the user
 *			response:
 * 				{
 * 					{position: 5, name: "Santa Claus", kp: 3948},
 *					{position: 6, name: "Mickey Mouse", kp: 3290},
 *					{position: 7, name: "Sheik Yaboodi", kp: 2988},
 *					{position: 8, name: "Joe User", kp: 2932},
 *					{position: 9, name: "Ben Dover", kp: 2891},
 *					{position: 10, name: "Chump Change", kp: 2218},
 *					{position: 11, name: "Agent Smart", kp: 1893},
 *					{position: 12, name: "Howie Long", kp: 1234},
 *					{position: 13, name: "Forrest Gump", kp: 982},
 *					{position: 14, name: "A Rock", kp: 12},
 *        		}
 */

RR.LBoard = {
	// info fields
	userInfo: null,
	friendsList: null,
	worldList: null,
	
	// UI elements
	
	
	// Methods
	Init: function()
	{
		// RR.Polls field setup

		// hit the server for the required data 
		var leaderboardInfo = lbTestData;	// *** for now we'll use the lbTestData
        //RR.TriviaBJ[ RR.TriviaBJ.userKey ] = Test.userInfo;		//  *** eventually get userInfo from RR.TriviaBJ
		//RR.LBoard.userInfo = RR.TriviaBJ[ RR.TriviaBJ.userKey ];
		RR.LBoard.userInfo = leaderboardInfo.userInfo;
		RR.LBoard.friendsList = leaderboardInfo.friendsList;
		RR.LBoard.worldList = leaderboardInfo.worldList;
		
		// fill in player profile info
		$("#playername").text(this.userInfo.name);
		$("#playerlevel").text(this.userInfo.level);
		$("#playerkp").text(this.userInfo.kp);
		$("#playercoins").text(this.userInfo.coin);

		// fill in player stats info
		$("#playercorrect").text(this.userInfo.correct);
		$("#playerincorrect").text(this.userInfo.incorrect);
		$("#playeraccuracy").text(String(Math.round(this.userInfo.correct*100/(this.userInfo.correct + this.userInfo.incorrect))) + "%");
		$("#playeraccuracypercentile").text(this.userInfo.accuracyPercentile);

		// fill in the leaderboards
		for (var i in this.friendsList) {
			this.AddPlayerRow($("#tablekpfriends"), this.friendsList[i].position, this.friendsList[i].name, this.friendsList[i].kp);
		}
		for (var i in this.worldList) {
			this.AddPlayerRow($("#tablekpworld"), this.worldList[i].position, this.worldList[i].name, this.worldList[i].kp);
		}
		
		// UI manipulation
		$('table.stripe tr:even').addClass('alt'); 	// alternate row color
	},
	
	// this helper function adds a leaderboard row (with the given position, name, and xp) to a leaderboard table (element)
	AddPlayerRow: function(element, position, name, xp)
	{
		var newRowHTML = "<tr><td class='tablerowposition'>" + position + "</td><td class='tablerowname'>" + name + "</td><td class='tablerowxp'>" + xp + "</td></tr>\n";
		
		// when we have player images add: <td class='pic'><img src='images/profile-pic.png'></td>
		element.append(newRowHTML);
	},
}
