
//Global vars
//var _isPC = navigator.userAgent.match(/Chrome|MSIE|Firefox|Opera|Safari/i);
//var _isMobile = navigator.userAgent.match(/mobile|ip(ad|hone|od)|Android/i);

/*var _fbSession = null; /* Session Fields: = {    
		uid: "100000730526137", 
		session_key: "2.SFgVFqX3_rtGOvM8F68icw__.3600.1301770800-100000730526137", 
		secret: "YtSKT5IdkPiubkHvz4F0Tw__", 
		expires: 1301770800, 
		base_domain: "repraz.com", 
		access_token: "293059732886|2.SFgVFqX3_rtGOvM8F68icw__.3600.1301770800-100000730526137|xYXRGjwecP-kbkHYQNaoojEN5_E",
		sig:"38c7aff072ab259b1cb3af5453ba6fcf"
}; */
var _test = 1;


function Log(a,b,c,d,e,f,g)
{
	if (!_test) return;
	
	b = b || '';
	c = c || '';
	d = d || '';
	e = e || '';
	f = f || '';
	g = g || '';
	
	if (window.console) {
		console.log(a,b,c,d,e,f,g);
	} else {
		//alert(a+b+c+d+e+f+g);
	}
};


// Parses a query string
function QueryString (str)
{
	this.string = str;
	this.Parse = (function(a) {
	    if (a == "") return {};
	    var b = {};
	    for (var i = 0; i < a.length; ++i)
	    {
	        var p=a[i].split('=');
	        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	    }
	    return b;
	})(this.string.split('&'));
};
	
// Parses the menu and id from the location hash
function ParseUrl(url)
{
	if (! url) return ({});

	// discard everything before the '?'
	idx = url.indexOf("?");
	if (idx < 0) return ({});	// '?' not found!
	
	string = url.substring(idx+1);

	var result = {};
	
	var qs = new QueryString(string);
	var menu = qs.Parse["menu"];
	var id   = qs.Parse["id"];
	
	if (menu && menu!=undefined) result["menu"] = menu;
	if (id   &&   id!=undefined) result["id"]= id;

	return (result);
};

var callbackStack = [];

function ValidateInput($input, $errorbox)
{
	if (typeof($input) == 'string') $input = $($input); // make a jq object if not already				
	var required = $input.attr("required");
	
	var type   = $input.prop("type"),
		val    = $input.val(),
		valid  = true,
		errMsg = "";
	
	switch (type)
	{
		case "text":
		case "password":
			if (required && $.trim(val)== "") { 
				errMsg = "You must enter a value for"; 
				valid  = false;
			}
			break;
		case "email":
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!filter.test(val)) {
				errMsg = "The value entered is not valid for"; 
				valid  = false;
			} 						
			break;
		case "checkbox":
			if (required && ! $input.prop("checked")) {
				errMsg = "You must check";
				valid  = false;
			}
			break;
		case "radio":
			if (required && !val) {
				errMsg = "A value must be selected for";
				valid  = false;
			}
			break;
		default:
			break;
	}
	if (!valid) {
		$input.addClass("notvalid");
		var label  = $input.closest("form").find("label[for='"+$input.attr('name')+"']").text();
		errMsg += " <b>'"+label+"'</b>";
	} else {
		$input.removeClass("notvalid");
	}
	return (errMsg);
};

function ValidateForm($form, $errorBox)
{
	if (typeof($form) == 'string') $form = $($form); // make it a jq object if not already
	var valid = true;

	var errorMsg  = ""; 
	
	$form.find("input").each(function() {
		var $input = $(this);
		var msg = ValidateInput($input);
		if (msg.length) { 
			if (! errorMsg.length) {
				errorMsg = msg;
			} else {
				errorMsg = "Please enter valid values for the items hilighted above";
			}
			valid = false;
		} 
	});

	if ($errorBox.length > 0) {
		if (! valid) {
			$errorBox.html(errorMsg);	
			$errorBox.show();
			Log("   error message: ", errorMsg);
		} else {
			$errorBox.hide();
		}
	}
	return (valid);
};


/**
 * form: "#formid" or a jq object
 * svc: script to post to, such as "svc.login.php"
 * onSuccess: page to go to next, or a callback function to execute 
 */
function PostForm(form, onSuccess)
{
	Log("PostForm (", form, ",", onSuccess, ")");
	var $form = $(form);
	var $errorBox = $form.children("div.errorbox");

	var separator = "?";
	var url = RR.Page.AbsoluteURL($form.attr("action"));
	if (url.indexOf("?") > 0) separator = "&";	// we already have a "?", so use a "&" to attach form args
	url += separator + $form.serialize();
	
	if (ValidateForm($form, $errorBox)) {
		Log ("   Validated form, submitting to ", url);
		
		$.ajax({
			url: url,
			type: "GET",
			crossDomain: true,
			data: { format: "json" },
			dataType: "json",
			success: function(json, status) {
				if (json.code == 1) {
					Log ("   Json Success! ");
					$errorBox.hide();
					//alert("Success: "+json.msg);
					
					// If there are functions in the callback stack, use it!
					if (callbackStack.length > 0) {
						onSuccess = callbackStack.shift();
					}
					// If still no onSuccess specified, try onSuccess attribute
					if (!onSuccess) {
						onSuccess = $form.attr("onSuccess"); 
					}
					if (onSuccess) {
						switch (typeof(onSuccess)) {
							case 'string':
								Log ("   reloading: ", onSuccess);
								RR.Page.Reload(onSuccess);
								break;
							case 'function':
								Log ("   executing onSuccess function");
								onSuccess();
								break;
							default:
								Log ("   no onSuccess found, doing nothing");
								// do nothing
						}
					} 
				} else {
					// Highlight the offending input field, if any
					if (json.field) {
						$("input[name='"+json.field+"']").addClass("notvalid");
					}
					// Display error message
					$errorBox.html(json.msg);
					$errorBox.show();
				}
			}
		});
	}
	return (void(0));
};

function ExecService(idName, idVal, svcScript, nextScript)
{
	Log("ExecService ("+idName+","+idVal+","+svcScript+","+nextScript+")");
	ExecService.$errorBox = $("div.errorbox", $.mobile.activePage);

	var data = { format: "json" };
	data[idName] = idVal;
	
	var url = RR.Page.AbsoluteURL(svcScript); 
	Log("   Sending service request to ",url);
	
	$.ajax({
		url: url,
		type: "GET",
		crossDomain: true,
		data: data,
		dataType: "json",
		success: function(json, status) {
			if (json.code == 1) {
				Log("   Success, reloading ",nextScript);
				ExecService.$errorBox.hide();
				// Reload the page 		
				RR.Page.Reload(nextScript);
			} else {
				Log("   Error, showing error msg: ",json.msg);
				ExecService.$errorBox.html(json.msg);
				ExecService.$errorBox.show();
			}
		}
	});
	return(void(0));
};

function IsArray( obj )
{
	return ( obj && typeof(obj.splice == "function"));
}

// Test scenarios
if (_test) {
	// window.device = { name:'TestDevice', phonegap:'1.1', platform:'Android', uuid:'13', version:'2.2' };
}

