/**
 * @file classes.js
 * @author Sherk Chung
 * 
 *  (c) 2011 Sherk Chung
 */

// Global objects
var RR = {
    Device: null,
    Page: null, 
    Profile: null,
    UserCache: null,
    ViewPort: null
};

// cache for performance, 
var _$window = $(window);

RR.Device = 
{
    isNative:    false,
    isMobile:    false,
    isApple:    false,
    isPC:        false,
    name:        null, 
    phonegap:    null, 
    platform:    null, 
    uuid:        null, 
    version:    null,

    // Should only be called when running in native mode
    NativeInit: function() 
    {
        Log("RR.Device.NativeInit()");
        RR.Device.isNative=true;
        RR.Device.isMobile=true;
        
        // Copy over info from phonegap device object
        for (attr in {name:0,phonegap:0,platform:0,uuid:0,version:0}) {
            Log("   RR.Device.",attr," = ",window.device[attr]);
            RR.Device[attr] = window.device[attr];
        }

        // Ajax requests go to the production site
        RR.Page.urlBase = "http://www.repraz.com/";
        
    },

    WebInit: function()
    {
        Log("RR.Device.WebInit()");
        RR.Device.isNative=false;

        // Since we are not native, we must be web
        // Find out if platform is mobile or PC
        RR.Device.platform= navigator.platform;
        RR.Device.isPC     = RR.Device.platform.match(/Win|Mac/i);
        RR.Device.isMobile= navigator.userAgent.match(/mobile|ip(ad|hone|od)|Android|WebOS/i);
        if (RR.Device.isMobile) {
            RR.Device.isPC = false;    // if device wants to act as a mobile, let it
            if (RR.Device.isMobile.indexOf("Android") != -1) {
                RR.Device.isMobile = "Android";    // Android match returns funny result, can't manipulate as string, so set as string to get around probem
            }
        }
        // Ajax requests go to the location where the first page came from
        RR.Page.urlBase = location.href.replace(/index.html.*/i, "");
        RR.Page.urlBase = RR.Page.urlBase.replace(/#.*/i, "");
        //alert(RR.Page.urlBase);
    },
    
    // Initializes the device object to contain the phonegap device attributes
    Init: function() 
    {
        Log("RR.Device.Init()");
        Log("   window.device: ", window.device);
        Log("   navigator.platform: ", navigator.platform);
        Log("   navigator.userAgent: ", navigator.userAgent);
        Log("   screen: ", screen.width, " x ", screen.height);

        // First figure out if we are running as a native app 
        if (!window.device) {
            Log("   Not native, must be web");
            RR.Device.WebInit();
        } else {
            Log("   Native app!");
            RR.Device.NativeInit();
        } // if (!window.device) {

        // We treat apple devices differently
        RR.Device.isApple    = RR.Device.platform.match(/ip(ad|hone|od)/i);
        Log("   Platform: ", RR.Device.platform);
        Log("   isPC: ", RR.Device.isPC);
        Log("   isMobile: ", RR.Device.isMobile);
        Log("   isApple: ", RR.Device.isApple);

    },
    
    /*
    Register: function(callback)
    {
        Log ("RR.Device.Register()");
        var data = {
            name:        RR.Device.name, 
            phonegap:    RR.Device.phonegap, 
            platform:    RR.Device.platform, 
            uuid:        RR.Device.uuid, 
            version:    RR.Device.version
        };
        data.format = "jsonp";

        $.ajax({
            url: RR.Page.AbsoluteURL("svc.m.register.php"),
            dataType: 'jsonp',
            crossDomain: true,
            data: data,
            success: function (json, status) {
                //Log("status:" + status);
                if (json.code==1) {    // success

                    // Assign device picture depending on device type
                    var image = "img/pic_device.png";
                    if (RR.Device.isApple) {
                        image = "img/pic_apple.png"; 
                    } else if (RR.Device.platform == "Android") {
                        image = "img/pic_android.png"; 
                    }
                    var name = RR.Device.name;
                    if (!name) name = 'Mobile RR.Device';
                    
                    // Cache the user info returned (device is registered as a user now)
                    var userInfo = {};
                    
                    userInfo[RR.UserCache.KEY_USERID]  = json.userId;
                    userInfo[RR.UserCache.KEY_PICTURE] = image;
                    userInfo[RR.UserCache.KEY_NAME]    = name;
                    userInfo[RR.UserCache.KEY_FRIENDS] = [];    // devices have no friends :-(
                    userInfo[RR.UserCache.KEY_PERSIST] = true;
                    RR.UserCache.Set(userInfo);
                    Log("   Registered device, caching device as user: ", userInfo);
                    
                    // Now that we registered, execute the callback Fn
                    if (callback) callback();
                    
                } else {    // Login failed
                    RR.Page.Error(json.msg);
                }
            } // success()
        }); // ajax
    } // Register
    */
}; // RR.Device

RR.Page = 
{
    // DOM elements
    $header:         null,
    $leftCol:         null,
    $leftContent:     null,
    $viewPort:         null,
    $rightCol:         null,
    $searchPage:    null,
    $errorPage:        null,
    $errorMsg:        null,

    $scrollables:    null,
    
    currWidth:        0,
    currHeight:     0,
    leftColVisible:    -1,
    min2ColWidth:    630,
    pageTransition:    'none',
    dialogTransition: 'none',

    appId:        293059732886,     // FB app Id
    urlBase:    "",

    // Call this on DOM ready
    Init: function()
    {
        Log("RR.Page.Init()");
        RR.Page.$header        = $("#mainheader");
        RR.Page.$leftCol    = $("#leftcol");
        RR.Page.$leftContent= $("#leftcontent");
        RR.Page.$viewPort    = $("#viewport");
        RR.Page.$rightCol    = $("#rightcol");
        RR.Page.$searchPage    = $("#searchpage");
        RR.Page.$errorPage    = $("#errorpage");
        RR.Page.$errorMsg    = $("#errormsg");
        
        //$.mobile.pageContainer  = RR.Page.$viewPort;
        
        // RR.Page resize handler
        var orientationEvent = "orientationchange";
        if (RR.Device.isPC || RR.Device.isMobile == "Android" || ! orientationEvent in window) orientationEvent = "resize";   
        _$window.bind(orientationEvent, RR.Page.Resize);
        Log("   Resize event: ", orientationEvent);
        
        /* Event Handlers */
        /*----------------*/
        // Clicking on "RR.Favorites" will get faves, or pop up login prompt if can't id the user
        //$("#favoritesnav").bind("vclick", RR.Favorites.Load);

        // Event handler for viewport pagehide 
        //$("div[data-role='page']", RR.Page.$viewPort).live('pagehide', RR.ViewPort.PageHide); 

        // Event handler for mouse clicks inside viewport (instead of binding handlers to each item inside viewport)
        $("div[data-role='page']", RR.Page.$viewPort).live('mouseup touchend', RR.ViewPort.Click);
        
        // Make the "search" button load up new listings in the listings menu
        $("#searchform").bind("submit", function() {
            //SetNavIdx(_searchNavIdx);
            // Ignore if input is empty
            var searchText = $("#searchtext").val();
            if ($.trim(searchText) == "") return (false);
            
            var url = "svc.listings.php?menu=search&id="+searchText;
            Log("Search.submit", url);
            RR.Page.Page(url);
            return(false);    // prevent default behavior
        });

        if (RR.Device.isApple || RR.Device.isPC) {
            Log("   Setting transitions to be animated");
            //RR.Page.pageTransition = 'slide';
            //RR.Page.dialogTransition = 'pop';
        }

        RR.Page.Resize();    // resize to determine how many cols to show

    }, // RR.Page.Init()

    Resize: function()
    {
        if (_$window.width() == RR.Page.currWidth && _$window.height() == RR.Page.currHeight ) {
            Log("Resize but no size change detected, ignoring...");
            return;   // do this if we binding to a resize type event
        }

        var t = _$window;
        var w = t.width();
        var h = t.height();

        Log("   Resize! width: ", w);

        // On some mobile devices, the box layout doesn't stretch properly
        // workaround for the browser bugs is to set explicitly set width 
        $("#mainbody").width(w);

        // Explicitly set the viewport height to make the scrolling
        // plugin work correctly
        if (!RR.Device.isPC) {
            // Update scrollable area if height has changed
            if (RR.Page.currHeight != h) {
                Log("   Explicitly setting height on scrollable area");
                if ( RR.Page.$scrollables ) {
                    RR.Page.$scrollables.touchScroll('update');    // update scroller to new height
                    // Explicitly set parent container height so touchscroll will work properly
                    //var $parent = RR.Page.scrollables[idx].parent(); 
                    //$parent.css("height", $parent.parent().innerHeight()+"px");
                }
            }
        }
        RR.Page.currWidth  = w;
        RR.Page.currHeight = h;

        RR.TriviaBJ.OrientationChanged();

    }, // RR.Page.Resize()
    
    Reload: function(pageUrl)
    {
        Log("RR.Page.Reload(", pageUrl, ")");
        // First remove current page from url history stack
        /*
        $.mobile.urlHistory.activeIndex--;
        $.mobile.urlHistory.clearForward();
        */
        return (RR.Page.Page(pageUrl, { reloadPage: true }));
    },
    
    Page: function(page, options)
    {
        var toPage = RR.Page.AbsoluteURL(page);
        Log ("RR.Page.Page(",toPage,",", options,")");
        Log ("   Setting RR.ViewPort.clearFwd = true");
        RR.ViewPort.clearFwd = true;    // Clear Fwd button on page after it renders
        if (!options) options = {};
        options = $.extend({},
            { 
//                transition: RR.Page.pageTransition //,
//                pageContainer: RR.Page.$viewPort 
            }, options);
        var url = RR.Page.AbsoluteURL(toPage);
        Log ("   Going to: ", url);
        $.mobile.changePage(url,options);
        return (void(0));
    },

    Dialog: function(page, options)
    {
        Log ("RR.Page.Dialog(",page,",", options,")");
        if (!options) options = {};
        $.extend(options, { 
            transition: RR.Page.dialogTransition, 
            role: 'dialog'
        });
        return (RR.Page.Page(page, options));
    },

    Error: function(errMsg)
    {
        Log("RR.Page.Error(",errMsg,")");
        RR.Page.$errorMsg.html(errMsg);
        return (RR.Page.Dialog(RR.Page.$errorPage));
    },

    Back: function()
    {
        history.back();
        return (void(0)); // in case it's called from an href
    },

    Forward: function()
    {
        history.forward();
        return (void(0)); // in case it's called from an href
    },

    AbsoluteURL: function(url)
    {
        if (typeof(url) == 'string') {
            if (url.substring(0,1) != "#" && url.substring(0,4).toLowerCase() != "http") {
                url = RR.Page.urlBase + url;
            }
        }
        return (url);
    },

    IsLandscape: function()
    {
        Log("height=" + RR.Page.currHeight );
        Log("width=" + RR.Page.currWidth );
        if ( RR.Page.currHeight < 600 )
            return true;
        return false;
    },

};    // RR.Page


RR.ViewPort = 
{
    clearFwd: false,
        
    PageShow: function(event)
    {
        Log("RR.ViewPort.PageShow()");
        Log("   window size: ", window.innerWidth, " x ", window.innerHeight);
        
        // Add "RepRaz" to the document title (jquery mobile changes it dynamically)
        document.title = "RepRaz | "+document.title;                        
            
        // If on a mobile device, use the touchscroll plugin to make
        // content inside the viewport scrollable
        var $this = $(this);
        
        if ( RR.Device.isMobile ) {
            Log("   Making content area scrollable...");
            RR.Page.$scrollables = $this.find("div.scrollable").touchScroll();
            Log("      found "+RR.Page.$scrollables.size()+" scrollables" );
        }
    },
    
    PageHide: function(event)
    {
        Log("RR.ViewPort.PageHide()");
        $this = $(this);
        if ($this.attr("data-cache") == "false") RR.ViewPort.Remove($this);
    },

    Remove: function($page)
    {
        Log("RR.ViewPort.Remove()");
        $page.remove(); // $this = the current page in viewport
    },

    Click: function(event)
    {
        Log ("Viewport.Click on ", event.target);
        var link = event.target;

        // Find the closest parent anchor (only check 3 levels)
        // If can't find an anchor, ignore the click
        // Keep this native js (not jquery) for performance
        //Log("   link: ",link.nodeName);
        if (link.nodeName != "A") {
            link = link.parentNode;
            //Log("   link: ",link.nodeName);
            if (link.nodeName != "A") {
                link = link.parentNode;
                //Log("   link: ",link.nodeName);
                if (link.nodeName != "A") {
                    // We don't need to handle this click event, so let the default behavior continue
                    //Log ("Not an anchor, allow default action...");
                    return (true);
                }
            } 
        } 
        var $link = $(link); 
        var linkClass = $link.attr("class").match(/menuitem|etc/);
        Log ("   class: ", linkClass);
        var allowDefault = false;
        
        // Handle the menu item drilldown click 
        if (linkClass == "somethingwecareabout") {
            Log("   ", linkClass);
        }
        else {
            // It's an anchor but not handled here, just ignore it
            Log("   Anchor but not handled, ignore it...");
            allowDefault = true;
        }
        return (allowDefault);
    }

}; // RR.ViewPort




function GlobalInit() 
{
    Log ("Global Init");
    // For testing
     /*
    RR.Device.isPC = false;
    RR.Device.isApple = false;
    RR.Device.isMobile = true;
    // End test code */

    RR.Page.Init();    // caches frequently used $jquery elements, calls Page.Resize()

    // Give the "loading" icon a little color 
    $("div.ui-loader").addClass("ui-btn-active");
};    

function PreInit()
{
     RR.Device.Init();    // Initializes device properties, determines if mobile, native, etc.

    /* pagecreate handlers have to execute before GlobalInit() because otherwise you
     * could miss the event if you waited until bodyload or dom ready
     */ 
    // Event handler for viewport pageshow
    $("div[data-role='page']").live("pagecreate", function() {
        Log("Binding pageshow");
        //$("div[data-role='page'], div[data-role='dialog']", RR.Page.$viewPort).live('pageshow', RR.ViewPort.PageShow); 
        $("div[data-role='page'], div[data-role='dialog']").live("pageshow", RR.ViewPort.PageShow);
    });
    
    if (typeof(PhoneGap) == 'object') {
         Log("PhoneGap, init on deviceready");
         //device ready, only fires if using phonegap and overides the default
         // _deviceInfo structure with the values from the device
        document.addEventListener("deviceready", GlobalInit, false);
    } else {
         Log("Non-phonegap, init on bodyload");
         $(function() { GlobalInit(); });    // not on phonegap, so init when Dom ready
    };
};

function SwipUpDownInit()
{

$(document).bind(
    'touchmove',
    function(e) {
        e.preventDefault();
    }
);

 (function() {
// initializes touch and scroll events
        var supportTouch = $.support.touch,
                scrollEvent = "touchmove scroll",
                touchStartEvent = supportTouch ? "touchstart" : "mousedown",
                touchStopEvent = supportTouch ? "touchend" : "mouseup",
                touchMoveEvent = supportTouch ? "touchmove" : "mousemove";

 // handles swipeup and swipedown
        $.event.special.swipeupdown = {
            setup: function() {
                var thisObject = this;
                var $this = $(thisObject);

                $this.bind(touchStartEvent, function(event) {
                    var data = event.originalEvent.touches ?
                            event.originalEvent.touches[ 0 ] :
                            event,
                            start = {
                                time: (new Date).getTime(),
                                coords: [ data.pageX, data.pageY ],
                                origin: $(event.target)
                            },
                            stop;

                    function moveHandler(event) {
                        if (!start) {
                            start = {
                                time: (new Date).getTime(),
                                coords: [ data.pageX, data.pageY ],
                                origin: $(event.target)
                            }
                            return;
                        }

                        var data = event.originalEvent.touches ?
                                event.originalEvent.touches[ 0 ] :
                                event;
                        stop = {
                            time: (new Date).getTime(),
                            coords: [ data.pageX, data.pageY ]
                        };

/*
                        // prevent scrolling
                        if (Math.abs(start.coords[1] - stop.coords[1]) > 5 || 
                            Math.abs(start.coords[0] - stop.coords[0]) > 5  ) {
                            event.preventDefault();
                        }
*/
                        if (start && stop) {
                            if (stop.time - start.time < 3000 &&
                                  Math.abs(start.coords[1] - stop.coords[1]) > 10 || 
                                    Math.abs(start.coords[0] - stop.coords[0]) > 10) {
                                start.origin
                                        .trigger("swipeupdown")
                                        .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                                start = stop = undefined;
                            }
                        }
                    }

                    $this
                            .bind(touchMoveEvent, moveHandler)
                            .one(touchStopEvent, function(event) {
                        $this.unbind(touchMoveEvent, moveHandler);

                        if (start && stop) {
                            if (stop.time - start.time < 3000 &&
                //                  Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
                //                  Math.abs(start.coords[0] - stop.coords[0]) < 75) {
                                    Math.abs(start.coords[1] - stop.coords[1]) > 10 || 
                                    Math.abs(start.coords[0] - stop.coords[0]) > 10) {
                                start.origin
                                        .trigger("swipeupdown")
                                        .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                            }
                        }
                        start = stop = undefined;
                    });
                });
            }
        };

//Adds the events to the jQuery events special collection
        $.each({
            swipedown: "swipeupdown",
            swipeup: "swipeupdown"
        }, function(event, sourceEvent){
            $.event.special[event] = {
                setup: function(){
                    $(this).bind(sourceEvent, $.noop);
                }
            };
        });

    })();
}

//Execute Pre-Init, which binds some event handlers that may have to fire before GlobalInit() gets called.
PreInit();    
SwipUpDownInit()
