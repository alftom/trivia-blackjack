# README #

### MIT License ###

Copyright 2021 <COPYRIGHT Alfred Tom> <COPYRIGHT Sherk Chung>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### What is this repository for? ###

* In 2013 General Motors announced a platform that allowed developers to build in-vehicle applications using HTML5 and Javascript.  I was one of the first researchers on that project and decided we needed to "eat our own dogfood", which means start developing HTML5 apps.  
* At that time the main platform for building a single codebase for web and mobile was PhoneGap.  Back then (2008) there were no front-end frameworks like Angular and React- just good old JQuery. There was also a US presidential election going on.  So, I worked with Sherk Chung, Geoff Chatterton, and Dave Evans to build a political trivia app we later launched as High Stakes Politics.

### How do I get set up? ###

* TBD

### Contribution guidelines ###

* TBD

### Who do I talk to? ###

* TBD
